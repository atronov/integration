/**
 * @author atronov
 */
$( document ).ready(function() {
	$("#message-message").hide();
	$("#message-constructor *[id$='etalon']").hide();
	$("#message-constructor>.content").addContainerButtons();
});

function validateMessage() {
	var invalidMessage;
	// first validate message
//	if ($("#message-constructor input").not("*[id$='etalon']").checkValidity())
//		invalidMessage = "Some message inputs are ";
	// check message existance
	if (!invalidMessage)
		if (!$('#message-constructor .message-element:visible').not(".content").length)
			invalidMessage = "Message must be defined";
	// check uniqueness of keys
	if (!invalidMessage)
		$('#message-constructor .message-map-element:visible').each(function() {
			var $keys = $(this).find(">.content")
			.find(">.key-container>.message-map-key,>.message-element>.key-container>.message-map-key")
			.find("input")
			.map(function() { return $(this).val(); })
			.get();
			if (_.unique($keys).length < $keys.length) {
				invalidMessage = "Map keys must be unique";
				return;
			}
			if (_.any($keys, function(el) { return !el || el.trim() == ""; })) {
				invalidMessage = "Map key cannot be empty";
				return;
			}
		});
	if (invalidMessage) {
		$("#message-message").text(invalidMessage).show();
		return false;
	} else {
		$("#message-message").text("").hide();
		return true;
	}
}

$.fn.copyEtalon = function() {
	return $(this).clone().removeAttr('id').show();
};
$.fn.addListOnClick = function() {
	return $(this).click(function() {
		var $list = $('#message-list-element-etalon')
			.copyEtalon()
			.addContainerHeader();
		if ($(this).isInMap())
			$list.addKey();
		$(this).toContent().append($list);
		checkContainerExists();
	});
};
$.fn.addMapOnClick = function() {
	return $(this).click(function() {
		var $map = $('#message-map-element-etalon')
			.copyEtalon()
			.addContainerHeader();
		if ($(this).isInMap())
			$map.addKey();
		$(this).toContent().append($map);
		checkContainerExists();
	});
};
$.fn.addContainerHeader = function() {
	return $(this).find(">.panel-heading")
		.addContainerButtons()
		.addValueButtons()
		.addDelButton()
		.parent();
};
$.fn.toContent = function() {
	var $element = $(this).closest(".message-element");
	if ($element.hasClass("content"))
		return $element;
	else
		return $element.find(">.content");
};
$.fn.addContainerButtons = function() {
	return $(this).append(
		$('#message-add-list-etalon').copyEtalon().addListOnClick()
		,$('#message-add-map-etalon').copyEtalon().addMapOnClick()
	);
};
$.fn.addDelButton = function() {
	return $(this).append(
		$("#message-del-button-etalon").copyEtalon()
		.click(function() {
			$(this).closest(".message-element").remove();
			checkContainerExists();
		})
	);
};
$.fn.isInMap = function() {
	return $(this).parent().closest(".message-element").hasClass('message-map-element');
};
$.fn.addKey = function() {
	var keyContainerSelector=".key-container";
	var $keyContainer;
	var $keyElement = $("#message-map-key-etalon").copyEtalon();
	if ($(this).has(".content").length) {
		$(this).find(keyContainerSelector).prepend($keyElement);
	} else {
		$(this).closest(keyContainerSelector).prepend($keyElement);
	}
	return $(this);
};
function checkContainerExists() {
	var $contentElement = $('#message-constructor>.content');
	if (!$contentElement.has('.message-container:visible').length)
		$contentElement.addContainerButtons();
	else
		$contentElement.children("button:visible").remove();
}
$.fn.addValue = function(valueType) {
	var $valueElement = $("#message-"+valueType+"-element-etalon").copyEtalon();
	if ($(this).isInMap()) {
		$valueElement.addKey();
	}
	$valueElement.addDelButton();
	$(this).closest(".message-element").find(">.content").append($valueElement);
	return $(this);
};
$.fn.addValueButtons = function() {
	return $(this).append(
		$('#message-add-boolean-etalon').copyEtalon().click(function() { $(this).addValue('boolean'); }),
		$('#message-add-number-etalon').copyEtalon().click(function() { $(this).addValue('number'); }),
		$('#message-add-text-etalon').copyEtalon().click(function() { $(this).addValue('text'); })
	);
};

function buildMessage() {
	var message = {};
	var $rootElement = $("#message-constructor > .message-element:visible > .message-element:visible");
	message.content = extractValue($rootElement);
	return message;
}

function extractValue($element) {
	var elementValue;
	var isMap = $element.hasClass("message-map-element");
	if (isMap)
		elementValue = {};
	else
		elementValue = [];
	var $elements = $element.find(">.content>.message-element");
	$elements.each(function() {
		var value;
		if ($(this).hasClass("message-container")) {
			value = extractValue($(this));
		 } else {
			value = $(this).getPrimitiveValue();
		}
		if (isMap) {
			var key = $(this).find(".message-map-key input").first().val();
			elementValue[key] = value;
		} else {
			elementValue.push(value);
		}
	});
	return elementValue;
};

$.fn.getPrimitiveValue = function() {
	if ($(this).hasClass("message-text-element"))
		return $(this).find(".message-element-value").val();
	else if ($(this).hasClass("message-number-element"))
		return parseFloat($(this).find(".message-element-value").val());
	else if ($(this).hasClass("message-boolean-element"))
		return $(this).find(".message-element-value").prop("checked");
	else
		return null;
};

