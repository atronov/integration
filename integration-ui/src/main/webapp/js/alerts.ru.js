/**
 * @author Alexey Tronov
 */
function Alerts() {}
Alerts.Ready = "Готов к началу моделирования";
Alerts.Busy = "Моделирование в процессе ...";
Alerts.AlreadyBusy = "Моделирование уже в процессе. Нельзя запустить дважды.";
Alerts.EndSuccess = "Моделирование успешно завершено";
Alerts.EndFailure = "Моделирование завершено с ошибкой";
Alerts.StopSuccess = "Моделирование успешно остановленно";
Alerts.StoppedFailure = "Моделирование остановленно с ошибкой";
Alerts.AlreadyStopped = "Моделирование уже остановленно. Нельзы установить дважды.";
Alerts.WrongConfig = "Неверная настройка. Исправьте и попробуйте снова.";
Alerts.Stopping = "Остановка ...";