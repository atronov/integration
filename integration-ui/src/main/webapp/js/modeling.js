/**
 * @author atronov
 */
modeling = (function() {
	var view = ModelingView();
	
	var collectConfig = function () {
		var configObject = {
			message: buildMessage(),
			buildConfig: {
				technology: $("#config-tech option:selected").text(),
				protocol: $("#config-protocol option:selected").text(),
				dataType: $("#config-data option:selected").text(),
				ssl: $("#config-secure").prop("checked"),
				topology: $("#config-topolpgy option:selected").text()
			},
			subscribers: parseInt($("#config-subscribers").val()),
			shedule: {
				duration: parseInt($("#config-duration").val()),
				step: parseInt($("#config-step").val())
			}
		};
		return configObject;
	};

	var runModeling = function () {
		if (!validate())
			return;
		var state = remoteState();
		if (state == 0) {
			var config = collectConfig();
			remoteRunAsync(config,
				function (state) {
					if (state == 0) {
						showStoppedState();
						info(Alerts.EndSuccess);
					} else if (state == 2){
						refreshState();
						info(Alerts.WrongConfig);
					} {
						refreshState();
						info(Alerts.EndFailure);
					}
				});
			info(Alerts.Busy);
		} else {
			showRunningState();
			info(Alerts.AlreadyBusy);
		}
	};

	var stopModeling = function() {
		var state = remoteState();
		if (state == 0) {
			info(Alerts.Stopping);
			var result = remoteStop();
			if (result == 0) {
				showStoppedState();
				info(Alerts.StopSuccess);
			} else {
				refreshState();
				info(Alerts.StoppedFailure);
			}
		} else {
			showStoppedState();
			info(Alerts.AlreadyStopped);
		}
	};

	var refreshState = function() {
		var state = remoteState();
		if (state == 0) {
			showStoppedState();
			info(Alerts.Ready);
			return false;
		} else {
			showRunningState();
			info(Alerts.Busy);
			return true;
		}
	};

	// only for debug
	var resetModeling = function() {
		$.ajax({
			type: "GET",
			async: false,
			url: "srv/control/reset",
			dataType: "json"
		});
	};

	var validate = function () {
		return validateMessage()
			&& true;
	};

	var remoteState = function() {
		var result;
		$.ajax({
			type: "GET",
			async: false,
			url: "srv/control/state",
			dataType: "json"
		}).done(function (state) {
			result = state;
		});
		return result;
	};

	var remoteStop = function() {
		var result;
		$.ajax({
			type: "GET",
			async: false,
			url: "srv/control/stop",
			dataType: "json"
		}).done(function(status) {
			result = state;
		});
		return result;
	};

	var remoteRunAsync = function(config, callback) {
		$.ajax({
			type: "POST",
			url: "srv/control/run",
			data: JSON.stringify(config),
			contentType: "application/json",
			dataType: "json"
		})
		.done(callback);
		showRunningState();
	};

	var showRunningState  = function() {
		$(".modeling-running").show();
		$(".modeling-stopped").hide();
	};

	var showStoppedState = function () {
		$(".modeling-running").hide();
		$(".modeling-stopped").show();
	};

	var info =  function(message) {
		var $message = $("#modeling-info");
		if (message)
			$message.text(message).show();
		else
			$message.hide();
	};
	
	var onload = function() {
		$("#modeling-run").click(runModeling);
		$("#modeling-refresh").click(refreshState);
		$("#modeling-stop").click(stopModeling);
		view.onload();
	};
	
	var load = function() {
		refreshState();
	};
	
	var tryLoad = function(target) {
		var possibleTargets = [
			"modeling",
			"modeling-message", 
			"modeling-technology", 
			"modeling-schedule", 
			"modeling-start"
		];
		if (_.contains(possibleTargets, target)) {
			load();
			return true;
		} else {
			return false;
		}
	};
	
	return {
		onload: onload,
		load: load,
		tryLoad: tryLoad
	};
})();
