/**
 * @author Alexey Tronov
 */
function Alerts() {}
Alerts.Ready = "Ready for modeling";
Alerts.Busy = "Modeling in progress ...";
Alerts.AlreadyBusy = "Modeling already in progress. Cannot be run twice.";
Alerts.EndSuccess = "Modeling has ended successfully";
Alerts.EndFailure = "Modeling hasn't ended successfully";
Alerts.StopSuccess = "Modeling has stopped successfully";
Alerts.StoppedFailure = "Modeling hasn't stopped successfully";
Alerts.AlreadyStopped = "Modeling already stopped. Cannot stop twice.";
Alerts.WrongConfig = "Wrong configuretion. Correct and try again";
Alerts.Stopping = "Stopping ...";