/**
 * @author atronov
 */
google.load("visualization", "1", {packages:["corechart"]});


result = (function () {
	
	var currentNumber = null;
	
	var loadResult = function (number) {
		if (number)
			var result = requestResult(number);
		else
			var result = requestLastResult();
		data = result;
		showMain(result);
		drawChart(result);
		showNodes(result);
		showMessage(result);
	};

	var requestResult = function (number) {
		var result;
		$.ajax({
			type: "GET",
			async: false,
			url: "srv/view/result/"+number,
			contentType: "application/json",
			dataType: "json"
		}).done(function(response) {
			result = response;
		});
		return result;
	};
	
	var requestLastResult = function () {
		console.log("requestLastResult not implemented");
		var result;
		$.ajax({
			type: "GET",
			async: false,
			url: "srv/view/result/last",
			contentType: "application/json",
			dataType: "json"
		}).done(function(response) {
			result = response;
		});
		return result;
	};

	var warning = function(message) {
		if (message) {
			$("#history-warning").text(message).show();
		} else {
			$("#history-warning").text("").hide();
		}
	};

	var showMain = function(result) {
		// fill timing
		var timePattern = "mm/dd/yy HH:MM:ss";
		var startTime = new Date(result.startTime).format(timePattern);
		var endTime = new Date(result.endTime).format(timePattern);
		var totalTime = result.endTime - result.startTime;
		$("#result-start-time").text(startTime);
		$("#result-end-time").text(endTime);
		$("#result-time-total").text(totalTime);
		// fill config
		$("#result-config-tech").text(result.config.technology);
		$("#result-config-topo").text(result.config.topology);
		$("#result-config-protocol").text(result.config.protocol);
		$("#result-config-data").text(result.config.dataType);
		$("#result-config-ssl").text((result.config.ssl)? "yes": "no");
		if (result.config.topology == "TOPIC") {
			var subscribers = result.traffic.length - 1;
			$("#result-config-subscribers").text(subscribers).parent().show();
		} else
			$("#result-config-subscribers").text("").parent().hide();
		// fill statistic
		var bytes = result.bytes;
		var messagesList = _.values(result.messagesReceived);
		var allMssages = _.reduce(messagesList, function(memo, num){ return memo + num; }, 0);
		var receivers = _.values(result.messagesReceived).length - 1;
		messages = Math.round(allMssages / receivers);
		var avgMsgLength = bytes/messages;
		var packets = result.packets;
		var avgPacketLength = bytes/packets;
		var avgMessageTime = totalTime/messages;
		$("#result-bites-total").text(bytes);
		$("#result-messages-transferred").text(messages);
		$("#result-packets-transferred").text(packets);
		$("#result-message-length").text(avgMsgLength.toFixed(2));
		$("#result-packet-length").text(avgPacketLength.toFixed(2));
		$("#result-message-time").text(avgMessageTime.toFixed(2));
	};

	var showMessage = function(result) {
		var messageJson = JSON.stringify(result.message, null, "\t");
		$("#result-message-content").text(messageJson);
		prettyPrint();
	};

	var drawChart = function (result) {
		var packets = [];
		_.values(result.traffic).forEach(function(hostPackets) {
			 packets = packets.concat(hostPackets);
		});
		var histoData = [["packet","size"]].concat(_.map(packets, function(packet) {
	    	 return ["packet x", packet.length];
	   	}));
	    var data = google.visualization.arrayToDataTable(histoData);
	    var options = {
	      title: Labels.PacketLength,
	      legend: { position: 'none' },
	      colors: ['green'],
	      vAxis : { title: Labels.Packets},
	      hAxis : {title: Labels.Length }
	    };
	
	    var chart = new google.visualization.Histogram(document.getElementById('chart_div'));
	    chart.draw(data, options);
	};
	
	var onload = function() {
		$("#result-warning").hide();
	};
	
	var load = function(number) {
		loadResult();
	};
	
	var sumTraffic = function(traficList) {
		var sum = _.reduce(traficList, function(sum, el){ return sum + el.length; }, 0);
		return sum;
	};
	
	var showNodes = function(result) {
		var $nodeTable = $("#result-nodes .table");
		var nodes = _.keys(result.traffic);
		nodes.forEach(function(node) {
			var trafficSum = sumTraffic(result.traffic[node]);
			var sent = result.messagesSent[node];
			var received = result.messagesReceived[node];
			var $nodeItem = $("<tr>")
				.append($("<td>").text(node))
				.append($("<td>").text(trafficSum))
				.append($("<td>").text(sent))
				.append($("<td>").text(received));
			$nodeTable.append($nodeItem);
		});
	};
	
	var tryLoad = function(target) {
		var resultRegex = "^result(-[a-z]+)?(\\d+)?$";
		if (target.match(resultRegex)) {
			var number = target.match(resultRegex)[2];
			window.location.hash = "#"+target;
			if (number)
				currentNumber = number;
			else
				if (currentNumber)
					window.location.hash += currentNumber;
				else
					console.error("Invalid hash, result number required");
			loadResult(currentNumber);
			$("#result-nav").show();
			return true;
		} else {
			$("#result-nav").hide();
			return false;
		}
	};
	
	return {
		onload: onload,
		load: loadResult,
		tryLoad: tryLoad
	};
})();
