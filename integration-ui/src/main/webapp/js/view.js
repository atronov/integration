/**
 * @author atronov
 */
function ModelingView() {
	var requestPossibleTechs = function() {
		var technologies;
		$.ajax({
			type: "GET",
			async: false,
			url: "srv/view/technologies",
			contentType: "application/json",
			dataType: "json"
		}).done(function(response) {
			technologies = response;
		});
		return technologies;
	};
	
	var possibleTechnologies = requestPossibleTechs();

	var onload = function() {
		fillTech();
		$("#modeling-technology select, #modeling-technology :checkbox").change(validateTech);
		$("#config-topolpgy").change(onChangeTopology).trigger("change");
		setDefaultTech();
		$('.selectpicker').selectpicker();
	};
	
	var onChangeTopology = function () {
		var $subscribers = $("#config-subscribers").add("label[for='config-subscribers']");
		var topology = $(this).find(":selected").text();
		if (topology.toUpperCase() == 'TOPIC')
			$subscribers.show();
		else
			$subscribers.hide();
	};

	var fillTech = function () {
		var callback = function(config) {
			var $techList = $("#config-tech");
			var $protList = $("#config-protocol");
			var $dataList = $("#config-data");
			var $topoList = $("#config-topolpgy");
			var $subscribers = $("#config-subscribers");
			for (var tech in config.technologies)
				$techList.append($("<option>").text(config.technologies[tech]));
			for (var prot in config.protocols)
				$protList.append($("<option>").text(config.protocols[prot]));
			for (var topo in config.topologies)
				$topoList.append($("<option>").text(config.topologies[topo]));
			for (var dat in config.dataTypes)
				$dataList.append($("<option>").text(config.dataTypes[dat]));
			$subscribers.attr("max",config.maxSubscribers);
			$subscribers.attr("placeholder", "max "+config.maxSubscribers);
		};
		requestConfig(callback);
	};

	var requestConfig = function(callback) {
		$.ajax({
			type: "GET",
			async: false,
			url: "srv/view/config",
			contentType: "application/json",
			dataType: "json"
		}).done(callback);
	};

	var setDefaultTech = function() {
		if (!possibleTechnologies) {
			console.log("no possible tech loaded, do nothing");
			return;
		}
		var tech = possibleTechnologies[0];
		$("#config-tech")
			.find("option:contains("+tech.technology+")").prop("selected",true);
		$("#config-protocol")
			.find("option:contains("+tech.protocol+")").prop("selected",true);
		$("#config-data")
			.find("option:contains("+tech.dataType+")").prop("selected",true);
		$("#config-topolpgy")
			.find("option:contains("+tech.topology+")").prop("selected",true);
	};

	var validateTech = function() {
		if (!possibleTechnologies) {
			console.log("no possible tech loaded, do nothing");
			return;
		}
		var currentTech = {
			technology: $("#config-tech option:selected").text(),
			protocol: $("#config-protocol option:selected").text(),
			dataType: $("#config-data option:selected").text(),
			ssl: $("#config-secure").prop("checked"),
			topology: $("#config-topolpgy option:selected").text()
		};
		if (_.any(possibleTechnologies,function(tech) { return compareTech(tech,currentTech); })) {
			$("#technology-message").text("").hide();
		} else {
			$("#technology-message").text("Unapplicable configuration").show();
		}
	};

	var compareTech = function (tech1, tech2) {
		return tech1.technology == tech2.technology
			&& tech1.protocol == tech2.protocol
			&& tech1.dataType == tech2.dataType
			&& tech1.ssl == tech2.ssl
			&& tech1.topology == tech2.topology;
	};
	
	return {
		onload: onload
	};
}

