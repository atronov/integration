/**
 * @author atronov
 */
$(document).ready(function() {
	modeling.onload();
	resultHistory.onload();
	result.onload();
	navigation.onload();
});

navigation = (function() {
	
	// each module should have tryLoad and load method
	var modules = [result, modeling, resultHistory];
	
	var onload = function() {
		$("nav a").click(handleNavigate);
		//$("nav a").not("[href='#']").forEach(function(a) {
		//	var id = $(a).prop("href").substr(1);
		//	$("#"+id).prepend($("<span>").addClass("fake-anchor"))
		//	.
		//});
		navigateOnLoad();
	};
	
	var handleNavigate = function() {
		var target = "modeling";
		var targetHref = $(this).attr("href");
		if (targetHref)
			targetHref = targetHref.trim();
			var hashIndex = targetHref.indexOf("#");
			if (hashIndex == 0) {
				target = targetHref.substr(1);
		}
		goTo(target);
	};
	
	var goTo = function(target) {
		if (target) {
			var targetSelector = "#"+target.match("^[A-Za-z]+")[0];
			var $toShow = $(targetSelector).closest(".main>section");
			var $toHide = $(".main>section").not($toShow);
			$toHide.hide();
			$toShow.show();
			for (var i in modules) {
				if (modules[i].tryLoad(target))
					return;
			}
			console.error("Navigation targer doesn't match to no module");
		}
	};
	
	var navigateOnLoad = function() {
		var target = "modeling";
		if (document.location.hash && document.location.hash.trim().substr(1)) {
			target = document.location.hash.substr(1);
		}
		goTo(target);
	};
	
	var isResult = function(target) {
		return target.match("^result\\d$");
	};
	
	var loadResult = function(target) {
		var resultNumber = target.substr(6);
		result.load(resultNumber);
	};
	
	return {
		goTo: goTo,
		onload: onload
	};
})();
