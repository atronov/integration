/**
 * @author atronov
 */
resultHistory = (function() {
	var onload = function() {
		$("#history-refresh").click(loadHistory);
	};
	
	var loadHistory = function() {
		var history = requestHistory();
		var $table = $("#history table");
		$table.find("tr").not(".table-header").remove();
		var sortedHistory = _.sortBy(history, function(el){ return el.time; });
		for (var i = 0; i < sortedHistory.length; i++) {
			var numb = i + 1;
			var item = sortedHistory[i];
			item.order = numb;
			$table.appendRow(item);
		}
	};
	
	var requestHistory = function() {
		var history;
		$.ajax({
			type: "GET",
			async: false,
			url: "srv/view/result/history",
			contentType: "application/json",
			dataType: "json"
		}).done(function(response) {
			history = response;
		});
		return history;
	};
	
	$.fn.appendRow = function(item) {
		var time = new Date(item.time).format("mm/dd/yy HH:MM:ss");
		var number = item.number;
		var $sslRow = $("<td>");
		if (item.config.ssl)
			$sslRow.addClass("text-success")
			.append($("<span>").addClass("glyphicon glyphicon-ok"));
		var $row = $("<tr>")
			.append($("<td>").text(item.order))
			.append($("<td>").append($("<time>").text(time)))
			.append($("<td>").text(item.config.technology))
			.append($("<td>").text(item.config.topology))
			.append($("<td>").text(item.config.dataType))
			.append($sslRow)
			.append($("<td>").text(item.messages))
			.append($("<td>").text(item.bytes))
			.append($("<td>").text(item.packets))
			.append($("<td>").addClass("result-number").text(number).hide());
		$row.dblclick(toResult);
		$(this).append($row);
	};
	
	var toResult = function () {
		var number = $(this).find(".result-number").text();
		navigation.goTo("result"+number);
	};
	
	var tryLoad = function(target) {
		if (target == "history") {
			loadHistory();
			return true;
		} else
			return false;
	};
	
	return {
		onload: onload,
		tryLoad: tryLoad,
		load: loadHistory
	};
})();

