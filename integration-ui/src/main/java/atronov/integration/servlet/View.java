package atronov.integration.servlet;

import java.io.File;
import java.io.IOException;
import java.lang.reflect.Type;
import java.util.Collections;
import java.util.Comparator;
import java.util.LinkedList;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import org.apache.commons.io.FileUtils;
import org.apache.log4j.Logger;

import atronov.integration.config.NodeConfig;
import atronov.integration.message.IMessage;
import atronov.integration.run.build.BuildConfig;
import atronov.integration.run.build.DataType;
import atronov.integration.run.build.MappingBuilder;
import atronov.integration.run.build.Protocol;
import atronov.integration.run.build.Technology;
import atronov.integration.run.build.Topology;
import atronov.integration.run.remoting.ModelRunResult;

import com.google.common.collect.Lists;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.JsonDeserializationContext;
import com.google.gson.JsonDeserializer;
import com.google.gson.JsonElement;
import com.google.gson.JsonParseException;


@Path("view")
public class View {
	private static final String RESULT_PATTERN = "result(\\d+)\\.json";
	
	private static Logger log = Logger.getLogger(View.class);

	@GET
	@Path("config")
	public Response getConfigView() {
		ConfigView configView = new ConfigView();
		configView.protocols = Protocol.values();
		configView.technologies = Technology.values();
		configView.topologies = Topology.values();
		configView.dataTypes = DataType.values();
		configView.maxSubscribers = NodeConfig.get().nodes().length - 1;
		Gson gson = new Gson();
		String configJson = gson.toJson(configView);
		return Response.ok(configJson, MediaType.APPLICATION_JSON).build();
	}
		
	public static class ConfigView {
		public Protocol[] protocols;
		public Technology[] technologies;
		public Topology[] topologies;
		public DataType[] dataTypes;
		public int maxSubscribers;
	}
	
	@GET
	@Path("technologies")
	public Response possibleTechs() {
		Gson gson = new Gson();
		List<BuildConfig> mapping = MappingBuilder.createPossible();
		String mappingJson = gson.toJson(mapping);
		return Response.ok(mappingJson, MediaType.APPLICATION_JSON).build();
	}
	
	@GET
	@Path("result/{resultNumber: [0-9]+}")
	public Response getResult(@PathParam("resultNumber") String number) {
		final String resultFilePattern = "result/result%s.json";
		final String resultFileName = String.format(resultFilePattern, number);
		log.info("Request result from file "+resultFileName);
		File resultFile = new File(resultFileName);
		if (resultFile.exists()) {
			try {
			String result = FileUtils.readFileToString(resultFile);
			return Response.ok(result, MediaType.APPLICATION_JSON).build();
			} catch (IOException e) {
				log.error("Errow while reading file result file "+resultFileName, e);
				return Response.status(Response.Status.INTERNAL_SERVER_ERROR).build();
			}
		} else {
			return Response.status(Response.Status.NOT_FOUND).build();
		}
	}
	
	@GET
	@Path("result/history")
	public Response getResultTable() throws IOException {
		log.info("Collect result table");
		List<ResultTableItem> resultItems = new LinkedList<>();
		File resultDir = new File("result");
		for (File resultFile : resultDir.listFiles()) {
			if (isResult(resultFile)) {
				ModelRunResult result = readResult(resultFile);
				ResultTableItem item = resultToResultItem(result);
				item.number = extractNumber(resultFile);
				resultItems.add(item);
			} else
				log.info(String.format("File %s skipped, as it is not result file",resultFile));
		}
		Collections.sort(resultItems, new Comparator<ResultTableItem>() {
			public int compare(ResultTableItem o1, ResultTableItem o2) {
				return (int)(o1.time - o2.time);
			}
		} );
		Gson gson = new Gson();
		String resultTableJson = gson.toJson(resultItems);
		return Response.ok(resultTableJson, MediaType.APPLICATION_JSON).build();
	}
	
	private String extractNumber(File resultFile) {
		Pattern pattern = Pattern.compile(RESULT_PATTERN);
		Matcher matcher = pattern.matcher(resultFile.getName());
		if(matcher.find()){
			String number = matcher.group(1);
			return number;
		} else {
			log.error("Cannot extract result number from file: "+resultFile);
			return null;
		}
	}
	
	private static boolean isResult(File resultFile) {
		String name = resultFile.getName();
		return name.matches(RESULT_PATTERN);
	}
	
	private ResultTableItem resultToResultItem(ModelRunResult result) {
		ResultTableItem tableItem = new ResultTableItem();
		tableItem.time = result.getStartTime();
		tableItem.messages = 0;
		if (!result.getMessageReceived().isEmpty()) {
			int totalMessages = 0;
			int receivers = result.getMessageReceived().keySet().size();
			for (int el : result.getMessageReceived().values())
				totalMessages += el;
			tableItem.messages = (int)Math.round((double)totalMessages / receivers);
		}
		tableItem.bytes = result.getBytes();
		tableItem.config = result.getConfig();
		tableItem.packets = result.getPackets();
		return tableItem;
	}
	

//	@GET
//	@Path("result/statistic")
//	public Response getStatistic() throws IOException {
//		log.info("Collect statistic");
//		throw new NotIm
//	}
	
	private ModelRunResult readResult(File resultFile) {
		log.info("Extract result from file "+resultFile);
		GsonBuilder builder = new GsonBuilder();
		builder.registerTypeAdapter(IMessage.class, new JsonDeserializer<IMessage>(){
			public IMessage deserialize(JsonElement json, Type typeOfT, JsonDeserializationContext context) throws JsonParseException {
				// exclude this field from deserialization
				return null;
			}});
		Gson gson = builder.create();
		String resultJson = null;
		try {
			resultJson = FileUtils.readFileToString(resultFile);
		} catch (IOException e) {
			log.error("Error while reading result file "+resultFile, e);
			return null;
		}
		ModelRunResult result = gson.fromJson(resultJson, ModelRunResult.class);
		return result;
	}
	
	public static class ResultTableItem {
		public long time;
		public long bytes;
		public int messages;
		public int packets;
		public String number;
		public BuildConfig config;
	}
	
}
