package atronov.integration.servlet.config;

import atronov.integration.message.ICustomMessage;
import atronov.integration.run.build.BuildConfig;

public class RunConfig {
	
	public ICustomMessage message;
	public BuildConfig buildConfig;
	public Shedule shedule;
	public int subscribers;
	
	public static class Shedule {
		public long duration;
		public long step;
	}
}
