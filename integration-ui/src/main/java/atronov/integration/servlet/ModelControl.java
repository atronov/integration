package atronov.integration.servlet;

import java.io.File;
import java.io.IOException;
import java.lang.reflect.Type;

import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import org.apache.commons.io.FileUtils;
import org.apache.log4j.Logger;

import atronov.integration.config.WrongModelConfigException;
import atronov.integration.message.ICustomMessage;
import atronov.integration.message.TypeRecognition;
import atronov.integration.message.json.recognition.CustomRecognition;
import atronov.integration.run.build.Technology;
import atronov.integration.run.build.Topology;
import atronov.integration.run.remoting.Facade;
import atronov.integration.run.remoting.ModelFacede;
import atronov.integration.run.remoting.ModelRunResult;
import atronov.integration.servlet.config.RunConfig;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.JsonDeserializationContext;
import com.google.gson.JsonDeserializer;
import com.google.gson.JsonElement;
import com.google.gson.JsonParseException;

@Path("control")
public class ModelControl {

	private volatile static Facade model = null;

	private static Logger log = Logger.getLogger(ModelControl.class);

	/**
	 * @return 0 - ready, 1 - busy
	 */
	@Path("state")
	@GET
	public Response getStatus() {
		int status = (model != null) ? 1 : 0;
		return Response.ok(status).build();
	}
	
	@Path("reset")
	@GET
	public Response reset() {
		log.info("Reset model");
		model = null;
		return Response.ok().build();
	}

	/**
	 * @return 0 - stopped successfully, 1 - already stopped
	 */
	@Path("stop")
	@GET
	public Response stop() {
		boolean success = stopModeling();
		int status = (success) ? 0 : 1;
		return Response.ok(status).build();
	}

	/**
	 * @return 0 - modeling completed successfully, 1 - modeling completed with issue, 2 - wrong config
	 */
	@Path("run")
	@POST
	@Consumes(MediaType.APPLICATION_JSON)
	public Response run(String runConfig) {
		log.info("Received config:" + runConfig);
		RunConfig config = parseConfig(runConfig);
		log.info("Config parsed successfully");
		int status;
		try {
			boolean success = run(config);
			status = (success) ? 0 : 1;
		} catch (WrongModelConfigException e) {
			log.error(e);
			status = 2;
		}
		return Response.ok(status).build();
	}

	private synchronized boolean startModeling(RunConfig config) {
		if (model == null) {
			synchronized (this.getClass()) {
				if (model == null) {
					log.info("Model is free. It can be run.");
					model = new ModelFacede();
					model.setResourceConfig(config.buildConfig);
					model.setIterationTime(config.shedule.step);
					model.setMessageToSend(config.message);
					if (config.buildConfig.getTopology() == Topology.TOPIC)
						model.setSubscribers(config.subscribers);
					model.start();
					return true;
				}
			}
		}
		log.info("Model cannot be run as it's busy");
		return false;
	}

	private synchronized boolean stopModeling() {
		if (model != null) {
			synchronized (this.getClass()) {
				if (model != null) {
					log.info("Model is running. Stop it.");
					model.stop();
					saveResult();
					model = null;
					return true;
				}
			}
		}
		log.info("Model is currently stopped. No action is needed.");
		return false;
	}

	private boolean run(RunConfig config) {
		if (startModeling(config)) {
			try {Thread.sleep(config.shedule.duration);} catch (InterruptedException e) {}
			return stopModeling();
		} else {
			return false;
		}
	}

	private boolean saveResult() {
		if (model == null) {
			log.error("Model is null. Result cannot be saved.");
			return false;
		}
		final String fileNamePattern = "result/result%s.json";
		String fileName = String.format(fileNamePattern, System.currentTimeMillis());
		log.info("Save run result as file " + fileName);
		ModelRunResult result = model.getResult();
		String resultJson = new Gson().toJson(result);
		try {
			FileUtils.writeStringToFile(new File(fileName), resultJson);
			return true;
		} catch (IOException e) {
			log.error("Error while writing result to json file", e);
			return false;
		}
	}

	private static RunConfig parseConfig(String configJson) {
		log.info("Parse received config");
		GsonBuilder builder = new GsonBuilder();
		builder.registerTypeAdapter(ICustomMessage.class, new JsonDeserializer<ICustomMessage>() {
			public ICustomMessage deserialize(JsonElement json, Type typeOfT, JsonDeserializationContext context) throws JsonParseException {
				String messageJsonString = json.toString();
				TypeRecognition<String> recognition = new CustomRecognition();
				Class<?> messageType = recognition.recognize(messageJsonString);
				ICustomMessage message = context.deserialize(json, messageType);
				return message;
			}
		});
		Gson gson = builder.create();
		RunConfig config = null;
		try {
			config = gson.fromJson(configJson, RunConfig.class);
		} catch (Exception e) {
			log.info("Message cannot be cast to Custom list message");
			log.error("Cannot parse config", e);
			throw e;
		}
		return config;
	}

}
