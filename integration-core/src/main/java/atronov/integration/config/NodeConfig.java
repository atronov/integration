package atronov.integration.config;

import atronov.integration.message.simple.ArrayConverter;
import ru.yandex.qatools.properties.annotations.Property;
import ru.yandex.qatools.properties.annotations.Resource;
import ru.yandex.qatools.properties.annotations.Use;

@Resource.Classpath("/node.properties")
public class NodeConfig {
	private volatile static NodeConfig instance = null;
	
	private NodeConfig() {
		ConfigLoader.populate(this);
	}
	
	public static NodeConfig get() {
		if (instance == null) {
			synchronized (NodeConfig.class) {
				if (instance == null) {
					instance = new NodeConfig();
				}
			}
		}
		return instance;
	}
	@Property("node.host")
	private String host; 
	@Property("node.ssh.password")
	private String sshPassword;
	@Property("node.ssh.user")
	private String sshUser;
	@Use(ArrayConverter.class)
	@Property("node.list")
	private String[] nodes;

	public String host() {
		return host;
	}

	public String sshPassword() {
		return sshPassword;
	}

	public String sshUser() {
		return sshUser;
	}

	public String[] nodes() {
		return nodes;
	}
}
