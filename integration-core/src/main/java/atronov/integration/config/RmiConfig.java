package atronov.integration.config;

import ru.yandex.qatools.properties.PropertyLoader;
import ru.yandex.qatools.properties.annotations.Property;
import ru.yandex.qatools.properties.annotations.Resource;

@Resource.Classpath("/rmi.properties")
public class RmiConfig {
	
	private volatile static RmiConfig instance = null;
	
	private RmiConfig() {
		ConfigLoader.populate(this);
	}
	
	public static RmiConfig get() {
		if (instance == null) {
			synchronized (RmiConfig.class) {
				if (instance == null) {
					instance = new RmiConfig();
				}
			}
		}
		return instance;
	}
	
	
	@Property("rmi.port")
	private int port;

	@Property("rmi.secure.port")
	private int securePort;
	
	@Property("rmi.host")
	private String host;

	public static RmiConfig getInstance() {
		return instance;
	}

	public int getPort() {
		return port;
	}

	public int getSecurePort() {
		return securePort;
	}

	public String getHost() {
		if (System.getProperty("rmi.host") != null)
			return System.getProperty("rmi.host");
		return host;
	}
}
