package atronov.integration.config;

public class WrongModelConfigException extends RuntimeException {

	public WrongModelConfigException(String message) {
		super(message);
	}
	
	private static final long serialVersionUID = 1060441385617237870L;

}
