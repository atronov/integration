package atronov.integration.config;

import ru.yandex.qatools.properties.PropertyLoader;
import ru.yandex.qatools.properties.annotations.Property;
import ru.yandex.qatools.properties.annotations.Resource;

@Resource.Classpath("/server.properties")
public class HttpServerConfig {
	
	private volatile static HttpServerConfig instance = null;
	
	private HttpServerConfig() {
		ConfigLoader.populate(this);
	}
	
	public static HttpServerConfig get() {
		if (instance == null) {
			synchronized (HttpServerConfig.class) {
				if (instance == null) {
					instance = new HttpServerConfig();
				}
			}
		}
		return instance;
	}
	
	@Property("http.server.host")
	private String host;
	
	@Property("http.server.port")
	private int port;
	
	@Property("http.server.secure.port")
	private int securePort;

	public String host() {
		if (System.getProperty("http.server.host") != null)
			return System.getProperty("http.server.host");
		return host;
	}

	public int port() {
		if (System.getProperty("http.server.port") != null)
			return Integer.parseInt(System.getProperty("http.server.port"));
		return port;
	}

	public int securePort() {
		if (System.getProperty("http.server.secure.port") != null)
			return Integer.parseInt(System.getProperty("http.server.secure.port"));
		return securePort;
	}
}
