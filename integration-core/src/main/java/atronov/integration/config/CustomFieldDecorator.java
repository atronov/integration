package atronov.integration.config;

import org.apache.commons.beanutils.*;
import org.apache.log4j.Logger;

import ru.yandex.qatools.properties.annotations.Property;
import ru.yandex.qatools.properties.annotations.Use;
import ru.yandex.qatools.properties.converters.URIConverter;
import ru.yandex.qatools.properties.decorators.FieldDecorator;

import java.lang.reflect.Field;
import java.net.URI;
import java.util.Properties;

public class CustomFieldDecorator implements FieldDecorator {

		private static Logger log = Logger.getLogger(ConfigLoader.class);
	
    private final ConvertUtilsBean converters;

    private Properties properties;

    public CustomFieldDecorator(Properties properties) {
        this.converters = BeanUtilsBean.getInstance().getConvertUtils();
        this.converters.register(true, false, -1);
        this.converters.register(new URIConverter(), URI.class);
        this.properties = properties;
    }

    @Override
    public Object decorate(Field field) {
        if (!field.isAnnotationPresent(Property.class)) {
        	log.debug("Annotation wasnt found");
            return null;
        }

        String key = field.getAnnotation(Property.class).value();

    		log.debug("Key="+key);
        if (key == null || "".equals(key)) {
        		log.debug("Key wasnt found");
            return null;
        }

        String value = properties.getProperty(key);

    		log.debug("value="+value);
        if (value == null || "".equals(value)) {
      			log.debug("value is empty");
            return null;
        }

        Converter converter = getConverterFrom(field);
        log.debug("converter="+converter);
        if (converter == null) {
    				log.debug("Converter wasnt found");
            return null;
        }

        try {
            return converter.convert(field.getType(), properties.getProperty(key));
        } catch (ConversionException e) {
        		log.error("Conversion error: ", e);
            return null;
        }
    }

    private Converter getConverterFrom(Field field) {
        if (field.isAnnotationPresent(Use.class)) {
            return createNewInstanceFromUseAnnotations(field);
        } else {
            return converters.lookup(field.getType());
        }
    }

    private Converter createNewInstanceFromUseAnnotations(Field field) {
        try {
            return field.getAnnotation(Use.class).value().newInstance();
        } catch (Exception e) {
            return null;
        }
    }
}