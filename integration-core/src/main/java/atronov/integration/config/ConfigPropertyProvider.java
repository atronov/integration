package atronov.integration.config;

import static ru.yandex.qatools.properties.utils.PropertiesUtils.readProperties;

import java.util.Properties;

import ru.yandex.qatools.properties.annotations.Resource;
import ru.yandex.qatools.properties.providers.DefaultPropertyProvider;

public class ConfigPropertyProvider extends DefaultPropertyProvider {
	
  @Override
  public <T> Properties provide(T bean, Properties properties) {
      Class<?> clazz = bean.getClass();

      if (have(clazz, Resource.Classpath.class)) {
          String path = classpath(clazz, properties);
          properties.putAll(readProperties(clazz.getResourceAsStream(path)));
      }

      if (have(clazz, Resource.File.class)) {
          String path = filepath(clazz, properties);
          properties.putAll(readProperties(new java.io.File(path)));
      }

      properties.putAll(System.getProperties());
      return properties;
  }

}
