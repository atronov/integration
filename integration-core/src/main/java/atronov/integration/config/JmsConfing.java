package atronov.integration.config;

import ru.yandex.qatools.properties.PropertyLoader;
import ru.yandex.qatools.properties.annotations.Property;
import ru.yandex.qatools.properties.annotations.Resource;

@Resource.Classpath("/jms.properties")
public class JmsConfing {
	
	private volatile static JmsConfing instance = null;
	
	private JmsConfing() {
		ConfigLoader.populate(this);
	}
	
	public static JmsConfing get() {
		if (instance == null) {
			synchronized (JmsConfing.class) {
				if (instance == null) {
					instance = new JmsConfing();
				}
			}
		}
		return instance;
	}
	
	@Property("jms.host.name")
	private String hostName;

	@Property("jms.host")
	private String host;

	@Property("jms.host.user")
	private String hostUser;

	@Property("jms.host.passw")
	private String hostPassword;

	@Property("jms.host.root.passw")
	private String hostRootPassword;

	@Property("jms.host.ssh.port")
	private int hostShhPort;
	
	@Property("jms.queue")
	private String queue;
	
	@Property("jms.topic")
	private String topic;

	@Property("jms.http.jndi.port")
	private int httpJndiPort;

	@Property("jms.http.factory")
	private String httpFactory;

	@Property("jms.tcp.jndi.port")
	private int tcpJndiPort;
	
	@Property("jms.tcp.factory")
	private String tcpFactory;
			
	@Property("jms.tcp.ssl.factory")
	private String tcpSslFactory;

	public String getJmsHostName() {
		if (System.getProperty("jms.host.name") != null)
			return System.getProperty("jms.host.name");
		return hostName;
	}

	public String jmsHost() {
		if (System.getProperty("jms.host") != null)
			return System.getProperty("jms.host");
		return host;
	}

	public String jmsHostUser() {
		return hostUser;
	}

	public String jmsHostPassword() {
		return hostPassword;
	}

	public String jmsHostRootPassword() {
		return hostRootPassword;
	}

	public int jmsHostShhPort() {
		return hostShhPort;
	}

	public String jmsQueue() {
		return queue;
	}

	public String jmsTopic() {
		return topic;
	}

	public int jmsHttpJndiPort() {
		return httpJndiPort;
	}

	public String jmsHttpFactory() {
		return httpFactory;
	}

	public int jmsTcpJndiPort() {
		return tcpJndiPort;
	}

	public String jmsTcpFactory() {
		return tcpFactory;
	}

	public String jmsTcpSslFactory() {
		return tcpSslFactory;
	}
	
}
