package atronov.integration.config;

import java.lang.reflect.Field;
import java.util.Properties;

import org.apache.commons.beanutils.Converter;
import org.apache.log4j.Logger;

import ru.yandex.qatools.properties.annotations.Property;
import ru.yandex.qatools.properties.annotations.Use;
import ru.yandex.qatools.properties.annotations.With;
import ru.yandex.qatools.properties.decorators.DefaultFieldDecorator;
import ru.yandex.qatools.properties.decorators.FieldDecorator;
import ru.yandex.qatools.properties.exeptions.PropertyLoaderException;
import ru.yandex.qatools.properties.providers.PropertyProvider;

public class ConfigLoader {

	private static Logger log = Logger.getLogger(ConfigLoader.class);

	private ConfigLoader() {
	}

	public static <T> void populate(T bean) {
		populate(bean, new Properties());
	}

	public static <T> void populate(T bean, Properties properties) {
		PropertyProvider provider = instantiatePropertyProvider(bean);
		Properties completed = provider.provide(bean, properties);
		log.debug("We loaded properties: " + completed);
		populate(bean, new CustomFieldDecorator(completed));
	}

	public static <T> void populate(T bean, FieldDecorator decorator) {
		Class<?> clazz = bean.getClass();
		while (clazz != Object.class) {
			initFields(decorator, bean, clazz);
			clazz = clazz.getSuperclass();
		}
	}

	private static void initFields(FieldDecorator decorator, Object bean, Class<?> clazz) {
      Field[] fields = clazz.getDeclaredFields();
      for (Field field : fields) {
      		log.debug("try to set field " + field.getName());
          Object value = decorator.decorate(field);
          if (value != null) {
              try {
              	  log.info(field.getName()+"="+value);
                  field.setAccessible(true);
                  field.set(bean, value);
              } catch (IllegalAccessException e) {
                  throw new PropertyLoaderException(
                          String.format("Can not set bean <%s> field <%s> value", bean, field),
                          e);
              }
          }
      }
  }

	private static <T> PropertyProvider instantiatePropertyProvider(T bean) {
		Class<?> clazz = bean.getClass();
		if (clazz.isAnnotationPresent(With.class)) {
			try {
				return clazz.getAnnotation(With.class).value().newInstance();
			} catch (InstantiationException e) {
				throw new PropertyLoaderException("Can't create instance property provider in class " + bean.getClass().getName(), e);
			} catch (IllegalAccessException e) {
				throw new PropertyLoaderException("Can't load property provider in class " + bean.getClass().getName(), e);
			}
		}
		return new ConfigPropertyProvider();
	}

}
