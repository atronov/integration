package atronov.integration.message.pojo.custom;

import java.util.HashMap;
import java.util.Map;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.google.common.collect.ImmutableMap;

public final class CustomMapElement extends CustomElement<Map<String, Object>> {

	private static final long serialVersionUID = -7999542095691747295L;

	CustomMapElement() {}
	
	@JsonProperty(required=true)
	Map<String, Object> content = new HashMap<>();
	
	@Override
	public Map<String, Object> getContent() {
		return ImmutableMap.copyOf(content);
	}
}
