package atronov.integration.message.pojo.custom;

import atronov.integration.message.ICustomMessage;

public class CustomMessageBuilder  {

	private final CustomListElement activeList;
	private final CustomMapElement activeMap;
	private final boolean listState;
	
	public boolean isMap() {
		return !listState;
	}
	
	public boolean isList() {
		return listState;
	}
	
	private CustomMessageBuilder(CustomListElement listElement) {
		listState = true;
		activeList = listElement;
		activeMap = null;
	}
	
	private CustomMessageBuilder(CustomMapElement mapElement) {
		listState = false;
		activeMap = mapElement;
		activeList = null;
	}
	
	public CustomMessageBuilder add(String text) {
		checkListAccess();
		activeList.content.add(text);
		return this;
	}
	
	public CustomMessageBuilder add(Number number) {
		checkListAccess();
		activeList.content.add(number);
		return this;
	}
	
	public CustomMessageBuilder add(Boolean flag) {
		checkListAccess();
		activeList.content.add(flag);
		return this;
	}
	
	public CustomMessageBuilder put(String key, String text) {
		checkMapAccess();
		activeMap.content.put(key, text);
		return this;
	}
	
	public CustomMessageBuilder put(String key, Number number) {
		checkMapAccess();
		activeMap.content.put(key, number);
		return this;
	}
	
	public CustomMessageBuilder put(String key, Boolean flag) {
		checkMapAccess();
		activeMap.content.put(key, flag);
		return this;
	}
	
	public CustomMessageBuilder addMapElement() {
		checkListAccess();
		CustomMapElement element = new CustomMapElement();
		activeList.content.add(element);
		CustomMessageBuilder messageBuilder = new CustomMessageBuilder(element);
		return messageBuilder;
	}
	
	public CustomMessageBuilder addListElement() {
		checkListAccess();
		CustomListElement element = new CustomListElement();
		activeList.content.add(element);
		CustomMessageBuilder messageBuilder = new CustomMessageBuilder(element);
		return messageBuilder;
	}
	
	public CustomMessageBuilder putMapElement(String key) {
		checkMapAccess();
		CustomMapElement element = new CustomMapElement();
		activeMap.content.put(key, element);
		CustomMessageBuilder messageBuilder = new CustomMessageBuilder(element);
		return messageBuilder;
	}
	
	public CustomMessageBuilder putListElement(String key) {
		checkMapAccess();
		CustomListElement element = new CustomListElement();
		activeMap.content.put(key, element);
		CustomMessageBuilder messageBuilder = new CustomMessageBuilder(element);
		return messageBuilder;
	}
	
	public void checkMapAccess() {
		if (!isMap())
			new IllegalAccessException("Current builder linked with list");
	}
	
	public void checkListAccess() {
		if (!isList())
			new IllegalAccessException("Current builder linked with map");
	}
	
	public static CustomMessageBuilder CreateForList() {
		CustomListElement element = new CustomListElement();
		return new CustomMessageBuilder(element);
	}
	
	public static CustomMessageBuilder CreateForMap() {
		CustomMapElement element = new CustomMapElement();
		return new CustomMessageBuilder(element);
	}
	
	public ICustomMessage build() {
		if (isList())
			return activeList;
		else
			return activeMap;
	}
}
