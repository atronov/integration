package atronov.integration.message.pojo;

import com.fasterxml.jackson.annotation.JsonProperty;

import atronov.integration.message.IBooleanMessage;

public class BooleanMessage extends PojoMesage implements IBooleanMessage {

	@JsonProperty(required=true)
	private boolean active;

	@Override
	public boolean isActive() {
		return active;
	}

	@Override
	public void setActive(boolean active) {
		this.active = active;
	}
}
