package atronov.integration.message.pojo;

import java.util.Map;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.databind.annotation.JsonDeserialize;

import atronov.integration.message.IMixedMessage;
import static com.google.common.base.Objects.equal;

public class MixedMessage extends PojoMesage implements IMixedMessage {

	@JsonProperty(required=true)
	private boolean active;
	@JsonProperty(required=true)
	private String text;
	@JsonProperty(required=true)
	private double amount;
	@JsonProperty(required=true)
	private int count;
	@JsonProperty(required=true)
	private String[] tags;
	@JsonProperty(required=true)
	private Map<String, String> properties;

	@Override
	public boolean isActive() {
		return active;
	}

	@Override
	public void setActive(boolean active) {
		this.active = active;
	}

	@Override
	public String getText() {
		return text;
	}

	@Override
	public void setText(String text) {
		this.text = text;
	}

	@Override
	public double getAmount() {
		return amount;
	}

	@Override
	public void setAmount(double amount) {
		this.amount = amount;
	}

	@Override
	public int getCount() {
		return count;
	}

	@Override
	public void setCount(int count) {
		this.count = count;
	}

	@Override
	public String[] getTags() {
		return tags;
	}

	@Override
	public void setTags(String[] tags) {
		this.tags = tags;
	}

	@Override
	public Map<String, String> getProperties() {
		return properties;
	}

	@Override
	public void setProperties(Map<String, String> properties) {
		this.properties = properties;
	}

	@Override
	public boolean equals(Object obj) {
		if (obj == null) {
			return false;
		}
		if (getClass() != obj.getClass()) {
			return false;
		}
		final IMixedMessage other = (IMixedMessage) obj;
		return equal(this.active, other.isActive())
				&& equal(this.count, other.getCount())
				&& equal(this.amount, other.getAmount())
				&& equal(this.text, other.getText())
				&& equal(this.tags, other.getTags())
				&& equal(this.properties, other.getProperties());
	}
}
