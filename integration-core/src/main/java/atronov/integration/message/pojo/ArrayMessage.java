package atronov.integration.message.pojo;

import com.fasterxml.jackson.annotation.JsonProperty;

import atronov.integration.message.IArrayMessage;

public class ArrayMessage extends PojoMesage implements IArrayMessage{

	@JsonProperty(required=true)
	private String[] tags;

	@Override
	public String[] getTags() {
		return tags;
	}

	@Override
	public void setTags(String[] tags) {
		this.tags = tags;
	}

}
