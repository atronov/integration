package atronov.integration.message.pojo;

import com.fasterxml.jackson.annotation.JsonProperty;

import atronov.integration.message.IIntegerMessage;

public class IntegerMessage extends PojoMesage implements IIntegerMessage {

	@JsonProperty(required=true)
	private int count;

	@Override
	public int getCount() {
		return count;
	}

	@Override
	public void setCount(int count) {
		this.count = count;
	}
}
