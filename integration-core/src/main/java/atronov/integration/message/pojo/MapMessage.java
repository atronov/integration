package atronov.integration.message.pojo;

import java.util.Map;

import com.fasterxml.jackson.annotation.JsonProperty;

import atronov.integration.message.IMapMessage;

public class MapMessage extends PojoMesage implements IMapMessage {
	
	@JsonProperty(required=true)
	private Map<String, String> properties;

	@Override
	public Map<String, String> getProperties() {
		return properties;
	}

	@Override
	public void setProperties(Map<String, String> properties) {
		this.properties = properties;
	}
}
