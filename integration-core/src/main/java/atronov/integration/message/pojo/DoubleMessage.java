package atronov.integration.message.pojo;

import com.fasterxml.jackson.annotation.JsonProperty;

import atronov.integration.message.IDoubleMessage;

public class DoubleMessage extends PojoMesage implements IDoubleMessage {

	@JsonProperty(required=true)
	private double amount;

	@Override
	public double getAmount() {
		return amount;
	}

	@Override
	public void setAmount(double amount) {
		this.amount = amount;
	}
}
