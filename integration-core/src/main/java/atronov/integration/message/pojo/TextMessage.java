package atronov.integration.message.pojo;

import com.fasterxml.jackson.annotation.JsonProperty;

import atronov.integration.message.ITextMessage;

public class TextMessage extends PojoMesage implements ITextMessage {

	@JsonProperty(required=true)
	private String text;
	
	@Override
	public String getText() {
		return text;
	}

	@Override
	public void setText(String text) {
		this.text = text;
	}

}
