package atronov.integration.message.pojo.custom;

import java.io.Serializable;

import atronov.integration.message.ICustomMessage;
import atronov.integration.message.pojo.PojoMesage;

public abstract class CustomElement<TContent> extends PojoMesage implements ICustomMessage, Serializable  {

	private static final long serialVersionUID = -7127949174774296177L;

	CustomElement() {}
	
	public abstract TContent getContent();

}
