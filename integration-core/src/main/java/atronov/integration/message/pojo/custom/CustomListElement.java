package atronov.integration.message.pojo.custom;

import java.util.ArrayList;
import java.util.List;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.google.common.collect.ImmutableList;

public final class CustomListElement extends CustomElement<List<Object>> {

	private static final long serialVersionUID = 406324365735999194L;

	CustomListElement() {}
	
	@JsonProperty(required=true)
	List<Object> content = new ArrayList<>();

	@Override
	public List<Object> getContent() {
		return ImmutableList.copyOf(content);
	}

}
