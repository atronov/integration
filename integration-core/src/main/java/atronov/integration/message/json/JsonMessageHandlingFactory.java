package atronov.integration.message.json;

import atronov.integration.message.MessageHandlingFactory;
import atronov.integration.message.Serializer;
import atronov.integration.message.TypeRecognition;
import atronov.integration.message.json.recognition.JsonRecognition;

public class JsonMessageHandlingFactory implements MessageHandlingFactory<String> {

	@Override
	public Serializer<String> createSerializer() {
		return new JsonSerializer();
	}

	@Override
	public TypeRecognition<String> createRecognition() {
		return new JsonRecognition();
	}

}
