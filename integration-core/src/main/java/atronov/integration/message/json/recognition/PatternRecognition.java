package atronov.integration.message.json.recognition;

import atronov.integration.message.IMessage;
import atronov.integration.message.TypeRecognition;

public abstract class PatternRecognition implements TypeRecognition<String> {
	
	@Override
	public Class<? extends IMessage> recognize(String messageData) {
		String pattern = regexPattern();
		String messageWithoutSpaces = messageData.replaceAll("\\s", "");
		if (messageWithoutSpaces.matches(pattern))
			return matchedClass();
		else
			return null;
	}
	
	abstract protected String regexPattern();

	abstract protected Class<? extends IMessage> matchedClass();
}
