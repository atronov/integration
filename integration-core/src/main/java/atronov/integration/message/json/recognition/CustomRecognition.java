package atronov.integration.message.json.recognition;

import atronov.integration.message.IMessage;
import atronov.integration.message.TypeRecognition;
import atronov.integration.message.pojo.custom.CustomListElement;
import atronov.integration.message.pojo.custom.CustomMapElement;

public class CustomRecognition implements TypeRecognition<String> {

	@Override
	public Class<? extends IMessage> recognize(String messageData) {
		String mapPattern = "\\{\\s*\"content\"\\s*:\\s*\\{\\s*\".*?\"\\s*:.*\\}\\s*\\}";
		String messageWithoutSpaces = messageData.replaceAll("\\s", "");
		if (messageWithoutSpaces.matches(mapPattern))
			return CustomMapElement.class;
		else
			return CustomListElement.class;
	}
}
