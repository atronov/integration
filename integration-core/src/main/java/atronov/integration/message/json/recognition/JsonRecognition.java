package atronov.integration.message.json.recognition;

import atronov.integration.message.IMessage;
import atronov.integration.message.TypeRecognition;

/**
 * Implementation to allow bind class with implementation, without direct using builder
 */
public class JsonRecognition implements TypeRecognition<String> {

	private TypeRecognition<String> nestedRecognition;
	
	public JsonRecognition() {
		this.nestedRecognition = JsonRecognitionBuilder.create();
	}
	
	@Override
	public Class<? extends IMessage> recognize(String messageData) {
		return nestedRecognition.recognize(messageData);
	}

}
