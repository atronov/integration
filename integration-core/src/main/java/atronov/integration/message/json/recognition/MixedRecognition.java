package atronov.integration.message.json.recognition;

import atronov.integration.message.IMessage;
import atronov.integration.message.pojo.MixedMessage;

public class MixedRecognition extends PatternRecognition {

	@Override
	protected String regexPattern() {
		String pattern = "{\"active\":(true)|(false)}";
		return pattern;
	}

	@Override
	protected Class<? extends IMessage> matchedClass() {
		return MixedMessage.class;
	}
}
