package atronov.integration.message.json.recognition;

import atronov.integration.message.IMessage;
import atronov.integration.message.RecognitionChain;
import atronov.integration.message.TypeRecognition;
import atronov.integration.message.pojo.ArrayMessage;
import atronov.integration.message.pojo.BooleanMessage;
import atronov.integration.message.pojo.DoubleMessage;
import atronov.integration.message.pojo.IntegerMessage;
import atronov.integration.message.pojo.MapMessage;
import atronov.integration.message.pojo.MixedMessage;
import atronov.integration.message.pojo.TextMessage;
import atronov.integration.message.pojo.custom.CustomMessageBuilder;

// TODO refactor for Json Schema checker
class JsonRecognitionBuilder {
	
	private final static String BOOL_PATTERN_PART = "\"active\":((true)|(false))";
	private final static String DOUBLE_PATTERN_PART = "\"amount\":-?\\d+([.]\\d+)?";
	private final static String INT_PATTERN_PART = "\"count\":-?\\d*";
	private final static String TEXT_PATTERN_PART = "\"text\":\".*\"";
	private final static String ARRAY_PATTERN_PART = "\"tags\":\\[.*\\]";
	private final static String MAP_PATTERN_PART = "\"properties\":\\{.*\\}";
	private final static String ANY_PATTERN_PART = ".*";
	
	private final static String BOOL_PATTERN = "\\{"+BOOL_PATTERN_PART+"\\}";
	private final static String DOUBLE_PATTERN = "\\{"+DOUBLE_PATTERN_PART+"\\}";
	private final static String INT_PATTERN = "\\{"+INT_PATTERN_PART+"\\}";
	private final static String TEXT_PATTERN = "\\{"+TEXT_PATTERN_PART+"\\}";
	private final static String ARRAY_PATTERN = "\\{"+ARRAY_PATTERN_PART+"\\}";
	private final static String MAP_PATTERN = "\\{"+MAP_PATTERN_PART+"\\}";
	
	private static final TypeRecognition<String> BOOL_RECOGNITION;
	private static final TypeRecognition<String> DOUBLE_RECOGNITION;
	private static final TypeRecognition<String> INT_RECOGNITION;
	private static final TypeRecognition<String> TEXT_RECOGNITION;
	private static final TypeRecognition<String> ARRAY_RECOGNITION;
	private static final TypeRecognition<String> MAP_RECOGNITION;
	private static final TypeRecognition<String> MIX_RECOGNITION;
	private static final TypeRecognition<String> CUSTOM_RECOGNITION;
	
	static {
		BOOL_RECOGNITION = new PatternRecognition() {
			protected String regexPattern() {	return BOOL_PATTERN; }
			protected Class<BooleanMessage> matchedClass() { return BooleanMessage.class;  }
		};
		DOUBLE_RECOGNITION = new PatternRecognition() {
			protected String regexPattern() {	return DOUBLE_PATTERN; }
			protected Class<DoubleMessage> matchedClass() { return DoubleMessage.class;  }
		};
		INT_RECOGNITION = new PatternRecognition() {
			protected String regexPattern() {	return INT_PATTERN; }
			protected Class<IntegerMessage> matchedClass() { return IntegerMessage.class;  }
		};
		TEXT_RECOGNITION = new PatternRecognition() {
			protected String regexPattern() {	return TEXT_PATTERN; }
			protected Class<TextMessage> matchedClass() { return TextMessage.class;  }
		};
		ARRAY_RECOGNITION = new PatternRecognition() {
			protected String regexPattern() {	return ARRAY_PATTERN; }
			protected Class<ArrayMessage> matchedClass() { return ArrayMessage.class;  }
		};
		MAP_RECOGNITION = new PatternRecognition() {
			protected String regexPattern() {	return MAP_PATTERN; }
			protected Class<MapMessage> matchedClass() { return MapMessage.class;  }
		};
		MIX_RECOGNITION = createMixedRecognition();
		CUSTOM_RECOGNITION = new CustomRecognition();
	}
	
	private static TypeRecognition<String> createMixedRecognition() {
		return new TypeRecognition<String>() {
			public Class<MixedMessage> recognize(String messageData) {
				messageData = messageData.replaceAll("\\s", "");
				boolean isMatch = true;
				isMatch &= (messageData.matches(wrapAny(BOOL_PATTERN_PART)));
				isMatch &= (messageData.matches(wrapAny(DOUBLE_PATTERN_PART)));
				isMatch &= (messageData.matches(wrapAny(INT_PATTERN_PART)));
				isMatch &= (messageData.matches(wrapAny(TEXT_PATTERN_PART)));
				isMatch &= (messageData.matches(wrapAny(ARRAY_PATTERN_PART)));
				isMatch &= (messageData.matches(wrapAny(MAP_PATTERN_PART)));
				if (isMatch)
					return MixedMessage.class;
				else
					return null;
			}
			
			private String wrapAny(String wrappedPatternPart) {
				return "\\{"+ANY_PATTERN_PART+wrappedPatternPart+ANY_PATTERN_PART+"\\}";
			}
		};
	}
	
	static TypeRecognition<String> create() {
		RecognitionChain<String> chain = null;
		chain = RecognitionChain.create(CUSTOM_RECOGNITION, chain);
		chain = RecognitionChain.create(BOOL_RECOGNITION, chain);
		chain = RecognitionChain.create(DOUBLE_RECOGNITION, chain);
		chain = RecognitionChain.create(INT_RECOGNITION, chain);
		chain = RecognitionChain.create(TEXT_RECOGNITION, chain);
		chain = RecognitionChain.create(ARRAY_RECOGNITION, chain);
		chain = RecognitionChain.create(MAP_RECOGNITION, chain);
		chain = RecognitionChain.create(MIX_RECOGNITION, chain);
		return chain;
	}
}
