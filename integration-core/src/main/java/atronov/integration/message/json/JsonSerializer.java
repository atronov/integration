package atronov.integration.message.json;

import org.apache.log4j.Logger;

import com.google.gson.Gson;

import atronov.integration.message.IMessage;
import atronov.integration.message.Serializer;

// TODO refactor to Jackson
public class JsonSerializer implements Serializer<String> {

	private final Logger log = Logger.getLogger(this.getClass());
	
	private final Gson gson = new Gson();
	
	@Override
	public <TMessage extends IMessage> String serealize(TMessage message) {
		String json = gson.toJson(message);
		log.debug("Object serialized as:" + json);
		log.debug("Object length:" + json.length());
		return json;
	}

	@Override
	public <TMessage extends IMessage> TMessage deserealize(String data, Class<TMessage> messageClass) {
		log.debug("Deserialize of object:" + data);
		log.debug("Object length:" + data.length());
		TMessage message = gson.fromJson(data, messageClass);
		return message;
	}
}
