package atronov.integration.message;

import java.util.Map;

public interface IMapMessage extends IMessage {

	Map<String, String> getProperties();

	void setProperties(Map<String, String> properties);

}