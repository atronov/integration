package atronov.integration.message;

public interface ITextMessage extends IMessage {

	public abstract String getText();

	public abstract void setText(String text);

}