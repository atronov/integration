package atronov.integration.message;

public interface IIntegerMessage extends IMessage {

	int getCount();

	void setCount(int count);

}