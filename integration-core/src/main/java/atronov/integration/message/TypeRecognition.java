package atronov.integration.message;

public interface TypeRecognition<TData> {
	 Class<? extends IMessage> recognize(TData messageData);
}
