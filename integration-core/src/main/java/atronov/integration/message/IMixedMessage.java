package atronov.integration.message;

public interface IMixedMessage extends
	IIntegerMessage
	, IDoubleMessage
	, IBooleanMessage
	, ITextMessage
	, IArrayMessage
	, IMapMessage
	, IMessage{

}
