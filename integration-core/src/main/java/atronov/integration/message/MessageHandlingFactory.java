package atronov.integration.message;

public interface MessageHandlingFactory<TMessageData> {
	
	Serializer<TMessageData> createSerializer();
	
	TypeRecognition<TMessageData> createRecognition();
	
}
