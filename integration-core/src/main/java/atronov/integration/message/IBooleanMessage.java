package atronov.integration.message;

public interface IBooleanMessage extends IMessage {

	public abstract boolean isActive();

	public abstract void setActive(boolean active);

}