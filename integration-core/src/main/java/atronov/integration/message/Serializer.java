package atronov.integration.message;


public interface Serializer<TData> {
	
	<TMessage extends IMessage> TData serealize(TMessage message);
	
	<TMessage extends IMessage> TMessage deserealize(TData data, Class<TMessage> messageClass);
}
