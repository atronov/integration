package atronov.integration.message;

public interface IDoubleMessage extends IMessage {

	double getAmount();

	void setAmount(double amount);

}