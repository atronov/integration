package atronov.integration.message;

public abstract class RecognitionChain<TMessageData> implements TypeRecognition<TMessageData> {
	
	protected TypeRecognition<TMessageData> nestedRecognition;
	
	@Override
	public Class<? extends IMessage> recognize(TMessageData messageData) {
		Class<? extends IMessage> result = tryRecognize(messageData);
		if (result == null) {
			return nestedRecognition.recognize(messageData);
		} else
			return result;
	};
	
	protected abstract Class<? extends IMessage> tryRecognize(TMessageData messageData);

	/**
	 * @param recognition should return null, if cannot be recognized
	 */
	public static <TMessageData> RecognitionChain<TMessageData> create(
			final TypeRecognition<TMessageData> recognition,
			final TypeRecognition<TMessageData> nested
		) {
		return new RecognitionChain<TMessageData>() {
			{ this.nestedRecognition = nested; }
			protected Class<? extends IMessage> tryRecognize(TMessageData messageData) {
				return recognition.recognize(messageData);
			}
		};
	}
}