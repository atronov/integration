package atronov.integration.message.simple;

import java.util.HashMap;
import java.util.Map;

import org.apache.commons.beanutils.Converter;
import org.apache.commons.lang3.StringUtils;

import com.google.common.base.Splitter;

public class MapConvertor implements Converter {
  private String pairDelimiter;
  private String keyValueDelimiter;
  
	public MapConvertor() {
		this(";",":");
	}

	public MapConvertor(String pairDelimiter, String keyValueDelimiter) {
		this.pairDelimiter = pairDelimiter;
		this.keyValueDelimiter = keyValueDelimiter;
	}

	@Override
	public Object convert(@SuppressWarnings("rawtypes") Class clazz, Object obj) {
		 if (!(obj instanceof String)) return new String[0];
     String str = (String) obj;
     Map<String, String> map = new HashMap<>();
     for (String pair : Splitter.on(pairDelimiter).splitToList(str)) {
    	 String key = StringUtils.substringBefore(pair, keyValueDelimiter);
    	 String value = StringUtils.substringAfter(pair, keyValueDelimiter);
    	 map.put(key, value);
     }
     return map;
	}
}
