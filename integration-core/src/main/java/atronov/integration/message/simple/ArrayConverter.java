package atronov.integration.message.simple;

import org.apache.commons.beanutils.Converter;

public class ArrayConverter implements Converter {
  private String delimiter;

  public ArrayConverter () {
      this(";");
  }

  public ArrayConverter(String delimiter) {
      this.delimiter = delimiter;
  }

  @Override
  public Object convert(@SuppressWarnings("rawtypes") Class aClass, Object obj) {
      if (!(obj instanceof String)) return new String[0];
      String str = (String) obj;
      return str.split(delimiter);
  }
}
