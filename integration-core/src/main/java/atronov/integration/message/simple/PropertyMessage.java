package atronov.integration.message.simple;

import java.io.Serializable;
import java.util.Map;

import ru.yandex.qatools.properties.annotations.Property;
import ru.yandex.qatools.properties.annotations.Resource;
import ru.yandex.qatools.properties.annotations.Use;
import atronov.integration.message.IMixedMessage;

@Resource.Classpath("message.properties")
public class PropertyMessage implements IMixedMessage, Serializable {

	@Property("active")
	private boolean active;

	@Property("text")
	private String text;

	@Property("amount")
	private double amount;

	@Property("count")
	private int count;

	@Use(ArrayConverter.class)
	@Property("tags")
	private String[] tags;

	@Use(MapConvertor.class)
	@Property("properties")
	private Map<String, String> properties;

	public boolean isActive() {
		return active;
	}

	public void setActive(boolean active) {
		this.active = active;
	}

	public String getText() {
		return text;
	}

	public void setText(String text) {
		this.text = text;
	}

	public double getAmount() {
		return amount;
	}

	public void setAmount(double amount) {
		this.amount = amount;
	}

	public int getCount() {
		return count;
	}

	public void setCount(int count) {
		this.count = count;
	}

	public String[] getTags() {
		return tags;
	}

	public void setTags(String[] tags) {
		this.tags = tags;
	}

	public Map<String, String> getProperties() {
		return properties;
	}

	public void setProperties(Map<String, String> properties) {
		this.properties = properties;
	}
}
