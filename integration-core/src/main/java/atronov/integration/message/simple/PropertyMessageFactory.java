package atronov.integration.message.simple;

import ru.yandex.qatools.properties.PropertyLoader;
import atronov.integration.message.IArrayMessage;
import atronov.integration.message.IBooleanMessage;
import atronov.integration.message.ICustomMessage;
import atronov.integration.message.IDoubleMessage;
import atronov.integration.message.IIntegerMessage;
import atronov.integration.message.IMapMessage;
import atronov.integration.message.IMixedMessage;
import atronov.integration.message.ITextMessage;
import atronov.integration.message.MessageFactory;
import atronov.integration.message.pojo.custom.CustomMessageBuilder;

public class PropertyMessageFactory implements MessageFactory {
	
	public IBooleanMessage createBooleanMessage() {
		return create();
	}

	public IIntegerMessage createIntegerMessage() {
		return create();
	}

	public IDoubleMessage createDoubleMessage() {
		return create();
	}

	public ITextMessage createTextMessage() {
		return create();
	}

	public IArrayMessage createArrayMessage() {
		return create();
	}

	public IMapMessage createMapMessage() {
		return create();
	}

	public IMixedMessage createMixedMessage() {
		return create();
	}
	
	private IMixedMessage create() {
		IMixedMessage message = new PropertyMessage();
		PropertyLoader.populate(message);
		return message;
	}

	@Override
	public ICustomMessage createCustomMessage() {
		CustomMessageBuilder messageBuilder = CustomMessageBuilder
			.CreateForMap()
			.put("flag1",true)
			.put("flag2",false)
			.put("flag3",true)
			.put("text1", "gsfgsgsdgsdfsdgsdg")
			.put("text2", "ergerhweh3hrergweghw3rg3aqger")
			.put("text3", "ergergergqwgq34gq34rgq35gq4gq")
			.put("number1", 5)
			.put("number2", 45)
			.put("number3", 532.532)
			.put("number4", -0.2423);
			
		CustomMessageBuilder innerBuilder1 = messageBuilder.putListElement("Child1")
				.add(false)
				.add(true)
				.add(false)
				.add("weeeeeeeee")
				.add("ttttttttttttttt")
				.add("ggggggggggggggggggggggggggggg")
				.add(77)
				.add(88888)
				.add(444.555)
				.add(-0.00000001);
	
	CustomMessageBuilder innerBuilder2 = messageBuilder.putMapElement("Child2")
			.put("flag11",false)
			.put("flag12",true)
			.put("flag13",false)
			.put("text11","weeeeeeeee")
			.put("text12","ttttttttttttttt")
			.put("text13","ggggggggggggggggggggggggggggg")
			.put("number11",77)
			.put("number12",88888)
			.put("number13",444.555)
			.put("number14",-0.00000001);
	
			ICustomMessage message = messageBuilder.build();
			return message;
	}

}
