package atronov.integration.message;

public interface IArrayMessage extends IMessage {

	String[] getTags();

	void setTags(String[] tags);

}