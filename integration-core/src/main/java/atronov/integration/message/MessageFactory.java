package atronov.integration.message;

public interface MessageFactory {
	
	IBooleanMessage createBooleanMessage();
	
	IIntegerMessage createIntegerMessage();
	
	IDoubleMessage createDoubleMessage();
	
	ITextMessage createTextMessage();
	
	IArrayMessage createArrayMessage();
	
	IMapMessage createMapMessage();
	
	IMixedMessage createMixedMessage();
	
	ICustomMessage createCustomMessage();
}
