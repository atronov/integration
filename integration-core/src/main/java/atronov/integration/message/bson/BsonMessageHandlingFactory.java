package atronov.integration.message.bson;

import atronov.integration.message.MessageHandlingFactory;
import atronov.integration.message.Serializer;
import atronov.integration.message.TypeRecognition;
import atronov.integration.message.bson.recognition.BsonRecognition;
import atronov.integration.message.json.recognition.JsonRecognition;

public class BsonMessageHandlingFactory implements MessageHandlingFactory<byte[]> {

	@Override
	public Serializer<byte[]> createSerializer() {
		return new BsonSerializer();
	}

	@Override
	public TypeRecognition<byte[]> createRecognition() {
		return new BsonRecognition();
	}

}
