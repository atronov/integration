package atronov.integration.message.bson;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.IOException;

import org.apache.log4j.Logger;

import atronov.integration.message.IMessage;
import atronov.integration.message.Serializer;

import com.fasterxml.jackson.databind.DeserializationFeature;
import com.fasterxml.jackson.databind.ObjectMapper;

import de.undercouch.bson4jackson.BsonFactory;

public class BsonSerializer implements Serializer<byte[]> {

	private final Logger log = Logger.getLogger(this.getClass());
	
	@Override
	public <TMessage extends IMessage> byte[] serealize(TMessage message) {
		ByteArrayOutputStream baos = new ByteArrayOutputStream();
    ObjectMapper mapper = new ObjectMapper(new BsonFactory());
    try {
			mapper.writeValue(baos, message);
			log.debug("Object serialized as:" + new String(baos.toByteArray()));
			log.debug("Object length:" + baos.toByteArray().length);
		} catch (IOException e) {
			throw new RuntimeException(e);
		}
		return baos.toByteArray();
	}

	@Override
	public <TMessage extends IMessage> TMessage deserealize(byte[] data, Class<TMessage> messageClass) {
		ByteArrayInputStream bais = new ByteArrayInputStream(data);
    ObjectMapper mapper = new ObjectMapper(new BsonFactory());
    mapper.configure(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES, false);
		TMessage message;
		try {
			log.debug("Deserialize of object:" + new String(data));
			log.debug("Object length:" + data.length);
			message = mapper.readValue(bais, messageClass);
			return message;
		} catch (IOException e) {
			throw new RuntimeException(e);
		}
	}

}
