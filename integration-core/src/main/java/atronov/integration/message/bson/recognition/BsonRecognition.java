package atronov.integration.message.bson.recognition;

import atronov.integration.message.IMessage;
import atronov.integration.message.TypeRecognition;

public class BsonRecognition implements TypeRecognition<byte[]> {

	private TypeRecognition<byte[]> nestedRecognition;
	
	public BsonRecognition() {
		this.nestedRecognition = BsonRecognitionBuilder.create();
	}
	
	@Override
	public Class<? extends IMessage> recognize(byte[] messageData) {
		return nestedRecognition.recognize(messageData);
	}

}
