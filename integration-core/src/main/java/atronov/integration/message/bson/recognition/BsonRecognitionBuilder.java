package atronov.integration.message.bson.recognition;

import java.io.ByteArrayInputStream;
import java.io.IOException;

import atronov.integration.message.IMessage;
import atronov.integration.message.RecognitionChain;
import atronov.integration.message.TypeRecognition;
import atronov.integration.message.pojo.ArrayMessage;
import atronov.integration.message.pojo.BooleanMessage;
import atronov.integration.message.pojo.DoubleMessage;
import atronov.integration.message.pojo.IntegerMessage;
import atronov.integration.message.pojo.MapMessage;
import atronov.integration.message.pojo.MixedMessage;
import atronov.integration.message.pojo.TextMessage;
import atronov.integration.message.pojo.custom.CustomListElement;
import atronov.integration.message.pojo.custom.CustomMapElement;

import com.fasterxml.jackson.core.JsonParseException;
import com.fasterxml.jackson.databind.DeserializationFeature;
import com.fasterxml.jackson.databind.JsonMappingException;
import com.fasterxml.jackson.databind.ObjectMapper;

import de.undercouch.bson4jackson.BsonFactory;


public class BsonRecognitionBuilder {
	
	private static final ObjectMapper MAPPER = new ObjectMapper(new BsonFactory());
	
	private static final TypeRecognition<byte[]> BOOL_RECOGNITION;
	private static final TypeRecognition<byte[]> DOUBLE_RECOGNITION;
	private static final TypeRecognition<byte[]> INT_RECOGNITION;
	private static final TypeRecognition<byte[]> TEXT_RECOGNITION;
	private static final TypeRecognition<byte[]> ARRAY_RECOGNITION;
	private static final TypeRecognition<byte[]> MAP_RECOGNITION;
	private static final TypeRecognition<byte[]> MIX_RECOGNITION;
	private static final TypeRecognition<byte[]> CUSTOM_MAP_RECOGNITION;
	private static final TypeRecognition<byte[]> CUSTOM_LIST_RECOGNITION;
	

	static {
//		MAPPER.enable(DeserializationFeature.FAIL_ON_IGNORED_PROPERTIES);
//		MAPPER.enable(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES);
//		MAPPER.enable(DeserializationFeature.FAIL_ON_NULL_FOR_PRIMITIVES);
		BOOL_RECOGNITION = new MapperRecognition(MAPPER, BooleanMessage.class);
		DOUBLE_RECOGNITION = new MapperRecognition(MAPPER, DoubleMessage.class);
		INT_RECOGNITION = new MapperRecognition(MAPPER, IntegerMessage.class);
		TEXT_RECOGNITION = new MapperRecognition(MAPPER, TextMessage.class);
		ARRAY_RECOGNITION = new MapperRecognition(MAPPER, ArrayMessage.class);
		MAP_RECOGNITION = new MapperRecognition(MAPPER, MapMessage.class);
		MIX_RECOGNITION = new MapperRecognition(MAPPER, MixedMessage.class);
		CUSTOM_MAP_RECOGNITION = new MapperRecognition(MAPPER, CustomMapElement.class);
		CUSTOM_LIST_RECOGNITION = new MapperRecognition(MAPPER, CustomListElement.class);
	}
	
	static TypeRecognition<byte[]> create() {
		RecognitionChain<byte[]> chain = null;
		chain = RecognitionChain.create(CUSTOM_LIST_RECOGNITION, chain);
		chain = RecognitionChain.create(CUSTOM_MAP_RECOGNITION, chain);
		chain = RecognitionChain.create(MIX_RECOGNITION, chain);
		chain = RecognitionChain.create(BOOL_RECOGNITION, chain);
		chain = RecognitionChain.create(DOUBLE_RECOGNITION, chain);
		chain = RecognitionChain.create(INT_RECOGNITION, chain);
		chain = RecognitionChain.create(TEXT_RECOGNITION, chain);
		chain = RecognitionChain.create(ARRAY_RECOGNITION, chain);
		chain = RecognitionChain.create(MAP_RECOGNITION, chain);
		return chain;
	}

	
	private static class MapperRecognition implements TypeRecognition<byte[]> {

		private ObjectMapper mapper;
		private Class<? extends IMessage> trgetType;
		
		MapperRecognition(ObjectMapper mapper, Class<? extends IMessage> trgetType) {
			this.mapper = mapper;
			this.trgetType = trgetType;
		}
		
		@Override
		public Class<? extends IMessage> recognize(byte[] messageData) {
				ByteArrayInputStream bais = new ByteArrayInputStream(messageData);
				Object o;
				try {
					o =mapper.readValue(bais, trgetType);
				} catch (JsonParseException e) {
					return null;
				} catch (JsonMappingException e) {
					return null;
				} catch (IOException e) {
					throw new RuntimeException(e);
				}
				return trgetType;
		}
	}
}
