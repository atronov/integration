package atronov.integration.utility;

import java.util.ArrayList;
import java.util.List;

import org.jboss.util.threadpool.ThreadPoolFullException;

import com.google.common.base.Predicate;

public class  Wait {
	private long iterTime = 250; // 0.25 sec
	private long waitTime = 5_000; // 5 sec
	
	private Condition condition;
	private List<Class<? extends Exception>> ignoreList = new ArrayList<>();
	
	private Wait(Condition condition) {
		this.condition = condition;
	}
	
	public void ignoring(Class<? extends Exception> errType) {
		ignoreList.add(errType);
	}
	
	public static Wait until(Condition predicate) {
		return new Wait(predicate);
	}
	
	public boolean go() {
		long startTime = System.currentTimeMillis();
		while (System.currentTimeMillis() - startTime <= waitTime) {
			try {
				if (condition.check())
					return true;
			} catch (Exception err) {
				checkIgnoring(err);
			}
			try {Thread.sleep(iterTime);} catch (InterruptedException e) {}
		}
		return false;
	}
	
	private void checkIgnoring(Exception ex) {
		for (Class<? extends Throwable> errType : ignoreList) {
			if (ex.getClass().equals(errType)
					|| errType.isAssignableFrom(ex.getClass()))
				return;
		}
		throw new RuntimeException("Error while waiting", ex);
	}
	
	public interface Condition {
		boolean check();
	}
	
	public Wait forPeriod(long time) {
		waitTime = time;
		return this;
	}
}
