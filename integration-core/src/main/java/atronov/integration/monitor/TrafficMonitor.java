package atronov.integration.monitor;

import java.util.List;

public interface TrafficMonitor {
	
	void start() throws Exception;

	List<PacketInfo> stop() throws Exception;
	
	void setFilter(PacketFilter filter);

}