package atronov.integration.monitor;

import static com.google.common.base.Preconditions.*;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.LinkedList;
import java.util.List;
import java.util.NoSuchElementException;

import org.apache.log4j.Logger;
import org.jnetpcap.Pcap;
import org.jnetpcap.PcapIf;
import org.jnetpcap.packet.JHeader;
import org.jnetpcap.packet.JHeaderPool;
import org.jnetpcap.packet.JPacket;
import org.jnetpcap.packet.Payload;
import org.jnetpcap.packet.PcapPacket;
import org.jnetpcap.packet.PcapPacketHandler;
import org.jnetpcap.packet.format.FormatUtils;
import org.jnetpcap.protocol.network.Ip4;
import org.jnetpcap.protocol.tcpip.Tcp;

import atronov.integration.monitor.PacketInfo.Protocol;

import com.google.common.base.Predicate;
import com.google.common.collect.Iterables;

public class NetPcapMonitor implements TrafficMonitor {
	
	private final Logger log = Logger.getLogger(this.getClass());
	
	private final String deviceName;
	private PacketFilter filter;
	private Pcap pcap;
	private final List<PacketInfo> packets = Collections.synchronizedList(new LinkedList<PacketInfo>());
	private Thread worker;
	private volatile boolean stopFlag = false;
	
	public NetPcapMonitor(String deviceName) {
		this.deviceName = deviceName;
		filter = new PacketFilter() {
			public boolean check(PacketInfo packetInfo) {
				return true;
			}
		};
	}
	
	@Override
	public void start() throws Exception {
		log.info("Start trafic motitoring");
		stopFlag = false;
		List<PcapIf> allDevs = new ArrayList<>();
		StringBuilder errbuf = new StringBuilder();
		int r = Pcap.findAllDevs(allDevs, errbuf);
		log.info("Find all devices");
		if (r == Pcap.NOT_OK || allDevs.isEmpty()) {
			log.info("No device found");
			throw new IllegalAccessException(String.format("Can't read list of devices, error is %s", errbuf.toString()));
		}
		log.info(""+allDevs.size() + " devices found");
		for (PcapIf dev : allDevs)
			log.info("Found device "+dev.getName());
		PcapIf targetDevice = null;
		try {
		targetDevice = Iterables.find(allDevs, new Predicate<PcapIf>() {
			public boolean apply(PcapIf dev) {
				return dev.getName().trim().equalsIgnoreCase(deviceName);
			}
		});
		} catch (NoSuchElementException e) {
			throw new IllegalArgumentException("No device with name " + deviceName, e);
		}
		final int snaplen = 64 * 1024; // Capture all packets, no trucation
		final int flags = Pcap.MODE_PROMISCUOUS; // capture all packets
		final int timeout = 30 * 1000; // 10 seconds in millis
		final PcapPacketHandler<List<PacketInfo>> monitorHandler = new PacketCollector();
		log.info("Open pcap");
		pcap = Pcap.openLive(targetDevice.getName(), snaplen, flags, timeout, errbuf);
		log.info("Start capture");
		packets.clear();
		Runnable monitorRunnable = new Runnable() {
			public void run() {
				try {
					pcap.loop(Pcap.LOOP_INFINITE, monitorHandler, packets);
				} catch (Exception e) {
					log.error("Error wrile traffic monitorring",e);
				}
			}
		};
		worker = new Thread(monitorRunnable);
		worker.start();
	}

	@Override
	public List<PacketInfo> stop() throws Exception {
		log.info("Stop trafic motitoring");
		if (pcap == null) {
			log.info("Pcap wasn't created");
			return new ArrayList<>();
		}
		stopFlag = true;
		Thread.yield();
		log.debug("Join working process");
		worker.join(5_000);
		log.debug("Close pcap");
		pcap.close();
		stopFlag = false;
		return packets;
	}
	
	@Override
	public void setFilter(PacketFilter filter) {
		checkNotNull(filter, "Filter cannot be null");
		this.filter = filter;
	} 
	
	private class PacketCollector implements PcapPacketHandler<List<PacketInfo>> {
		private final Tcp tcp = new Tcp();
		private final Ip4 ip = new Ip4();
		public void nextPacket(PcapPacket packet, List<PacketInfo> statistic) {
			log.debug("Catched packets: "+packets.size());
			log.debug("Current heap size " + Runtime.getRuntime().totalMemory());
			if (stopFlag)
				Thread.currentThread().interrupt();
			try {
			PacketInfo packetInfo = new PacketInfo();
			packetInfo.length = packet.getCaptureHeader().wirelen();
			try {
				JHeader last = getStaticLastHeader(packet);
				packetInfo.protocol = Protocol.byName(last.getName());
			} catch (Exception e) {
				log.error("Error in getting protocol name", e);
			}
			if (packet.hasHeader(Ip4.ID)) {
				packet.getHeader(ip);
				packetInfo.dstIp4 = Arrays.copyOf(ip.destination(), 4);
				packetInfo.srcIp4 = Arrays.copyOf(ip.source(),4);
			}
			if (packet.hasHeader(Tcp.ID)) {
				packet.getHeader(tcp);
				packetInfo.dstPort = tcp.destination();
				packetInfo.srcPort = tcp.source();
			}
			if (filter.check(packetInfo))
				statistic.add(packetInfo);
			else
				log.debug("Skipped by filter: " + packetInfo);
			} catch (Exception e) {
				log.error("Error handling network packet",e);
			}
		}
	}

	public static JHeader getStaticLastHeader(JPacket packet) {
		return getStaticLastHeader(packet, false);
	}

	public static JHeader getStaticLastHeader(JPacket packet, boolean payloadOk) {
		int last = packet.getState().getHeaderCount() - 1;

		if (!payloadOk && packet.getState().getHeaderIdByIndex(last) == Payload.ID && last > 0) {
			last--; // We want the last header before payload
		}

		final JHeader header = JHeaderPool.getDefault().getHeader(packet.getState().getHeaderIdByIndex(last));
		packet.getHeaderByIndex(last, header);

		return header;
	}
	
	public static void main(String[] args) throws Exception {
		String devName = (args.length > 0)? args[0] : "\\Device\\NPF_{76498161-6AC7-41B5-9A41-F4A4180A6CB9}";
		NetPcapMonitor mon = new NetPcapMonitor(devName);
		mon.start();
		System.out.print("Sleep 10s");
		Thread.sleep(10_000);
		mon.stop();
		System.out.println("Captured packets "+mon.packets.size());
		for (PacketInfo info : mon.packets)
			System.out.println(info);
	}
}
