package atronov.integration.monitor;

import java.io.Serializable;
import java.util.List;

import org.apache.log4j.Logger;
import org.jnetpcap.packet.format.FormatUtils;

public class HostsFilter implements PacketFilter, Serializable {

	private static final long serialVersionUID = 2709512358050592458L;

	private final List<String> allowedFromHosts;
	private final List<String> allowedToHosts;

	public HostsFilter(List<String> fromHosts, List<String> tomHosts) {
		allowedFromHosts = fromHosts;
		allowedToHosts = tomHosts;
	}

	public boolean check(PacketInfo packetInfo) {
		if (packetInfo.srcIp4 == null || !allowedFromHosts.contains(FormatUtils.ip(packetInfo.srcIp4)))
			return false;
		if (packetInfo.dstIp4 == null || !allowedToHosts.contains(FormatUtils.ip(packetInfo.dstIp4)))
			return false;
		return true;
	}

}
