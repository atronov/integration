package atronov.integration.monitor;

import java.io.Serializable;

import org.jnetpcap.packet.format.FormatUtils;

public class PacketInfo implements Serializable {

	private static final long serialVersionUID = -3510006389371165677L;

	public static enum Protocol {
		TCP,
		IP4,
		IP6,
		UDP,
		HTTP,
		OTHER;
		
		private static Protocol[] allPossible = new Protocol[] {TCP,
			IP4,
			IP6,
			UDP,
			HTTP
		};
		
		public static Protocol byName(String protocolName) {
			for (Protocol protocol : allPossible) {
				if (protocol.name().equalsIgnoreCase(protocolName))
					return protocol;
			}
			return OTHER;
		}
		
		@Override
		public String toString() {
			return name();
		}
	}
	
	public Protocol protocol;
	public byte[] srcIp4;
	public byte[] dstIp4;
	public int srcPort = -1;
	public int dstPort = -1;
	public int length = -1;
	
	public PacketInfo() {
	}
	
	@Override
	public String toString() {
		return String.format(
				"Protocol %s: %s:%s -> %s:%s %s bytes",
				protocol,
				(srcIp4 != null)? FormatUtils.ip(srcIp4) : "not-set",
				srcPort,
				(dstIp4 != null)? FormatUtils.ip(dstIp4) : "not-set",
				dstPort,
				length);
	}
}
