package atronov.integration.monitor;

import java.util.Collection;
import java.util.LinkedList;
import java.util.List;

import org.apache.log4j.Logger;

public class FilterBuilder {

	@SuppressWarnings("unused")
	private final Logger log = Logger.getLogger(this.getClass());
	
	private final List<String> allowedFromHosts = new LinkedList<>();
	private final List<String> allowedToHosts = new LinkedList<>();
	
	private FilterBuilder() {
	}
	
	public static FilterBuilder allow() {
		return new FilterBuilder();
	}
	
	public FilterBuilder from(String hostName) {
		allowedFromHosts.add(hostName);
		return this;
	}
	
	public FilterBuilder from(Collection<String> hostNames) {
		allowedFromHosts.addAll(hostNames);
		return this;
	}
	
	public FilterBuilder to(String hostName) {
		allowedToHosts.add(hostName);
		return this;
	}	
	
	public FilterBuilder to(Collection<String> hostNames) {
		allowedToHosts.addAll(hostNames);
		return this;
	}
	
	public PacketFilter create() {
		return new HostsFilter(allowedFromHosts, allowedToHosts);
	}
	
}
