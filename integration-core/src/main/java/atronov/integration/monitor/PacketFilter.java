package atronov.integration.monitor;

public interface PacketFilter {
	boolean check(PacketInfo packetInfo);
}