package atronov.integration.transfer.hessian;

import java.net.MalformedURLException;
import java.util.Date;

import atronov.integration.message.IMessage;
import atronov.integration.transfer.Receiver;
import atronov.integration.transfer.resource.TopicService;
import atronov.integration.transfer.server.CommunicationConfigFactory;

import com.caucho.hessian.client.HessianProxyFactory;

public class HessianSubcriber implements Receiver {

	private final CommunicationConfigFactory communicationConfig;

	private final String subscriberId = Integer.toString((int)new Date().getTime());
	
	private TopicService topic;
	
	public HessianSubcriber(CommunicationConfigFactory communicationConfig) {
		this.communicationConfig=communicationConfig;
		subscribe();
	}
	
	private void subscribe() {
		String url = communicationConfig.getResourceUri().toString();
		HessianProxyFactory factory = new HessianProxyFactory();
		try {
			topic = (TopicService) factory.create(TopicService.class, url);
			topic.subscribe(subscriberId);
		} catch (MalformedURLException e) {
			throw new RuntimeException(e);
		}
	}
	
	@Override
	public <TMessage extends IMessage> IMessage getReceived() {
		return topic.receive(subscriberId);
	}

}
