package atronov.integration.transfer.hessian;


import java.util.Collection;

import javax.ws.rs.Path;

import atronov.integration.message.IMessage;
import atronov.integration.transfer.resource.MessageTopic;
import atronov.integration.transfer.resource.TopicService;

import com.caucho.hessian.server.HessianServlet;

@Path("/hessian")
public class HessianTopicService extends HessianServlet implements TopicService {

	private static final long serialVersionUID = -6464664119598965893L;
	
	private TopicService topic = new MessageTopic("HessianTopic");
	
	@Override
	public void subscribe(String subscriberId) {
		topic.subscribe(subscriberId);
	}

	@Override
	public IMessage receive(String subscriberId) {
		return topic.receive(subscriberId);
	}

	@Override
	public Collection<IMessage> receiveAll(String subscriberId) {
		return topic.receiveAll(subscriberId);
	}

	@Override
	public void publish(IMessage message) {
		topic.publish(message); 
	}

}
