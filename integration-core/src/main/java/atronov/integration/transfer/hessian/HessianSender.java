package atronov.integration.transfer.hessian;

import java.net.MalformedURLException;

import atronov.integration.message.IMessage;
import atronov.integration.transfer.Sender;
import atronov.integration.transfer.resource.QueueService;
import atronov.integration.transfer.server.CommunicationConfigFactory;

import com.caucho.hessian.client.HessianProxyFactory;

public class HessianSender implements Sender {

	private final CommunicationConfigFactory communicationConfig;
	
	private QueueService queue = null;
	
	public HessianSender(CommunicationConfigFactory communicationConfig) {
		this.communicationConfig=communicationConfig;
	}
	
	@Override
	public <TMessage extends IMessage> boolean send(IMessage message) {
		getQueue().push(message);
		return true;
	}
	
	public QueueService getQueue() {
		if (queue == null) {
			try {
				String url = communicationConfig.getHostUri().toString();
				HessianProxyFactory factory = new HessianProxyFactory();
				queue = (QueueService) factory.create(QueueService.class, url);
			} catch (MalformedURLException e) {
				throw new RuntimeException(e);
			}
		}
		return queue;
	}

}
