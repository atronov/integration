package atronov.integration.transfer.hessian;

import atronov.integration.message.IMessage;
import atronov.integration.transfer.resource.MessageQueue;
import atronov.integration.transfer.resource.QueueService;

import com.caucho.hessian.server.HessianServlet;

public class HessianQueueService extends HessianServlet implements QueueService {
	
	private static final long serialVersionUID = -5782905402989343158L;
	
	private QueueService storage = new MessageQueue("HessianQueue");

	@Override
	public void push(IMessage message) {
		storage.push(message);
	}
	
	@Override
	public IMessage remove() {
		return storage.remove();
	}
}
