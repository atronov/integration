package atronov.integration.transfer.hessian;

import java.util.NoSuchElementException;

import org.glassfish.grizzly.servlet.ServletRegistration;
import org.glassfish.grizzly.servlet.WebappContext;

import atronov.integration.message.IMessage;
import atronov.integration.transfer.ClosableReceiver;
import atronov.integration.transfer.server.CommunicationConfigFactory;
import atronov.integration.transfer.server.Server;

public class HessianReceiver extends Server implements ClosableReceiver {

	private HessianQueueService messages;
	
	public HessianReceiver(CommunicationConfigFactory serverConfig) {
		super(serverConfig);
		messages = new HessianQueueService();
		startServer();
	}
	
	@Override
	public <TMessage extends IMessage> IMessage getReceived() {
		try {
				@SuppressWarnings("unchecked")
				TMessage message = (TMessage) messages.remove();
				return message;
			} catch (NoSuchElementException e) {
				return null;
			}
	}

	@Override
	protected ServletRegistration addServlet(WebappContext context) {
		return context.addServlet("HessianService", HessianQueueService.class);
	}
}
