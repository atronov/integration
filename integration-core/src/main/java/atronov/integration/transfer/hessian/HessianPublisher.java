package atronov.integration.transfer.hessian;

import org.glassfish.grizzly.servlet.ServletRegistration;
import org.glassfish.grizzly.servlet.WebappContext;

import atronov.integration.message.IMessage;
import atronov.integration.transfer.ClosableSender;
import atronov.integration.transfer.resource.TopicService;
import atronov.integration.transfer.server.CommunicationConfigFactory;
import atronov.integration.transfer.server.Server;

public class HessianPublisher extends Server implements ClosableSender {

	private TopicService topic = new HessianTopicService();
	
	public HessianPublisher(CommunicationConfigFactory serverConfig) {
		super(serverConfig);
		startServer();
	}

	@Override
	protected ServletRegistration addServlet(WebappContext context) {
		return context.addServlet("HessianService", HessianTopicService.class);
	}

	@Override
	public <TMessage extends IMessage> boolean send(IMessage message) {
		topic.publish(message);
		return true;
	}
	
}
