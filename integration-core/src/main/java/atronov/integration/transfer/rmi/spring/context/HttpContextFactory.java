package atronov.integration.transfer.rmi.spring.context;

import org.springframework.beans.factory.support.BeanDefinitionBuilder;
import org.springframework.context.support.AbstractApplicationContext;
import org.springframework.context.support.GenericApplicationContext;
import org.springframework.remoting.httpinvoker.HttpInvokerProxyFactoryBean;
import org.springframework.remoting.httpinvoker.HttpInvokerServiceExporter;
import org.springframework.web.context.support.GenericWebApplicationContext;

import atronov.integration.config.RmiConfig;
import atronov.integration.transfer.resource.MessageQueue;
import atronov.integration.transfer.resource.MessageTopic;
import atronov.integration.transfer.resource.QueueService;
import atronov.integration.transfer.resource.TopicService;

public class HttpContextFactory implements ContextFactory {
	@Override
	public AbstractApplicationContext createForClient() {
		BeanDefinitionBuilder queueBuilder = BeanDefinitionBuilder
				.rootBeanDefinition(HttpInvokerProxyFactoryBean.class)
				.addPropertyValue("serviceUrl", String.format("http://%s:9988/", RmiConfig.get().getHost()))
				.addPropertyValue("serviceInterface", QueueService.class);
		BeanDefinitionBuilder topicBuilder = BeanDefinitionBuilder
				.rootBeanDefinition(HttpInvokerProxyFactoryBean.class)
				.addPropertyValue("serviceUrl", String.format("http://%s:9988/", RmiConfig.get().getHost()))
				.addPropertyValue("serviceInterface", TopicService.class);
		GenericApplicationContext context = new GenericApplicationContext();
		context.registerBeanDefinition("remoteQueue", queueBuilder.getBeanDefinition());
		context.registerBeanDefinition("remoteTopic", topicBuilder.getBeanDefinition());
		context.refresh();
		return context;
	}

	@Override
	public AbstractApplicationContext createForServer() {
		BeanDefinitionBuilder queueBuilder = BeanDefinitionBuilder
				.genericBeanDefinition(MessageQueue.class)
				.addConstructorArgValue("SpringHttpMessageQueue");
		BeanDefinitionBuilder topicBuilder = BeanDefinitionBuilder
				.genericBeanDefinition(MessageTopic.class)
				.addConstructorArgValue("SpringHttpMessageTopic");
		BeanDefinitionBuilder queueExporterBuilder = BeanDefinitionBuilder
				.rootBeanDefinition(HttpInvokerServiceExporter.class)
				.addPropertyReference("service", "queue")
				.addPropertyValue("serviceInterface", QueueService.class);
		BeanDefinitionBuilder topicExporterBuilder = BeanDefinitionBuilder
				.rootBeanDefinition(HttpInvokerServiceExporter.class)
				.addPropertyReference("service", "topic")
				.addPropertyValue("serviceInterface", TopicService.class);
		GenericApplicationContext context = new GenericWebApplicationContext();
		context.registerBeanDefinition("queue", queueBuilder.getBeanDefinition());
		context.registerBeanDefinition("topic", topicBuilder.getBeanDefinition());
		context.registerBeanDefinition("QueueService", queueExporterBuilder.getBeanDefinition());
		context.registerBeanDefinition("TopicService", topicExporterBuilder.getBeanDefinition());
		context.refresh();
		return context;
	}
}
