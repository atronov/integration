package atronov.integration.transfer.rmi.spring;

import java.util.Date;

import org.apache.log4j.Logger;
import org.springframework.context.support.AbstractApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;

import atronov.integration.message.IMessage;
import atronov.integration.transfer.Receiver;
import atronov.integration.transfer.resource.TopicService;
import atronov.integration.transfer.rmi.spring.context.ContextFactory;

public class SpringSubscriber implements Receiver {
	
	private final Logger log = Logger.getLogger(this.getClass());
	
	private ContextFactory contextFactory;

	private TopicService topic;	
	private String subscriberId = Long.toString(new Date().getTime());
	
	static {
		System.setProperty("javax.net.ssl.trustStore", "messaging.truststore");
		System.setProperty("javax.net.ssl.trustStorePassword", "secureexample");
	}
	
	public SpringSubscriber(ContextFactory contextFactory) {
		this.contextFactory = contextFactory;
		subscribe();
	}
	
	private void subscribe() {
		try {
			try (AbstractApplicationContext factory = contextFactory.createForClient()) {
				topic = (TopicService) factory.getBean("remoteTopic");
			}
			topic.subscribe(subscriberId);
		} catch (Throwable e) {
			log.error("Error while creating RMI subscriber", e);
			throw e;
		}
	}
	
	@Override
	public <TMessage extends IMessage> IMessage getReceived() {
		return topic.receive(subscriberId);
	}
}
