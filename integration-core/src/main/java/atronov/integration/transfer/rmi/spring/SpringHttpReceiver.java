package atronov.integration.transfer.rmi.spring;

import java.util.NoSuchElementException;

import org.glassfish.grizzly.servlet.ServletRegistration;
import org.glassfish.grizzly.servlet.WebappContext;
import org.springframework.context.ApplicationContext;
import org.springframework.context.support.AbstractApplicationContext;
import org.springframework.web.context.WebApplicationContext;
import org.springframework.web.context.request.RequestContextListener;
import org.springframework.web.context.support.GenericWebApplicationContext;
import org.springframework.web.context.support.HttpRequestHandlerServlet;

import atronov.integration.message.IMessage;
import atronov.integration.transfer.ClosableReceiver;
import atronov.integration.transfer.resource.QueueService;
import atronov.integration.transfer.rmi.spring.context.ContextFactory;
import atronov.integration.transfer.rmi.spring.context.CustomContextLoaderListener;
import atronov.integration.transfer.rmi.spring.context.CustomServlet;
import atronov.integration.transfer.server.CommunicationConfigFactory;
import atronov.integration.transfer.server.Server;

public class SpringHttpReceiver extends Server implements ClosableReceiver   {

	private QueueService messages;
	private final ContextFactory contextFactory;
	
	public SpringHttpReceiver(ContextFactory contextFactory, CommunicationConfigFactory config ) {
		super(config);
		this.contextFactory = contextFactory;
		try (AbstractApplicationContext context = contextFactory.createForServer()) {
			messages = (QueueService) context.getBean("queue");
		}
		startServer();
	}
	
	@Override
	public <TMessage extends IMessage> IMessage getReceived() {
		try {
			@SuppressWarnings("unchecked")
			TMessage message = (TMessage) messages.remove();
			return message;
		} catch (NoSuchElementException e) {
			return null;
		}
	}

	@Override
	protected ServletRegistration addServlet(WebappContext context) {
		GenericWebApplicationContext webContext = new GenericWebApplicationContext(context);
		ApplicationContext nestedContext = contextFactory.createForServer();
		webContext.setParent(nestedContext);
		CustomContextLoaderListener.setWebApplicationContext(webContext);
		webContext.refresh();
		context.addListener(CustomContextLoaderListener.class);
		context.addListener(RequestContextListener.class);
		return context.addServlet("QueueService", HttpRequestHandlerServlet.class);
	}
	
}
