package atronov.integration.transfer.rmi.spring.context;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.support.BeanDefinitionBuilder;
import org.springframework.context.support.AbstractApplicationContext;
import org.springframework.context.support.GenericApplicationContext;
import org.springframework.remoting.rmi.RmiProxyFactoryBean;
import org.springframework.remoting.rmi.RmiServiceExporter;

import atronov.integration.config.RmiConfig;
import atronov.integration.transfer.resource.MessageQueue;
import atronov.integration.transfer.resource.MessageTopic;
import atronov.integration.transfer.resource.QueueService;
import atronov.integration.transfer.resource.TopicService;

public class TcpContextFactory implements ContextFactory {

	private final Logger log = Logger.getLogger(this.getClass());
	
	@Override
	public AbstractApplicationContext createForClient() {
		BeanDefinitionBuilder queueBuilder = BeanDefinitionBuilder
				.rootBeanDefinition(RmiProxyFactoryBean.class)
				.addPropertyValue("serviceUrl", String.format("rmi://%s:%s/Queue", RmiConfig.get().getHost(), RmiConfig.get().getPort()))
				.addPropertyValue("serviceInterface", QueueService.class);
		BeanDefinitionBuilder topicBuilder = BeanDefinitionBuilder
				.rootBeanDefinition(RmiProxyFactoryBean.class)
				.addPropertyValue("serviceUrl", String.format("rmi://%s:%s/Topic", RmiConfig.get().getHost(), RmiConfig.get().getPort()))
				.addPropertyValue("serviceInterface", TopicService.class);
		GenericApplicationContext context = new GenericApplicationContext();
		context.registerBeanDefinition("remoteQueue", queueBuilder.getBeanDefinition());
		context.registerBeanDefinition("remoteTopic", topicBuilder.getBeanDefinition());
		context.refresh();
		return context;
	}

	@Override
	public AbstractApplicationContext createForServer() {
		log.info("Create server spring application context for tcp rmi");
		log.info("Set java.rmi.server.hostname to " + RmiConfig.get().getHost());
		System.setProperty("java.rmi.server.hostname", RmiConfig.get().getHost());
		BeanDefinitionBuilder queueBuilder = BeanDefinitionBuilder
				.genericBeanDefinition(MessageQueue.class)
				.addConstructorArgValue("SpringTcpMessageQueue");
		BeanDefinitionBuilder topicBuilder = BeanDefinitionBuilder
				.genericBeanDefinition(MessageTopic.class)
				.addConstructorArgValue("SpringTcpMessageTopic");
		BeanDefinitionBuilder queueExporterBuilder = BeanDefinitionBuilder
				.genericBeanDefinition(RmiServiceExporter.class)
				.addPropertyValue("serviceName", "Queue")
				.addPropertyReference("service", "queue")
				.addPropertyValue("serviceInterface", QueueService.class)
				.addPropertyValue("registryPort", RmiConfig.get().getPort());
		BeanDefinitionBuilder topicExporterBuilder = BeanDefinitionBuilder
				.genericBeanDefinition(RmiServiceExporter.class)
				.addPropertyValue("serviceName", "Topic")
				.addPropertyReference("service", "topic")
				.addPropertyValue("serviceInterface", TopicService.class)
				.addPropertyValue("registryPort", RmiConfig.get().getPort());
		GenericApplicationContext context = new GenericApplicationContext();
		context.registerBeanDefinition("queue", queueBuilder.getBeanDefinition());
		context.registerBeanDefinition("topic", topicBuilder.getBeanDefinition());
		context.registerBeanDefinition("queueExporter", queueExporterBuilder.getBeanDefinition());
		context.registerBeanDefinition("topicExporter", topicExporterBuilder.getBeanDefinition());
		context.refresh();
		return context;
	}
}
