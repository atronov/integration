package atronov.integration.transfer.rmi.spring;

import org.springframework.context.support.AbstractApplicationContext;

import atronov.integration.message.IMessage;
import atronov.integration.transfer.Sender;
import atronov.integration.transfer.resource.QueueService;
import atronov.integration.transfer.rmi.spring.context.ContextFactory;

public class SpringSender implements Sender {
	private ContextFactory contextFactory;
	
	private QueueService queue = null;
	
	static {
		System.setProperty("javax.net.ssl.trustStore", "messaging.truststore");
		System.setProperty("javax.net.ssl.trustStorePassword", "secureexample");
	}
	
	public SpringSender(ContextFactory contextFactory) {
		this.contextFactory = contextFactory;
	}

	@Override
	public <TMessage extends IMessage> boolean send(IMessage message) {
		getQueue().push(message);
		return true;
	}
	
	private QueueService getQueue() {
		if (queue == null) {
			try (AbstractApplicationContext factory = contextFactory.createForClient()) {
				queue = (QueueService) factory.getBean("remoteQueue");
			}
		}
		return queue;
	}
}
