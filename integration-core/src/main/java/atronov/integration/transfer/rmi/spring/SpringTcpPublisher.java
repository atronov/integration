package atronov.integration.transfer.rmi.spring;

import org.springframework.context.support.AbstractApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;

import atronov.integration.message.IMessage;
import atronov.integration.transfer.ClosableSender;
import atronov.integration.transfer.resource.TopicService;
import atronov.integration.transfer.rmi.spring.context.ContextFactory;

public class SpringTcpPublisher implements ClosableSender {

	private TopicService topic;
	
	private AbstractApplicationContext context;
	
	static {
		System.setProperty("javax.net.ssl.keyStore", "messaging.keystore");
		System.setProperty("javax.net.ssl.keyStorePassword", "secureexample");
	}
	
	public SpringTcpPublisher(ContextFactory contextFactory) {
		context = contextFactory.createForServer();
		topic = (TopicService) context.getBean("topic");
	}
	
	@Override
	public <TMessage extends IMessage> boolean send(IMessage message) {
		topic.publish(message);
		return true;
	}

	@Override
	public void close() throws Exception {
		if (context != null) context.close();
	}

}
