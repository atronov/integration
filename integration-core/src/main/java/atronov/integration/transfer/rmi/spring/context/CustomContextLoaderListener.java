package atronov.integration.transfer.rmi.spring.context;

import javax.servlet.ServletContextEvent;

import org.springframework.web.context.ContextLoaderListener;
import org.springframework.web.context.WebApplicationContext;

public class CustomContextLoaderListener extends ContextLoaderListener {
	
	private static WebApplicationContext sharedWebApplicationContext = null;
	
	public static void setWebApplicationContext(WebApplicationContext context) {
		sharedWebApplicationContext = context;
	}
	
	public CustomContextLoaderListener() {
			super(sharedWebApplicationContext);
	}
	
	public CustomContextLoaderListener(WebApplicationContext context) {
		super(context);
	}

	@Override
	public void contextInitialized(ServletContextEvent event) {
		initWebApplicationContext(event.getServletContext());
	}
}
