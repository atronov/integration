package atronov.integration.transfer.rmi.spring;

import java.util.NoSuchElementException;

import javax.net.ssl.SSLSocketFactory;

import org.springframework.context.support.AbstractApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;

import atronov.integration.message.IMessage;
import atronov.integration.transfer.ClosableReceiver;
import atronov.integration.transfer.Receiver;
import atronov.integration.transfer.hessian.HessianQueueService;
import atronov.integration.transfer.resource.QueueService;
import atronov.integration.transfer.rmi.spring.context.ContextFactory;

public class SpringTcpReceiver implements ClosableReceiver {

	private QueueService messages;

	private AbstractApplicationContext context;

	static {
		System.setProperty("javax.net.ssl.keyStore", "messaging.keystore");
		System.setProperty("javax.net.ssl.keyStorePassword", "secureexample");
	}

	public SpringTcpReceiver(ContextFactory contextFactory) {
		context = contextFactory.createForServer();
		messages = (QueueService) context.getBean("queue");
	}
	
	@Override
	public <TMessage extends IMessage> IMessage getReceived() {
		try {
			TMessage message = (TMessage) messages.remove();
			return message;
		} catch (NoSuchElementException e) {
			return null;
		}
	}

	@Override
	public void close() throws Exception {
		if (context != null) context.close();
	}
}
