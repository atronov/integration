package atronov.integration.transfer.rmi.spring.context;

import javax.rmi.ssl.SslRMIClientSocketFactory;
import javax.rmi.ssl.SslRMIServerSocketFactory;

import org.springframework.beans.factory.support.BeanDefinitionBuilder;
import org.springframework.context.support.AbstractApplicationContext;
import org.springframework.context.support.GenericApplicationContext;
import org.springframework.remoting.rmi.RmiProxyFactoryBean;
import org.springframework.remoting.rmi.RmiRegistryFactoryBean;
import org.springframework.remoting.rmi.RmiServiceExporter;

import atronov.integration.config.RmiConfig;
import atronov.integration.transfer.resource.MessageQueue;
import atronov.integration.transfer.resource.MessageTopic;
import atronov.integration.transfer.resource.QueueService;
import atronov.integration.transfer.resource.TopicService;

public class TcpSslContextFactory implements ContextFactory {
	@Override
	public AbstractApplicationContext createForClient() {
		BeanDefinitionBuilder queueBuilder = BeanDefinitionBuilder
				.rootBeanDefinition(RmiProxyFactoryBean.class)
				.addPropertyValue("serviceUrl", String.format("rmi://%s:%s/Queue", RmiConfig.get().getHost(), RmiConfig.get().getSecurePort()))
				.addPropertyValue("serviceInterface", QueueService.class)
				.addPropertyValue("registryClientSocketFactory", new SslRMIClientSocketFactory());
		BeanDefinitionBuilder topicBuilder = BeanDefinitionBuilder
				.rootBeanDefinition(RmiProxyFactoryBean.class)
				.addPropertyValue("serviceUrl", String.format("rmi://%s:%s/Topic", RmiConfig.get().getHost(), RmiConfig.get().getSecurePort()))
				.addPropertyValue("serviceInterface", TopicService.class)
				.addPropertyValue("registryClientSocketFactory", new SslRMIClientSocketFactory());
		GenericApplicationContext context = new GenericApplicationContext();
		context.registerBeanDefinition("remoteQueue", queueBuilder.getBeanDefinition());
		context.registerBeanDefinition("remoteTopic", topicBuilder.getBeanDefinition());
		context.refresh();
		return context;
	}
	@Override
	public AbstractApplicationContext createForServer() {
		System.setProperty("java.rmi.server.hostname", RmiConfig.get().getHost());
		BeanDefinitionBuilder queueBuilder = BeanDefinitionBuilder
				.genericBeanDefinition(MessageQueue.class)
				.addConstructorArgValue("SpringTcpSslMessageQueue");
		BeanDefinitionBuilder topicBuilder = BeanDefinitionBuilder
				.genericBeanDefinition(MessageTopic.class)
				.addConstructorArgValue("SpringTcpSslMessageTopic");
		BeanDefinitionBuilder registryBuilder = BeanDefinitionBuilder
				.rootBeanDefinition(RmiRegistryFactoryBean.class)
				.addPropertyValue("port", RmiConfig.get().getSecurePort())
				.addPropertyValue("serverSocketFactory", new SslRMIServerSocketFactory())
				.addPropertyValue("clientSocketFactory", new SslRMIClientSocketFactory());
		BeanDefinitionBuilder queueExporterBuilder = BeanDefinitionBuilder
				.rootBeanDefinition(RmiServiceExporter.class)
				.addPropertyReference("registry", "registry")
				.addPropertyValue("serviceName", "Queue")
				.addPropertyReference("service", "queue")
				.addPropertyValue("serviceInterface", QueueService.class);
		BeanDefinitionBuilder topicExporterBuilder = BeanDefinitionBuilder
				.rootBeanDefinition(RmiServiceExporter.class)
				.addPropertyReference("registry", "registry")
				.addPropertyValue("serviceName", "Topic")
				.addPropertyReference("service", "topic")
				.addPropertyValue("serviceInterface", TopicService.class);
		GenericApplicationContext context = new GenericApplicationContext();
		context.registerBeanDefinition("registry", registryBuilder.getBeanDefinition());
		context.registerBeanDefinition("queue", queueBuilder.getBeanDefinition());
		context.registerBeanDefinition("topic", topicBuilder.getBeanDefinition());
		context.registerBeanDefinition("queueExporter", queueExporterBuilder.getBeanDefinition());
		context.registerBeanDefinition("topicExporter", topicExporterBuilder.getBeanDefinition());
		context.refresh();
		return context;
	}
}
