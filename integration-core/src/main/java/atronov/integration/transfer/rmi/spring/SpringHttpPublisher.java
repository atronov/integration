package atronov.integration.transfer.rmi.spring;

import org.glassfish.grizzly.servlet.ServletRegistration;
import org.glassfish.grizzly.servlet.WebappContext;
import org.springframework.context.ApplicationContext;
import org.springframework.context.support.AbstractApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;
import org.springframework.web.context.ContextLoaderListener;
import org.springframework.web.context.WebApplicationContext;
import org.springframework.web.context.request.RequestContextListener;
import org.springframework.web.context.support.GenericWebApplicationContext;
import org.springframework.web.context.support.HttpRequestHandlerServlet;
import org.springframework.web.context.support.XmlWebApplicationContext;
import org.springframework.web.util.WebUtils;

import atronov.integration.message.IMessage;
import atronov.integration.transfer.ClosableSender;
import atronov.integration.transfer.resource.TopicService;
import atronov.integration.transfer.rmi.spring.context.ContextFactory;
import atronov.integration.transfer.rmi.spring.context.CustomContextLoaderListener;
import atronov.integration.transfer.rmi.spring.context.CustomServlet;
import atronov.integration.transfer.server.CommunicationConfigFactory;
import atronov.integration.transfer.server.Server;

public class SpringHttpPublisher extends Server implements ClosableSender {
	private ContextFactory contextFactory;

	private TopicService topic;
	
	public SpringHttpPublisher(ContextFactory contextFactory, CommunicationConfigFactory config ) {
		super(config);
		this.contextFactory = contextFactory;
		try (AbstractApplicationContext context = this.contextFactory.createForServer()) {
			topic = (TopicService) context.getBean("topic");
		}
		startServer();
	}
	
	
	@Override
	public <TMessage extends IMessage> boolean send(IMessage message) {
		topic.publish(message);
		return true;
	}

	@Override
	protected ServletRegistration addServlet(WebappContext context) {
		GenericWebApplicationContext webContext = new GenericWebApplicationContext(context);
		ApplicationContext nestedContext = contextFactory.createForServer();
		webContext.setParent(nestedContext);
		CustomContextLoaderListener.setWebApplicationContext(webContext);
		webContext.refresh();
		context.addListener(CustomContextLoaderListener.class);
		context.addListener(RequestContextListener.class);
		return context.addServlet("TopicService", HttpRequestHandlerServlet.class);
	}

}
