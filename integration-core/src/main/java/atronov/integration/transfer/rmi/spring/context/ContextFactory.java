package atronov.integration.transfer.rmi.spring.context;

import org.springframework.context.support.AbstractApplicationContext;

public interface ContextFactory {
	AbstractApplicationContext createForClient();
	AbstractApplicationContext createForServer();
}
