package atronov.integration.transfer.rmi.spring.context;

import org.springframework.beans.factory.support.BeanDefinitionBuilder;
import org.springframework.context.support.AbstractApplicationContext;
import org.springframework.context.support.GenericApplicationContext;
import org.springframework.remoting.httpinvoker.HttpInvokerProxyFactoryBean;

import atronov.integration.config.RmiConfig;
import atronov.integration.transfer.resource.QueueService;
import atronov.integration.transfer.resource.TopicService;

public class HttpsContextFactory extends HttpContextFactory {
	@Override
	public AbstractApplicationContext createForClient() {
		BeanDefinitionBuilder queueBuilder = BeanDefinitionBuilder
				.rootBeanDefinition(HttpInvokerProxyFactoryBean.class)
				.addPropertyValue("serviceUrl", String.format("https://%s:443/", RmiConfig.get().getHost()))
				.addPropertyValue("serviceInterface", QueueService.class);
		BeanDefinitionBuilder topicBuilder = BeanDefinitionBuilder
				.rootBeanDefinition(HttpInvokerProxyFactoryBean.class)
				.addPropertyValue("serviceUrl", String.format("https://%s:443/", RmiConfig.get().getHost()))
				.addPropertyValue("serviceInterface", TopicService.class);
		GenericApplicationContext context = new GenericApplicationContext();
		context.registerBeanDefinition("remoteQueue", queueBuilder.getBeanDefinition());
		context.registerBeanDefinition("remoteTopic", topicBuilder.getBeanDefinition());
		context.refresh();
		return context;
	}
}
