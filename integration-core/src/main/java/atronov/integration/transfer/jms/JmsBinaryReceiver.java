package atronov.integration.transfer.jms;

import javax.jms.BytesMessage;
import javax.jms.JMSException;
import javax.jms.Message;
import javax.jms.MessageConsumer;

import org.apache.log4j.Logger;

import atronov.integration.message.MessageHandlingFactory;
import atronov.integration.transfer.jms.resources.JmsResourcesFactory;

public class JmsBinaryReceiver extends JmsReceiver<byte[]> {

	private final Logger log = Logger.getLogger(this.getClass());
	
	public JmsBinaryReceiver(JmsResourcesFactory jmsResources, MessageHandlingFactory<byte[]> handlingFactory) {
		super(jmsResources, handlingFactory);
	}

	@Override
	protected byte[] readMessageData(MessageConsumer consumer) throws JMSException {
		log.info("Read message data start");
		Message rawMessage = consumer.receive(20_000);
		log.info("Received message: "+rawMessage);
		BytesMessage jmsMessage = (BytesMessage) rawMessage;
		if (jmsMessage == null)
			return null;
		byte[] messageData = new byte[(int) jmsMessage.getBodyLength()];
		jmsMessage.readBytes(messageData);
		log.info("Read message data end");
		return messageData;
	}
}
