package atronov.integration.transfer.jms;

import javax.jms.Destination;
import javax.jms.JMSException;
import javax.jms.Message;
import javax.jms.MessageProducer;
import javax.jms.Session;

import org.apache.log4j.Logger;

import atronov.integration.message.IMessage;
import atronov.integration.message.MessageHandlingFactory;
import atronov.integration.message.Serializer;
import atronov.integration.transfer.ClosableSender;
import atronov.integration.transfer.jms.resources.JmsResourcesFactory;

public abstract class JmsSender<TMessageData> extends CommonJmsResource implements ClosableSender {

	private final Logger log = Logger.getLogger(this.getClass());
	
	private final Serializer<TMessageData> serializer;

	public JmsSender(JmsResourcesFactory jmsResources, MessageHandlingFactory<TMessageData> handlingFactory) {
		super(jmsResources);
		this.serializer = handlingFactory.createSerializer();
	}

	@Override
	public <TMessage extends IMessage> boolean send(IMessage message) {
		TMessageData messageData = serializer.serealize(message);
		try {
			Destination destination = getDestination();
			MessageProducer producer = getSession().createProducer(destination);
			Message jmsMessage = createJmsMessage(getSession(), messageData);
			log.info("Send jms-message: "+jmsMessage);
			producer.send(jmsMessage);
			log.debug("jms-message was sent: "+message);
		} catch (JMSException e) {
			log.error("Error while send jms-message",e);
			throw new RuntimeException(e);
		}
		return true;
	}

	protected abstract Message createJmsMessage(Session session, TMessageData messageData) throws JMSException;
}
