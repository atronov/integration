package atronov.integration.transfer.jms.connection;

import javax.jms.Connection;

public class ConnectionInfo {

	private Connection connection;

	public boolean started = false;
	public boolean closePlanned = false;
	
	public ConnectionInfo(Connection connection) {
		this.connection = connection;
	}
	
	public Connection connection() {
		return connection;
	}
}
