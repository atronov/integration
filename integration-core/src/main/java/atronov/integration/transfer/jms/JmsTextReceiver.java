package atronov.integration.transfer.jms;

import javax.jms.JMSException;
import javax.jms.Message;
import javax.jms.MessageConsumer;
import javax.jms.TextMessage;

import org.apache.log4j.Logger;

import atronov.integration.message.MessageHandlingFactory;
import atronov.integration.transfer.jms.resources.JmsResourcesFactory;

public class JmsTextReceiver extends JmsReceiver<String> {

	private final Logger log = Logger.getLogger(this.getClass());
	
	public JmsTextReceiver(JmsResourcesFactory jmsResources, MessageHandlingFactory<String> handlingFactory) {
		super(jmsResources, handlingFactory);
	}

	@Override
	protected String readMessageData(MessageConsumer consumer) throws JMSException {
		Message rawMessage = consumer.receive(20_000);
		log.info("Received message: "+rawMessage);
		TextMessage textMessage = (TextMessage)rawMessage;
		if (textMessage == null)
			return null;
		String messageText = textMessage.getText();
		return messageText;
	}

}
