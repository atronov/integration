package atronov.integration.transfer.jms.resources;

import javax.jms.Connection;
import javax.jms.Destination;
import javax.jms.TopicConnection;

public interface JmsResourcesFactory {
	
	Connection getConnection();
	
	Destination getDestination();
	
	TopicConnection getTopicConnection();

}
