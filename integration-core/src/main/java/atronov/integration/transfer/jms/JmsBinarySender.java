package atronov.integration.transfer.jms;

import javax.jms.BytesMessage;
import javax.jms.JMSException;
import javax.jms.Message;
import javax.jms.Session;

import atronov.integration.message.MessageHandlingFactory;
import atronov.integration.transfer.jms.resources.JmsResourcesFactory;

public class JmsBinarySender extends JmsSender<byte[]> {

	public JmsBinarySender(JmsResourcesFactory jmsResources, MessageHandlingFactory<byte[]> handlingFactory) {
		super(jmsResources, handlingFactory);
	}

	@Override
	protected Message createJmsMessage(Session session, byte[] messageData) throws JMSException {
		BytesMessage message = session.createBytesMessage();
		message.writeBytes(messageData);
		return message;
	}
}
