package atronov.integration.transfer.jms;

import javax.jms.BytesMessage;
import javax.jms.JMSException;
import javax.jms.MessageConsumer;

import atronov.integration.message.MessageHandlingFactory;
import atronov.integration.transfer.Receiver;
import atronov.integration.transfer.jms.resources.JmsResourcesFactory;

public class JmsBinarySubscriber extends JmsSubscriber<byte[]> implements Receiver {

	public JmsBinarySubscriber(JmsResourcesFactory jmsResources, MessageHandlingFactory<byte[]> handlingFactory) {
		super(jmsResources, handlingFactory);
	}

	@Override
	protected byte[] readMessageData(MessageConsumer consumer) throws JMSException {
		BytesMessage jmsMessage = (BytesMessage) consumer.receive();
		if (jmsMessage == null)
			return null;
		byte[] messageData = new byte[(int) jmsMessage.getBodyLength()];
		jmsMessage.readBytes(messageData);
		return messageData;
	}

}
