package atronov.integration.transfer.jms.connection;

import java.util.Collections;
import java.util.HashMap;
import java.util.Map;

import javax.jms.Connection;
import javax.jms.ConnectionConsumer;
import javax.jms.ConnectionFactory;
import javax.jms.ConnectionMetaData;
import javax.jms.Destination;
import javax.jms.ExceptionListener;
import javax.jms.JMSException;
import javax.jms.ServerSessionPool;
import javax.jms.Session;
import javax.jms.Topic;

public class ConnectionProxy implements Connection {

	private int clientId;

	private static Map<Integer, ConnectionInfo> connectionsStore;

	static {
		synchronized (ConnectionProxy.class) {
			connectionsStore = Collections.synchronizedMap(new HashMap<Integer, ConnectionInfo>());
		}
	}

	private Connection getConnection() {
		return getConnectionInfo().connection();
	}
	
	private ConnectionInfo getConnectionInfo() {
		ConnectionInfo connection = connectionsStore.get(clientId);
		return connection;
	}

	public ConnectionProxy(ConnectionFactory factory, Integer clientId, String user, String password) throws JMSException {
		this.clientId = clientId;
		Connection  connection = factory.createConnection(user, password);
		store(clientId, connection);
	}

	public ConnectionProxy(ConnectionFactory factory, Integer clientId) throws JMSException {
		this.clientId = clientId;
		Connection  connection = factory.createConnection();
		store(clientId, connection);
	}
	
	private void store(Integer clientId, Connection connection) {
		if (!connectionsStore.containsKey(clientId)) {
			System.out.println("Create connection for "+this.clientId);
			connectionsStore.put(clientId, new ConnectionInfo(connection));
		} else
			System.out.println("Connection for "+this.clientId+" already exists");
	}

	@Override
	public ConnectionConsumer createConnectionConsumer(Destination arg0, String arg1, ServerSessionPool arg2, int arg3) throws JMSException {
		return getConnection().createConnectionConsumer(arg0, arg1, arg2, arg3);
	}

	@Override
	public ConnectionConsumer createDurableConnectionConsumer(Topic arg0, String arg1, String arg2, ServerSessionPool arg3, int arg4)
			throws JMSException {
		return getConnection().createDurableConnectionConsumer(arg0, arg1, arg2, arg3, arg4);
	}

	@Override
	public Session createSession(boolean arg0, int arg1) throws JMSException {
		return getConnection().createSession(arg0, arg1);
	}

	@Override
	public String getClientID() throws JMSException {
		return getConnection().getClientID();
	}

	@Override
	public ExceptionListener getExceptionListener() throws JMSException {
		return getConnection().getExceptionListener();
	}

	@Override
	public ConnectionMetaData getMetaData() throws JMSException {
		return getConnection().getMetaData();
	}

	@Override
	public void setClientID(String arg0) throws JMSException {
		getConnection().setClientID(arg0);
	}

	@Override
	public void setExceptionListener(ExceptionListener arg0) throws JMSException {
		getConnection().setExceptionListener(arg0);
	}

	@Override
	public void start() throws JMSException {
		synchronized (this) {
			if (!getConnectionInfo().started) {
				getConnectionInfo().started = true;
				System.out.println("Start connection for " + clientId);
				getConnection().start();
			} else {
				System.out.println("Connection already started for " + clientId);
			}
		}
	}

	@Override
	public void stop() throws JMSException {
		if (getConnectionInfo().started) {
			getConnectionInfo().started = false;
			System.out.println("Stop connection for " + clientId);
			getConnection().stop();
		} else {
			System.out.println("Connection already stoppe for " + clientId);
		}
	}
	
	@Override
	public void close() throws JMSException {
		synchronized (this) {
			if (!getConnectionInfo().closePlanned) {
				getConnectionInfo().closePlanned = true;
				System.out.println("Plan connecion close for " + clientId);
				getConnection().close();
				//planClose(getConnection());
			} else {
				System.out.println("Connection close already planned for " + clientId);
			}
		}
	}

	private static void planStop(final Connection connection) {
		Runtime.getRuntime().addShutdownHook(new Thread() {
			public void run() {
				try {
					System.out.println("Stop proxy connection");
					connection.stop();
				} catch (JMSException e) {
					e.printStackTrace();
				}
			}
		});
	}
	
	private static void planClose(final Connection connection) {
		Runtime.getRuntime().addShutdownHook(new Thread() {
			public void run() {
				try {
					System.out.println("Close proxy connection");
					connection.close();
				} catch (JMSException e) {
					e.printStackTrace();
				}
			}
		});
	}
}
