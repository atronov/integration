package atronov.integration.transfer.jms;

import javax.jms.JMSException;
import javax.jms.MessageConsumer;
import javax.jms.TextMessage;

import atronov.integration.message.MessageHandlingFactory;
import atronov.integration.transfer.jms.resources.JmsResourcesFactory;

public class JmsTextSubscriber extends JmsSubscriber<String>{

	public JmsTextSubscriber(JmsResourcesFactory jmsResources, MessageHandlingFactory<String> handlingFactory) {
		super(jmsResources, handlingFactory);
	}
	
	@Override
	protected String readMessageData(MessageConsumer consumer) throws JMSException {
		TextMessage textMessage = (TextMessage)consumer.receive();
		if (textMessage == null)
			return null;
		String messageText = textMessage.getText();
		return messageText;
	}
}
