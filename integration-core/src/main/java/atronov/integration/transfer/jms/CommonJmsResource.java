package atronov.integration.transfer.jms;

import javax.jms.Connection;
import javax.jms.Destination;
import javax.jms.JMSException;
import javax.jms.Session;

import org.apache.log4j.Logger;

import atronov.integration.transfer.jms.resources.JmsResourcesFactory;

public class CommonJmsResource implements AutoCloseable {
	
	private final Logger log = Logger.getLogger(this.getClass());
	
	private final JmsResourcesFactory jmsResources;
	
	private Connection connection = null;
	private Session session = null;
	private Destination destination = null;

	public CommonJmsResource(JmsResourcesFactory jmsResources) {
		this.jmsResources = jmsResources;
	}
	
	protected Session getSession() {
		if (session == null) {
			try {
				log.info("Create jms-session");
				connection = jmsResources.getConnection();
				connection.start();
				session = connection.createSession(false, Session.CLIENT_ACKNOWLEDGE);
			} catch (JMSException e) {
				log.error("Error while creating jms-session", e);
				throw new RuntimeException(e);
			}
		}
		return session;
	}
	
	protected Destination getDestination() {
		if (destination == null) {
			destination = jmsResources.getDestination();
		}
		return destination;
	}
	
	@Override
	public void close() throws Exception {
		if (connection != null) {
			log.info("Close jms-connection");
			connection.close();
			log.debug("jms-connection clossed");
		}
	}
	
}
