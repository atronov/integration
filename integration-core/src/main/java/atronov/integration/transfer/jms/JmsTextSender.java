package atronov.integration.transfer.jms;

import javax.jms.JMSException;
import javax.jms.Message;
import javax.jms.Session;
import javax.jms.TextMessage;

import atronov.integration.message.MessageHandlingFactory;
import atronov.integration.transfer.jms.resources.JmsResourcesFactory;

public class JmsTextSender extends JmsSender<String> {

	public JmsTextSender(JmsResourcesFactory jmsResources, MessageHandlingFactory<String> handlingFactory) {
		super(jmsResources, handlingFactory);
	}

	@Override
	protected Message createJmsMessage(Session session, String messageData) throws JMSException {
		TextMessage message = session.createTextMessage(messageData);
		return message;
	}
	
}
