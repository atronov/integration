package atronov.integration.transfer.jms;

import javax.jms.JMSException;
import javax.jms.MessageConsumer;

import org.apache.log4j.Logger;

import atronov.integration.message.IMessage;
import atronov.integration.message.MessageHandlingFactory;
import atronov.integration.message.Serializer;
import atronov.integration.message.TypeRecognition;
import atronov.integration.transfer.ClosableReceiver;
import atronov.integration.transfer.jms.resources.JmsResourcesFactory;

public abstract class JmsReceiver<TMessageData> extends CommonJmsResource implements ClosableReceiver {

	private final Logger log = Logger.getLogger(this.getClass());
	
	private final Serializer<TMessageData> serializer;
	private final TypeRecognition<TMessageData> recognition;

	private MessageConsumer consumer;
	
	public JmsReceiver(JmsResourcesFactory jmsResources, MessageHandlingFactory<TMessageData> handlingFactory) {
		super(jmsResources);
		this.recognition = handlingFactory.createRecognition();
		this.serializer = handlingFactory.createSerializer();
	}
	
	private MessageConsumer getConsumer() throws JMSException {
		synchronized (this) {
			if (consumer == null)
				consumer = getSession().createConsumer(getDestination());
			return consumer; 
		}
	}
	@Override
	public void close() throws Exception {
		consumer = null;
		super.close();
	}

	@Override
	public <TMessage extends IMessage> IMessage getReceived() {
		try {
				MessageConsumer consumer = getConsumer();
				log.info("Receive jms-message start");
				TMessageData mesageData = readMessageData(consumer);
				if (mesageData == null) return null;
				Class<? extends IMessage> messageType = recognition.recognize(mesageData);
				IMessage message = serializer.deserealize(mesageData, messageType);
				log.info("Receive jms-message end");
				return message;
		} catch (JMSException e) {
			throw new RuntimeException(e);
		}
	}

	protected abstract TMessageData readMessageData(MessageConsumer consumer) throws JMSException;
}
