package atronov.integration.transfer.jms;

import javax.jms.JMSException;
import javax.jms.MessageConsumer;
import javax.jms.Session;
import javax.jms.Topic;
import javax.jms.TopicConnection;
import javax.jms.TopicSession;

import atronov.integration.message.IMessage;
import atronov.integration.message.MessageHandlingFactory;
import atronov.integration.message.Serializer;
import atronov.integration.message.TypeRecognition;
import atronov.integration.transfer.ClosableReceiver;
import atronov.integration.transfer.Receiver;
import atronov.integration.transfer.jms.resources.JmsResourcesFactory;

public abstract class JmsSubscriber<TMessageData> implements ClosableReceiver  {

	private final Serializer<TMessageData> serializer;
	private final TypeRecognition<TMessageData> recognition;
	private final JmsResourcesFactory jmsResources;
	
	private MessageConsumer consumer;
	private TopicConnection connection;
	private TopicSession session;
	
	
	public JmsSubscriber(JmsResourcesFactory jmsResources, MessageHandlingFactory<TMessageData> handlingFactory) {
		this.jmsResources = jmsResources;
		this.recognition = handlingFactory.createRecognition();
		this.serializer = handlingFactory.createSerializer();
		subscribe();
	}
	
	protected void subscribe() {
		try {
			connection = jmsResources.getTopicConnection();
			connection.start();
			session = connection.createTopicSession(false, Session.AUTO_ACKNOWLEDGE);
			Topic topic = (Topic) jmsResources.getDestination();
			consumer = session.createSubscriber(topic);
		} catch (JMSException e) {
			throw new RuntimeException(e);
		}
	}
	
	@Override
	public <TMessage extends IMessage> IMessage getReceived() {
		try {
		TMessageData mesageData = readMessageData(consumer);
		Class<? extends IMessage> messageType = recognition.recognize(mesageData);
		IMessage message = serializer.deserealize(mesageData, messageType);
		return message;
		} catch (JMSException e) {
			throw new RuntimeException(e);
		}
	}
	
	private void releaseResources() {
		try {
			if (consumer != null) consumer.close();
		} catch (Throwable t) { t.printStackTrace();}
		try {
			if (session != null) session.close();
		} catch (Throwable t) { t.printStackTrace();}
		try {
			if (connection != null) connection.close();
		} catch (Throwable t) { t.printStackTrace();}
		consumer = null;
		session = null;
		consumer = null;
	}
	
	@Override
	public void close() throws Exception {
		releaseResources();
	}
	
	protected abstract TMessageData readMessageData(MessageConsumer consumer) throws JMSException;

}
