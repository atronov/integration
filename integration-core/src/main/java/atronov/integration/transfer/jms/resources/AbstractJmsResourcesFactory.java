package atronov.integration.transfer.jms.resources;

import java.util.Properties;

import javax.jms.Connection;
import javax.jms.ConnectionFactory;
import javax.jms.Destination;
import javax.jms.JMSException;
import javax.jms.TopicConnection;
import javax.jms.TopicConnectionFactory;
import javax.naming.Context;
import javax.naming.InitialContext;
import javax.naming.NamingException;

public abstract class AbstractJmsResourcesFactory implements JmsResourcesFactory {

	private Context context;
	
	@Override
	public Connection getConnection() {
		try {
			Object tmp = context().lookup(connectionFactory());
			ConnectionFactory connectionFactory = (ConnectionFactory) tmp;
			return connectionFactory.createConnection();
		} catch (NamingException | JMSException e) {
			throw new RuntimeException(e);
		}
	}
	
	@Override
	public TopicConnection getTopicConnection() {
		try {
			Object tmp = context().lookup(connectionFactory());
			TopicConnectionFactory connectionFactory = (TopicConnectionFactory) tmp;
			return connectionFactory.createTopicConnection();
		} catch (NamingException | JMSException e) {
			throw new RuntimeException(e);
		}
	}

	@Override
	public Destination getDestination() {
		try {
			Destination destination = (Destination) context().lookup(destiation());
			return destination;
		} catch (NamingException e) {
			throw new RuntimeException(e);
		}
	}
	
	
	
	private Context context() {
		try {
			context = new InitialContext(jndiProperties());
			return context;
		} catch (NamingException e) {
			throw new RuntimeException(e);
		}
	}
	
	protected abstract Properties jndiProperties();
	protected abstract String destiation();
	protected abstract String connectionFactory();
}
