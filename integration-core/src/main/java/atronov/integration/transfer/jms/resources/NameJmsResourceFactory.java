package atronov.integration.transfer.jms.resources;

public abstract class NameJmsResourceFactory extends AbstractJmsResourcesFactory {
	
	private String factoryName;
	private String destinationName;
	
	public NameJmsResourceFactory(String factoryName, String destinationName) {
		this.factoryName = factoryName;
		this.destinationName = destinationName;
	}
	
	@Override
	protected String destiation() {
		return destinationName;
	}

	@Override
	protected String connectionFactory() {
		return factoryName;
	}
	
}
