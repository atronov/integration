package atronov.integration.transfer.jms.resources;

import java.util.Properties;

import atronov.integration.config.JmsConfing;

public class TcpJmsResourcesFactory extends NameJmsResourceFactory {
	
	public TcpJmsResourcesFactory(String factoryName, String destinationName) {
		super(factoryName, destinationName);
	}

	static {
		System.setProperty("javax.net.ssl.trustStore", "messaging.truststore");
		System.setProperty("javax.net.ssl.trustStorePassword", "secureexample");
	}
	
	@Override
	protected Properties jndiProperties() {
		Properties props = new Properties();
		props.setProperty("java.naming.factory.initial", "org.jnp.interfaces.NamingContextFactory");
		props.setProperty("java.naming.factory.url.pkgs", "org.jboss.naming:org.jnp.interfaces");
		props.setProperty("java.naming.provider.url",
				String.format("jnp://%s:%s",JmsConfing.get().jmsHost(),JmsConfing.get().jmsTcpJndiPort()));
		return props;
	}
}
