package atronov.integration.transfer.jms.resources;

import java.util.Properties;

import atronov.integration.config.JmsConfing;

public class HttpJmsResourcesFactory extends NameJmsResourceFactory {

	public HttpJmsResourcesFactory(String factoryName, String destinationName) {
		super(factoryName, destinationName);
	}

	@Override
	protected Properties jndiProperties() {
		Properties props = new Properties();
		props.setProperty("java.naming.factory.initial", "org.jboss.naming.HttpNamingContextFactory");
		props.setProperty("java.naming.factory.url.pkgs", "org.jboss.naming:org.jnp.interfaces");
		props.setProperty("java.naming.provider.url",
				String.format("http://%s:%s/invoker/JNDIFactory",JmsConfing.get().jmsHost(),JmsConfing.get().jmsHttpJndiPort()));
		return props;
				
	}
	
}
