package atronov.integration.transfer;

import atronov.integration.message.IMessage;

public interface Publisher {
	<TMessage extends IMessage> boolean publish(IMessage message);
}
