package atronov.integration.transfer;

import atronov.integration.message.IMessage;

public interface Receiver {
	<TMessage extends IMessage> IMessage getReceived();
}
