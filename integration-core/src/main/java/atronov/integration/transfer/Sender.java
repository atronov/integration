package atronov.integration.transfer;

import atronov.integration.message.IMessage;

public interface Sender {
	<TMessage extends IMessage> boolean send(IMessage message);
}
