package atronov.integration.transfer;

public interface ClosableReceiver extends AutoCloseable, Receiver {
}
