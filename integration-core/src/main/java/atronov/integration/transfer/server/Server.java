package atronov.integration.transfer.server;

import java.io.IOException;
import java.net.URI;

import org.apache.log4j.Logger;
import org.glassfish.grizzly.http.server.HttpServer;
import org.glassfish.grizzly.servlet.ServletRegistration;
import org.glassfish.grizzly.servlet.WebappContext;

import atronov.integration.utility.Wait;

import com.sun.jersey.api.container.grizzly2.GrizzlyServerFactory;

public abstract class Server implements AutoCloseable {

	private final Logger log = Logger.getLogger(this.getClass());
	
	private HttpServer server;
	private final CommunicationConfigFactory serverConfig;
	
	protected abstract ServletRegistration addServlet(WebappContext context);
	
	public Server(CommunicationConfigFactory serverConfig) {
		this.serverConfig = serverConfig;
	}
	
	protected void startServer() {
		server = initializeServer();
		try {
			log.info("Start http-server.");
			server.start();
			boolean started = Wait.until(new Wait.Condition() {
				public boolean check() { return server.isStarted(); }
			}).forPeriod(5000).go();
			log.info("Http-server start period ended. Start result: " + started);
		} catch (IOException e) {
			e.printStackTrace();
			throw new RuntimeException(e);
		}
	}
	
	private HttpServer initializeServer() {
		log.info("Config http-server before start.");
		WebappContext ctx = new WebappContext("context");
		ServletRegistration reg = addServlet(ctx);
		reg.addMapping("/*");
		URI uri = serverConfig.getHostUri();
		HttpServer server = null;
		try {
			server = GrizzlyServerFactory.createHttpServer(
					uri
					, null
					, serverConfig.isSecure()
					, serverConfig.getSslConfigurator()
				);
			ctx.deploy(server);
		} catch (IllegalArgumentException | NullPointerException | IOException e) {
			throw new RuntimeException(e);
		}
		return server;
	}
	
	@Override
	public void close() {
		server.stop();
	}
}
