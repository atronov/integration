package atronov.integration.transfer.server;

import java.io.FileInputStream;
import java.io.IOException;
import java.net.URI;
import java.security.KeyStore;

import javax.net.ssl.HostnameVerifier;
import javax.net.ssl.HttpsURLConnection;
import javax.net.ssl.SSLContext;
import javax.net.ssl.SSLSession;
import javax.net.ssl.TrustManagerFactory;
import javax.ws.rs.core.UriBuilder;

import org.glassfish.grizzly.ssl.SSLContextConfigurator;
import org.glassfish.grizzly.ssl.SSLEngineConfigurator;

import atronov.integration.config.HttpServerConfig;

import com.sun.jersey.api.client.Client;
import com.sun.jersey.api.client.config.ClientConfig;
import com.sun.jersey.api.client.config.DefaultClientConfig;
import com.sun.jersey.client.urlconnection.HTTPSProperties;

public class SecureCommunicationFactory implements CommunicationConfigFactory {

	static {
		System.setProperty("javax.net.ssl.trustStore", "messaging.truststore");
		System.setProperty("javax.net.ssl.trustStorePassword", "secureexample");
    HttpsURLConnection.setDefaultHostnameVerifier(new HostnameVerifier() {
			@Override
			public boolean verify(String arg0, SSLSession arg1) {
				return true;
			}
	  });
	}
	
	@Override
	public boolean isSecure() {
		return true;
	}

	@Override
	public SSLEngineConfigurator getSslConfigurator() {
		// Grizzly ssl configuration
		SSLContextConfigurator sslContext = new SSLContextConfigurator();
		// set up security context
		sslContext.setKeyStoreFile("messaging.keystore"); // contains server keypair
		sslContext.setKeyStorePass("secureexample");
		return new SSLEngineConfigurator(sslContext).setClientMode(false).setNeedClientAuth(false);
	}

	@Override
	public URI getHostUri() {
		HttpServerConfig config = HttpServerConfig.get();
		String host = config.host();
		int port = config.securePort();
		return UriBuilder.fromUri("https://"+host).port(port).build();
	}

	@Override
	public Client createClient() {

		ClientConfig config;
		try {
			config = new DefaultClientConfig();
			SSLContext ctx = SSLContext.getInstance("SSL");
			KeyStore trustStore;
			trustStore = KeyStore.getInstance("JKS");
			trustStore.load(new FileInputStream("messaging.truststore"), "secureexample".toCharArray());
			TrustManagerFactory tmf = TrustManagerFactory.getInstance("SunX509");
			tmf.init(trustStore);
			ctx.init(null, tmf.getTrustManagers(), null);
			HostnameVerifier vrf = new HostnameVerifier() {
				public boolean verify(String hostname, javax.net.ssl.SSLSession sslSession) {
					return true;
				}
			};
			config.getProperties().put(HTTPSProperties.PROPERTY_HTTPS_PROPERTIES, new HTTPSProperties(vrf, ctx));
		} catch (java.security.GeneralSecurityException | IOException e) {
			throw new RuntimeException(e);
		}
		return Client.create(config);
	}

	@Override
	public URI getResourceUri() {
		return getHostUri().resolve("/rest");
	}
}
