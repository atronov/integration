package atronov.integration.transfer.server;

import java.net.URI;

import org.glassfish.grizzly.ssl.SSLEngineConfigurator;
import com.sun.jersey.api.client.Client;

public interface CommunicationConfigFactory {

	boolean isSecure();
	
	SSLEngineConfigurator getSslConfigurator();
	
	URI getHostUri();
	
	URI getResourceUri();
	
	Client createClient();
}
