package atronov.integration.transfer.server;

import java.net.URI;

import javax.ws.rs.core.UriBuilder;

import org.glassfish.grizzly.ssl.SSLEngineConfigurator;

import atronov.integration.config.HttpServerConfig;

import com.sun.jersey.api.client.Client;

public class SimpleCommunicationFactory implements CommunicationConfigFactory {

	@Override
	public boolean isSecure() {
		return false;
	}

	@Override
	public SSLEngineConfigurator getSslConfigurator() {
		return null;
	}

	@Override
	public URI getHostUri() {
		HttpServerConfig config = HttpServerConfig.get();
		String host = config.host();
		int port = config.port();
		return UriBuilder.fromUri("http://"+host).port(port).build();
	}

	@Override
	public Client createClient() {
		return Client.create();
	}

	@Override
	public URI getResourceUri() {
		return getHostUri().resolve("/rest");
	}
}
