package atronov.integration.transfer.resource;

import atronov.integration.message.IMessage;

public interface QueueService {

	void push(IMessage message);
	
	IMessage remove();
}
