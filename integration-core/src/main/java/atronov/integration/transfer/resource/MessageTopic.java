package atronov.integration.transfer.resource;

import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Queue;
import java.util.concurrent.LinkedTransferQueue;

import com.google.common.collect.Lists;
import com.google.inject.Inject;
import com.google.inject.name.Named;

import atronov.integration.message.IMessage;

public class MessageTopic implements TopicService {
	
	private final static Map<String, Map<String, Queue<IMessage>>> storageMapping = new HashMap<>();
	
	private final Map<String, Queue<IMessage>> storage;
	
	@Inject
	public MessageTopic(@Named("OwnerId") String ownerId) {
		storage = getStorage(ownerId);
	}
	
	private static Map<String, Queue<IMessage>> getStorage(String ownerId) {
		if (storageMapping.containsKey(ownerId))
			return storageMapping.get(ownerId);
		else {
			Map<String, Queue<IMessage>> createdStorage = new HashMap<>();
			storageMapping.put(ownerId, createdStorage);
			return createdStorage;
		}
	}
	
	public void subscribe(String subscriberId) {
		if (!storage.containsKey(subscriberId))
			storage.put(subscriberId, new LinkedTransferQueue<IMessage>());
		else
			throw new IllegalStateException("Subscriber with the same ID already exists");
	}
	
	public void publish(IMessage message) {
		for (Entry<String, Queue<IMessage>> entry : storage.entrySet()) {
			entry.getValue().add(message);
		}
	}
	
	public IMessage receive(String subscriberId) {
		if (storage.containsKey(subscriberId)) {
			Queue<IMessage> subscriberQueue = storage.get(subscriberId);
			return subscriberQueue.poll();
		} else {
			return null;
		}
	}
	
	public Collection<IMessage> receiveAll(String subscriberId) {
		if (!storage.containsKey(subscriberId)) {
			Queue<IMessage> subscriberQueue = storage.get(subscriberId);
			ArrayList<IMessage> resultList = new ArrayList<>(subscriberQueue);
			return resultList;
		} else {
			return Lists.newArrayList();
		}
	}
}
