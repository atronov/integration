package atronov.integration.transfer.resource;

import java.util.HashMap;
import java.util.Map;
import java.util.Queue;

import org.glassfish.grizzly.utils.LinkedTransferQueue;

import com.google.inject.Inject;
import com.google.inject.name.Named;

import atronov.integration.message.IMessage;

public class MessageQueue implements QueueService {

private final static Map<String, Queue<IMessage>> storageMapping = new HashMap<>();
	
	private final Queue<IMessage> storage;
	
	@Inject
	public MessageQueue(@Named("OwnerId") String ownerId) {
		storage = getStorage(ownerId);
	}
	
	private static Queue<IMessage> getStorage(String ownerId) {
		if (storageMapping.containsKey(ownerId))
			return storageMapping.get(ownerId);
		else {
			Queue<IMessage> createdStorage = new LinkedTransferQueue<>();
			storageMapping.put(ownerId, createdStorage);
			return createdStorage;
		}
	}
	
	@Override
	public void push(IMessage message) {
		storage.add(message);
	}

	public IMessage remove() {
		return storage.poll();
	}

}
