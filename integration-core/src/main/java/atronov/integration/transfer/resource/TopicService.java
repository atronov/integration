package atronov.integration.transfer.resource;

import java.util.Collection;

import atronov.integration.message.IMessage;

public interface TopicService {

	void subscribe(String subscriberId);
	
	IMessage receive(String subscriberId);
	
	Collection<IMessage> receiveAll(String subscriberId);
	
	void publish(IMessage message);
}
