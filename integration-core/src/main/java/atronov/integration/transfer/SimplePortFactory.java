package atronov.integration.transfer;

import java.io.IOException;
import java.net.ServerSocket;
import java.util.Random;

public class SimplePortFactory {

	private static Random rand = new Random();

	public static int freePort() {
		final int floor = 1000;
		final int celling = 100000;
		int port = 7778;
//		ServerSocket socket = null;
//		while (socket == null) {
//			try {
//				socket = new ServerSocket(port);
//			} catch (IOException ex) {
//				continue; // try next port
//			}
//			port = rand.nextInt(celling - floor) + floor;
//		}
		//socket.close();
		return port;
	}
}
