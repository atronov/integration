package atronov.integration.transfer;

public interface ClosableSender extends Sender, AutoCloseable {
}
