package atronov.integration.transfer.rest;

import atronov.integration.message.MessageHandlingFactory;
import atronov.integration.transfer.server.CommunicationConfigFactory;

import com.sun.jersey.api.client.ClientResponse;

public class RestTextSubscriber extends RestSubscriber<String> {

	public RestTextSubscriber(CommunicationConfigFactory communication, MessageHandlingFactory<String> handlingFactory) {
		super(communication, handlingFactory);
	}

	@Override
	protected String extractMessageData(ClientResponse response) {
		return response.getEntity(String.class);
	}

}
