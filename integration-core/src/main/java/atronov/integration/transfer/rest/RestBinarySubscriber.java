package atronov.integration.transfer.rest;

import atronov.integration.message.MessageHandlingFactory;
import atronov.integration.transfer.server.CommunicationConfigFactory;

import com.sun.jersey.api.client.ClientResponse;

public class RestBinarySubscriber extends RestSubscriber<byte[]> {

	public RestBinarySubscriber(CommunicationConfigFactory communication, MessageHandlingFactory<byte[]> handlingFactory) {
		super(communication, handlingFactory);
	}

	@Override
	protected byte[] extractMessageData(ClientResponse response) {
		return response.getEntity(byte[].class);
	}

}
