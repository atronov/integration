package atronov.integration.transfer.rest.handling;

import atronov.integration.message.IMessage;
import atronov.integration.message.Serializer;
import atronov.integration.message.TypeRecognition;
import atronov.integration.transfer.resource.MessageQueue;

import com.google.inject.Inject;

public class SerializeQueueMessageHandler<TMessageData> implements MessageReceptionHandler<TMessageData> {
	
	@Inject
	private MessageQueue storage;
	@Inject
	private Serializer<TMessageData> serializer;
	@Inject
	private TypeRecognition<TMessageData> recognition;
	
	@Override
	public boolean handle(TMessageData messageData) {
		Class<? extends IMessage> messageClass = recognition.recognize(messageData);
		IMessage message = serializer.deserealize(messageData, messageClass);
		storage.push(message);
		return true;
	}
}
