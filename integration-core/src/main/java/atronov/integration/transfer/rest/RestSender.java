package atronov.integration.transfer.rest;

import atronov.integration.message.IMessage;
import atronov.integration.message.MessageHandlingFactory;
import atronov.integration.message.Serializer;
import atronov.integration.transfer.Sender;
import atronov.integration.transfer.server.CommunicationConfigFactory;

import com.sun.jersey.api.client.Client;
import com.sun.jersey.api.client.ClientResponse;
import com.sun.jersey.api.client.WebResource;

public abstract class RestSender<TMessageData> implements Sender {

	private CommunicationConfigFactory comminication;
	
	private Serializer<TMessageData> serializer;
	
	public RestSender(CommunicationConfigFactory communication, MessageHandlingFactory<TMessageData> handlingFactory) {
		this.serializer = handlingFactory.createSerializer();
		this.comminication = communication;
	}
	
	protected abstract String messageMediaType();
	
	@Override
	public <TMessage extends IMessage> boolean send(IMessage message) {
		Client client = comminication.createClient();
		WebResource webResource = client.resource(comminication.getResourceUri());
		TMessageData messageData = serializer.serealize(message);
		ClientResponse response = webResource.type(messageMediaType()).post(ClientResponse.class, messageData);
		System.out.println("Sent rest: "+messageData);
		if (response.getStatus() != 200) {
			System.err.print("Failed : HTTP error code : " + response.getStatus());
			return false;
		}
		return true;
	}
}