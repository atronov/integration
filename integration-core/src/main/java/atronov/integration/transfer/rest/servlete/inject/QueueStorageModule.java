package atronov.integration.transfer.rest.servlete.inject;

import atronov.integration.transfer.resource.MessageQueue;
import atronov.integration.transfer.resource.QueueService;

import com.google.inject.name.Names;
import com.google.inject.servlet.ServletModule;
import com.sun.jersey.guice.spi.container.servlet.GuiceContainer;

public class QueueStorageModule extends ServletModule {

	private final String ownerId;
	
	public QueueStorageModule(String ownerId) {
		this.ownerId = ownerId;
	}
	
	@Override
	protected void configureServlets() {
		bind(String.class).annotatedWith(Names.named("OwnerId")).toInstance(ownerId);
		bind(QueueService.class).to(MessageQueue.class);
		serve("*").with(GuiceContainer.class);
	}
	
	
}
