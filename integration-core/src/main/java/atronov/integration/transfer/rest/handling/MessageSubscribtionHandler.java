package atronov.integration.transfer.rest.handling;

public interface MessageSubscribtionHandler<TMessageData> {

	void subscribe(String subscriberId);
	
	TMessageData receive(String subscriberId);
}
