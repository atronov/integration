package atronov.integration.transfer.rest.servlete.inject;

import atronov.integration.message.Serializer;
import atronov.integration.message.TypeRecognition;
import atronov.integration.transfer.rest.handling.MessageSubscribtionHandler;
import atronov.integration.transfer.rest.handling.SerializeTopicMessageHandler;
import atronov.integration.transfer.rest.servlete.TopicBinaryPublisherServlet;

import com.google.inject.TypeLiteral;
import com.google.inject.servlet.ServletModule;
import com.sun.jersey.guice.spi.container.servlet.GuiceContainer;

public class BinaryTopicServleteModule extends ServletModule {
	
	private final Serializer<byte[]> serializer;
	private final TypeRecognition<byte[]> recognition;
	
	public BinaryTopicServleteModule(Serializer<byte[]> serializer, TypeRecognition<byte[]> recognition) {
		this.serializer = serializer;
		this.recognition = recognition;
	}
	
	@Override
	protected void configureServlets() {
		bind(new TypeLiteral<Serializer<byte[]>>(){}).toInstance(serializer);
		bind(new TypeLiteral<TypeRecognition<byte[]>>(){}).toInstance(recognition);
		bind(new TypeLiteral<MessageSubscribtionHandler<byte[]>>(){}).to(new TypeLiteral<SerializeTopicMessageHandler<byte[]>>(){});
		bind(TopicBinaryPublisherServlet.class);
		serve("*").with(GuiceContainer.class);
	}
}
