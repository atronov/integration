package atronov.integration.transfer.rest;

import javax.ws.rs.core.MediaType;

import atronov.integration.message.MessageHandlingFactory;
import atronov.integration.transfer.server.CommunicationConfigFactory;

public class RestBinarySender extends RestSender<byte[]> {

	public RestBinarySender(CommunicationConfigFactory communication, MessageHandlingFactory<byte[]> handlingFactory) {
		super(communication, handlingFactory);
	}

	@Override
	protected String messageMediaType() {
		return MediaType.APPLICATION_OCTET_STREAM;
	}
}
