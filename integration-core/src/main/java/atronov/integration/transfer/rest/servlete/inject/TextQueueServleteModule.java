package atronov.integration.transfer.rest.servlete.inject;

import atronov.integration.message.Serializer;
import atronov.integration.message.TypeRecognition;
import atronov.integration.transfer.rest.handling.MessageReceptionHandler;
import atronov.integration.transfer.rest.handling.SerializeQueueMessageHandler;
import atronov.integration.transfer.rest.servlete.QueueTextReceiverServlet;

import com.google.inject.TypeLiteral;
import com.google.inject.servlet.ServletModule;
import com.sun.jersey.guice.spi.container.servlet.GuiceContainer;

public class TextQueueServleteModule extends ServletModule {

	private final Serializer<String> serializer;
	private final TypeRecognition<String> recognition;
	
	public TextQueueServleteModule(Serializer<String> serializer, TypeRecognition<String> recognition) {
		this.serializer = serializer;
		this.recognition = recognition;
	}
	
	@Override
	protected void configureServlets() {
		bind(new TypeLiteral<Serializer<String>>(){}).toInstance(serializer);
		bind(new TypeLiteral<TypeRecognition<String>>(){}).toInstance(recognition);
		bind(new TypeLiteral<MessageReceptionHandler<String>>(){}).to(new TypeLiteral<SerializeQueueMessageHandler<String>>(){});
		bind(QueueTextReceiverServlet.class);
		serve("*").with(GuiceContainer.class);
	}
}
