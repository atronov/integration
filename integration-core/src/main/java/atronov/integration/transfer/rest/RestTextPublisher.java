package atronov.integration.transfer.rest;

import javax.servlet.Servlet;

import atronov.integration.message.MessageHandlingFactory;
import atronov.integration.transfer.rest.servlete.inject.TextTopicServleteModule;
import atronov.integration.transfer.rest.servlete.inject.TopicStorageModule;
import atronov.integration.transfer.server.CommunicationConfigFactory;

import com.google.inject.Guice;
import com.google.inject.Injector;
import com.sun.jersey.guice.spi.container.servlet.GuiceContainer;

public class RestTextPublisher extends RestPublisher<String> {

	public RestTextPublisher(CommunicationConfigFactory serverConfig, MessageHandlingFactory<String> messageHandling) {
		super(serverConfig, messageHandling);
	}

	@Override
	protected Servlet createServlet() {
		Injector injector = Guice.createInjector(
				new TextTopicServleteModule(messageHandling.createSerializer(), messageHandling.createRecognition())
				, new TopicStorageModule(getOwnerId())
				);
		return new GuiceContainer(injector);
	}

	@Override
	protected String getOwnerId() {
		return "RestTextTopic";
	}

}
