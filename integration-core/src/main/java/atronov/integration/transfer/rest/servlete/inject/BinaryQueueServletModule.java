package atronov.integration.transfer.rest.servlete.inject;

import atronov.integration.message.Serializer;
import atronov.integration.message.TypeRecognition;
import atronov.integration.transfer.rest.handling.MessageReceptionHandler;
import atronov.integration.transfer.rest.handling.SerializeQueueMessageHandler;
import atronov.integration.transfer.rest.servlete.QueueBinaryReceiverServlet;

import com.google.inject.TypeLiteral;
import com.google.inject.servlet.ServletModule;
import com.sun.jersey.guice.spi.container.servlet.GuiceContainer;

public class BinaryQueueServletModule extends ServletModule {

	private final Serializer<byte[]> serializer;
	private final TypeRecognition<byte[]> recognition;
	
	public BinaryQueueServletModule(Serializer<byte[]> serializer, TypeRecognition<byte[]> recognition) {
		this.serializer = serializer;
		this.recognition = recognition;
	}
	
	@Override
	protected void configureServlets() {
		bind(new TypeLiteral<Serializer<byte[]>>(){}).toInstance(serializer);
		bind(new TypeLiteral<TypeRecognition<byte[]>>(){}).toInstance(recognition);
		bind(new TypeLiteral<MessageReceptionHandler<byte[]>>(){}). to(new TypeLiteral<SerializeQueueMessageHandler<byte[]>>(){});
		bind(QueueBinaryReceiverServlet.class);
		serve("*").with(GuiceContainer.class);
	}
}
