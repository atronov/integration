package atronov.integration.transfer.rest;

import javax.ws.rs.core.MediaType;

import atronov.integration.message.MessageHandlingFactory;
import atronov.integration.transfer.Sender;
import atronov.integration.transfer.server.CommunicationConfigFactory;

public class RestTextSender extends RestSender<String> implements Sender {

	public RestTextSender(CommunicationConfigFactory communication, MessageHandlingFactory<String> handlingFactory) {
		super(communication, handlingFactory);
	}

	@Override
	protected String messageMediaType() {
		return MediaType.TEXT_PLAIN;
	}

}
