package atronov.integration.transfer.rest;

import java.util.Date;

import atronov.integration.message.IMessage;
import atronov.integration.message.MessageHandlingFactory;
import atronov.integration.message.Serializer;
import atronov.integration.message.TypeRecognition;
import atronov.integration.transfer.Receiver;
import atronov.integration.transfer.server.CommunicationConfigFactory;

import com.sun.jersey.api.client.Client;
import com.sun.jersey.api.client.ClientResponse;
import com.sun.jersey.api.client.WebResource;

public abstract class RestSubscriber<TMessageData> implements Receiver {

	private CommunicationConfigFactory comminication;
	
	private Serializer<TMessageData> serializer;
	private TypeRecognition<TMessageData> recognition;
	
	private final int subscriberId = (int)new Date().getTime();
	
	public RestSubscriber(CommunicationConfigFactory communication, MessageHandlingFactory<TMessageData> handlingFactory) {
		this.serializer = handlingFactory.createSerializer();
		this.recognition = handlingFactory.createRecognition();
		this.comminication = communication;
		subcribe();
	}
	
	private void subcribe() {
		Client client = comminication.createClient();
		WebResource webResource = client.resource(comminication.getResourceUri());
		ClientResponse response = webResource
			.path("/subscribe")
			.queryParam("id", Integer.toString(subscriberId))
			.get(ClientResponse.class);
		if (response.getStatus() != 200) {
			System.err.print("Failed : HTTP error code : " + response.getStatus());
			throw new RuntimeException("Subscibtion is failled");
		}
	}
	
	protected abstract TMessageData extractMessageData(ClientResponse response);
	
	@Override
	public <TMessage extends IMessage> IMessage getReceived() {
		Client client = comminication.createClient();
		WebResource webResource = client.resource(comminication.getResourceUri());
		ClientResponse response = webResource
				.queryParam("id", Integer.toString(subscriberId))
				.get(ClientResponse.class);
		
		if (response.getStatus() != 200) {
			System.err.print("Failed : HTTP error code : " + response.getStatus());
			return null;
		}
		
		TMessageData messageData = extractMessageData(response);
		Class<? extends IMessage> messsageType = recognition.recognize(messageData);
		IMessage message = serializer.deserealize(messageData, messsageType);
		return message;
	}

}
