package atronov.integration.transfer.rest;

import java.util.NoSuchElementException;

import javax.servlet.Servlet;

import org.glassfish.grizzly.servlet.ServletRegistration;
import org.glassfish.grizzly.servlet.WebappContext;

import atronov.integration.message.IMessage;
import atronov.integration.message.MessageHandlingFactory;
import atronov.integration.transfer.ClosableReceiver;
import atronov.integration.transfer.resource.MessageQueue;
import atronov.integration.transfer.server.CommunicationConfigFactory;
import atronov.integration.transfer.server.Server;

public abstract class RestReceiver<TMessageData> extends Server implements ClosableReceiver {

	private MessageQueue messages;
	
	protected final MessageHandlingFactory<TMessageData> messageHandling;
	
	public RestReceiver(CommunicationConfigFactory serverConfig, MessageHandlingFactory<TMessageData> messageHandling) {
		super(serverConfig);
		this.messageHandling = messageHandling;
		messages = new MessageQueue(getOwnerId());
		startServer();
	}
	
	protected abstract Servlet createServlet();
	
	protected abstract String getOwnerId();
	
	@Override
	protected ServletRegistration addServlet(WebappContext context) {
		return context.addServlet("GuiceServlet", createServlet());
	}
	
	@Override
	public <TMessage extends IMessage> IMessage getReceived() {
		try {
			@SuppressWarnings("unchecked")
			TMessage message = (TMessage) messages.remove();
			return message;
		} catch (NoSuchElementException e) {
			return null;
		}
	}

}
