package atronov.integration.transfer.rest.handling;

public interface MessageReceptionHandler<TMessageData> {

	boolean handle(TMessageData messageData);
	
}
