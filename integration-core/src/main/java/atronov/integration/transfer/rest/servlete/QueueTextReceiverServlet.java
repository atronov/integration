package atronov.integration.transfer.rest.servlete;

import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import atronov.integration.transfer.rest.handling.MessageReceptionHandler;

import com.google.inject.Inject;

@Path("/rest")
public class QueueTextReceiverServlet {
	
	@Inject private MessageReceptionHandler<String> handler;
	
	@POST
	@Consumes(MediaType.TEXT_PLAIN)
	public Response post(String message) {
		System.out.println("Received: " + message);
		if (handler.handle(message)) {
			return Response.ok().build();
		} else {
			return Response.status(400).build();
		}
	}
	
	@GET
	public String get() {
		return "Hi! This is Text receiver.";
	}
	
}
