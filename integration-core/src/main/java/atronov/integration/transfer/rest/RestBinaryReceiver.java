package atronov.integration.transfer.rest;

import javax.servlet.Servlet;

import atronov.integration.message.MessageHandlingFactory;
import atronov.integration.transfer.rest.servlete.inject.BinaryQueueServletModule;
import atronov.integration.transfer.rest.servlete.inject.QueueStorageModule;
import atronov.integration.transfer.server.CommunicationConfigFactory;

import com.google.inject.Guice;
import com.google.inject.Injector;
import com.sun.jersey.guice.spi.container.servlet.GuiceContainer;

public class RestBinaryReceiver extends RestReceiver<byte[]> {
	
	public RestBinaryReceiver(CommunicationConfigFactory serverConfig, MessageHandlingFactory<byte[]> messageHandling) {
		super(serverConfig, messageHandling);
	}

	@Override
	protected Servlet createServlet() {
		Injector injector = Guice.createInjector(
				new BinaryQueueServletModule(messageHandling.createSerializer(), messageHandling.createRecognition())
				, new QueueStorageModule(getOwnerId())
				);
		return new GuiceContainer(injector);
	}

	@Override
	protected String getOwnerId() {
		return "RestBinaryQueue";
	}
}
