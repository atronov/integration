package atronov.integration.transfer.rest;

import javax.servlet.Servlet;

import atronov.integration.message.MessageHandlingFactory;
import atronov.integration.transfer.rest.servlete.inject.BinaryTopicServleteModule;
import atronov.integration.transfer.rest.servlete.inject.TopicStorageModule;
import atronov.integration.transfer.server.CommunicationConfigFactory;

import com.google.inject.Guice;
import com.google.inject.Injector;
import com.sun.jersey.guice.spi.container.servlet.GuiceContainer;

public class RestBinaryPublisher extends RestPublisher<byte[]> {

	public RestBinaryPublisher(CommunicationConfigFactory serverConfig, MessageHandlingFactory<byte[]> messageHandling) {
		super(serverConfig, messageHandling);
	}

	@Override
	protected Servlet createServlet() {
		Injector injector = Guice.createInjector(
				new BinaryTopicServleteModule(messageHandling.createSerializer(), messageHandling.createRecognition())
				, new TopicStorageModule(getOwnerId())
				);
		return new GuiceContainer(injector);
	}

	@Override
	protected String getOwnerId() {
		return "RestBinaryTopic";
	}

}
