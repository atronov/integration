package atronov.integration.transfer.rest.servlete.inject;

import atronov.integration.transfer.resource.MessageTopic;
import atronov.integration.transfer.resource.TopicService;

import com.google.inject.name.Names;
import com.google.inject.servlet.ServletModule;
import com.sun.jersey.guice.spi.container.servlet.GuiceContainer;

public class TopicStorageModule extends ServletModule {

	private final String ownerId;
	
	public TopicStorageModule(String ownerId) {
		this.ownerId = ownerId;
	}
	
	@Override
	protected void configureServlets() {
		bind(String.class).annotatedWith(Names.named("OwnerId")).toInstance(ownerId);
		bind(TopicService.class).to(MessageTopic.class);
		serve("*").with(GuiceContainer.class);
	}
}
