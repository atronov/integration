package atronov.integration.transfer.rest.servlete.inject;

import atronov.integration.message.Serializer;
import atronov.integration.message.TypeRecognition;
import atronov.integration.transfer.rest.handling.MessageSubscribtionHandler;
import atronov.integration.transfer.rest.handling.SerializeTopicMessageHandler;
import atronov.integration.transfer.rest.servlete.TopicTextPublisherServlet;

import com.google.inject.TypeLiteral;
import com.google.inject.servlet.ServletModule;
import com.sun.jersey.guice.spi.container.servlet.GuiceContainer;

public class TextTopicServleteModule extends ServletModule  {

	private final Serializer<String> serializer;
	private final TypeRecognition<String> recognition;
	
	public TextTopicServleteModule(Serializer<String> serializer, TypeRecognition<String> recognition) {
		this.serializer = serializer;
		this.recognition = recognition;
	}
	
	@Override
	protected void configureServlets() {
		bind(new TypeLiteral<Serializer<String>>(){}).toInstance(serializer);
		bind(new TypeLiteral<TypeRecognition<String>>(){}).toInstance(recognition);
		bind(new TypeLiteral<MessageSubscribtionHandler<String>>(){}).to(new TypeLiteral<SerializeTopicMessageHandler<String>>(){});
		bind(TopicTextPublisherServlet.class);
		serve("*").with(GuiceContainer.class);
	}
	
}
