package atronov.integration.transfer.rest.handling;

import atronov.integration.message.IMessage;
import atronov.integration.message.Serializer;
import atronov.integration.transfer.resource.TopicService;

import com.google.inject.Inject;

public class SerializeTopicMessageHandler<TMessageData> implements MessageSubscribtionHandler<TMessageData> {

	@Inject
	private TopicService storage;
	@Inject
	private Serializer<TMessageData> serializer;
	
	@Override
	public void subscribe(String subscriberId) {
		storage.subscribe(subscriberId);
	}
	
	@Override
	public TMessageData receive(String subscriberId) {
		IMessage message = storage.receive(subscriberId);
		if (message == null) return null;
		TMessageData messageData = serializer.serealize(message);
		return messageData;
	}

}
