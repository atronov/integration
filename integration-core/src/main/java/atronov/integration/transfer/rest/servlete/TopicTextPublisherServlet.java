package atronov.integration.transfer.rest.servlete;

import javax.ws.rs.DefaultValue;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.Response;

import atronov.integration.transfer.rest.handling.MessageSubscribtionHandler;

import com.google.common.base.Strings;
import com.google.inject.Inject;

@Path("/rest")
public class TopicTextPublisherServlet {

	@Inject
	private MessageSubscribtionHandler<String> handler;

	@GET
	@Path("/subscribe")
	public Response subscribe(@DefaultValue("default") @QueryParam("id") String subscriberId) {
		try {
			handler.subscribe(subscriberId);
			return Response.ok("Subscription OK").build();
		} catch (IllegalStateException e) {
			System.err.print(e);
			return Response.status(406).build();
		}
	}

	@GET
	public String receive(@DefaultValue("default") @QueryParam("id") String subscriberId) {
		String messageData = handler.receive(subscriberId);
		return Strings.nullToEmpty(messageData);
	}
}
