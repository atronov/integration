package atronov.integration.transfer.rest;

import javax.servlet.Servlet;

import org.glassfish.grizzly.servlet.ServletRegistration;
import org.glassfish.grizzly.servlet.WebappContext;

import atronov.integration.message.IMessage;
import atronov.integration.message.MessageHandlingFactory;
import atronov.integration.transfer.ClosableSender;
import atronov.integration.transfer.resource.MessageTopic;
import atronov.integration.transfer.server.CommunicationConfigFactory;
import atronov.integration.transfer.server.Server;

public abstract class RestPublisher<TMessageData> extends Server implements ClosableSender {

	private MessageTopic messages;
	
	protected final MessageHandlingFactory<TMessageData> messageHandling;
	
	public RestPublisher(CommunicationConfigFactory serverConfig, MessageHandlingFactory<TMessageData> messageHandling) {
		super(serverConfig);
		this.messageHandling = messageHandling;
		messages = new MessageTopic(getOwnerId());
		startServer();
	}

	protected abstract Servlet createServlet();
	
	protected abstract String getOwnerId();
	
	@Override
	protected ServletRegistration addServlet(WebappContext context) {
		return context.addServlet("GuiceServlet", createServlet());
	}
	
	@Override
	public <TMessage extends IMessage> boolean send(IMessage message) {
		messages.publish(message);
		return true;
	}
}
