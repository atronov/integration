package atronov.integration.run.remoting;

import java.io.Serializable;

import com.google.common.base.Objects;

import atronov.integration.config.NodeConfig;

public class NodeInfo implements Serializable {
	private static final long serialVersionUID = -398162706589370175L;
	
	private final String host;
	private final String sshUser;
	private final String sshPassword;
	
	public NodeInfo(String host, String sshUser, String sshPassword) {
		this.host = host;
		this.sshUser = sshUser;
		this.sshPassword = sshPassword;
	}
	
	public NodeInfo(String host) {
		this.host = host;
		this.sshUser = NodeConfig.get().sshUser();
		this.sshPassword = NodeConfig.get().sshPassword();
	}
	
	public NodeInfo() {
		NodeConfig config = NodeConfig.get();
		this.host = config.host();
		this.sshUser = config.sshUser();
		this.sshPassword = config.sshPassword();
	}

	public String getHost() {
		return host;
	}

	public String getSshUser() {
		return sshUser;
	}

	public String getSshPassword() {
		return sshPassword;
	}
	
	@Override
	public boolean equals(Object obj) {
		if (obj == null)
			return false;
		if (obj instanceof NodeInfo)
			return this.host.equals(((NodeInfo)obj).host);
		return false;
	}
	
	@Override
	public String toString() {
		return Objects.toStringHelper("Node")
		.add("Host", host)
		.add("User", sshUser)
		.add("Password", sshPassword)
		.toString();
	}
}
