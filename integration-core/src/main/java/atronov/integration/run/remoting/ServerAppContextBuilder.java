package atronov.integration.run.remoting;

import org.springframework.beans.factory.support.BeanDefinitionBuilder;
import org.springframework.context.support.AbstractApplicationContext;
import org.springframework.context.support.GenericApplicationContext;
import org.springframework.remoting.rmi.RmiServiceExporter;

public class ServerAppContextBuilder {

	private static final int DEFAULT_PORT = 8899;
	
	private ServerAppContextBuilder() {}
	
	public static AbstractApplicationContext build(String host, int port) {
		if (host != null)
			System.setProperty("java.rmi.server.hostname", host);
		BeanDefinitionBuilder serverServiceBuilder = BeanDefinitionBuilder
				.rootBeanDefinition(RmiServiceExporter.class)
				.addPropertyValue("serviceName", "Control")
				.addPropertyValue("service", new ResourceControl())
				.addPropertyValue("serviceInterface", Control.class)
				.addPropertyValue("registryPort", port);
		BeanDefinitionBuilder serverServiceDebugBuilder = BeanDefinitionBuilder
				.rootBeanDefinition(RmiServiceExporter.class)
				.addPropertyValue("serviceName", "ControlDebug")
				.addPropertyValue("service", new ResourceControllDebug())
				.addPropertyValue("serviceInterface", ControlDebug.class)
				.addPropertyValue("registryPort", port);
		serverServiceBuilder.getBeanDefinition();
		GenericApplicationContext context = new GenericApplicationContext();
		context.registerBeanDefinition("control", serverServiceBuilder.getBeanDefinition());
		//context.registerBeanDefinition("controlDebug", serverServiceDebugBuilder.getBeanDefinition());
		context.refresh();
		return context;
	}
	
	public static AbstractApplicationContext build() {
		return build(null, DEFAULT_PORT);
	}
	
	public static AbstractApplicationContext build(String host) {
		return build(host, DEFAULT_PORT);
	}
}
