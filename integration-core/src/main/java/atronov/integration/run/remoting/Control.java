package atronov.integration.run.remoting;

import java.util.List;

import atronov.integration.message.IMessage;
import atronov.integration.monitor.PacketFilter;
import atronov.integration.monitor.PacketInfo;
import atronov.integration.run.build.BuildConfig;

public interface Control {

	void setServerHost(String hostName);
	
	void setJmsHost(String hostName);

	void setRmiHost(String hostName);

	void setReceiver();

	void setSender();
	
	int getReceivedCount();
	
	int getSentCount();

	void setBuildConfig(BuildConfig config);

	void setMessageForSent(IMessage message);

	void setIterationTime(long iterration);

	void setMaxIterationCount(int iterationCount);

	void start();

	void stop();
	
	String getErrors();

	void setTrafficMonitor(boolean set);
	
	void setTrafficFilter(PacketFilter filter);
	
	void resetTrafic();

	List<PacketInfo> getTraficStat();

}