package atronov.integration.run.remoting;

public interface ControlDebug extends Control {

	int getRequiredMessages();

	void setRequiredMessages(int requiredMessages);
}
