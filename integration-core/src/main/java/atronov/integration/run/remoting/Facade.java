package atronov.integration.run.remoting;

import java.util.Collection;
import java.util.Map;

import atronov.integration.message.IMessage;
import atronov.integration.run.build.BuildConfig;

public interface Facade {
	
	// config before run
	void setNodes(Collection<NodeInfo> nodesInfo);

	void setJmsHost(NodeInfo jmsNode);
	
	void setResourceConfig(BuildConfig config);
	
	void setSubscribers(int count);
	
	// run commands
	void start();

	void stop();
	
	// run parameters
	void setIterationTime(long iterration);

	void setMaxIterationCount(int iterationCount);
	
	void setMaxSessionTime(long time);
	
	// trafic monitoring
	long getTotalBytesTransferred();
	
	Map<String, Long> getBytesTransferred();
	
	void resetTrafic();

	// transferred content
	void setMessageToSend(IMessage message);
	
	// errors
	Map<String, String> getErrors();
	
	// modeling result
	ModelRunResult getResult();
}
