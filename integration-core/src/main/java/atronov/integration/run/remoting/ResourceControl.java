package atronov.integration.run.remoting;

import static com.google.common.base.Preconditions.checkArgument;
import static com.google.common.base.Preconditions.checkNotNull;
import static com.google.common.base.Preconditions.checkState;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.IOException;
import java.io.PrintStream;
import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;

import org.apache.commons.io.FileUtils;
import org.apache.commons.io.output.TeeOutputStream;
import org.apache.log4j.Logger;
import org.jboss.util.NotImplementedException;
import org.jboss.util.xml.catalog.helpers.Debug;

import atronov.integration.config.WrongModelConfigException;
import atronov.integration.message.IMessage;
import atronov.integration.monitor.NetPcapMonitor;
import atronov.integration.monitor.PacketFilter;
import atronov.integration.monitor.PacketInfo;
import atronov.integration.monitor.TrafficMonitor;
import atronov.integration.run.build.BuildConfig;
import atronov.integration.run.build.ResourceBuilder;
import atronov.integration.transfer.Receiver;
import atronov.integration.transfer.Sender;
import atronov.integration.utility.Wait;
import atronov.integration.utility.Wait.Condition;

public class ResourceControl implements Control {

	private static final long STOP_TIMEOUT = 60_000;
	private static final long START_TIMEOUT = 30_000;
	
	private final Logger log = Logger.getLogger(this.getClass());
	
	// configs
	private volatile boolean started = false;
	private long iterationTime = 1_000; // 1 second
	private int maxIteration = 100;
	private BuildConfig config = null;
	private boolean isSender = true;
	private IMessage messageForSend = null;
	
	// working thread
	private Thread workingThread;
	private volatile boolean stopFlag = false;
	private volatile boolean resourceCreated = false;
	private volatile int receivedCount = 0;
	private volatile int sentCount = 0;
	
	//monitoring
	private TrafficMonitor traficMonitor = null;
	private final List<PacketInfo> traficStat = new LinkedList<>();
	
	@Override
	public void setServerHost(String hostName) {
		checkState(!started,"Cannot be changed while running");
		log.info("Set server host as "+hostName);
		System.setProperty("http.server.host", hostName);
	}
	
	@Override
	public void setJmsHost(String hostName) {
		checkState(!started,"Cannot be changed while running");
		log.info("Set jms host as "+hostName);
		System.setProperty("jms.host", hostName);
	}
	
	@Override
	public void setRmiHost(String hostName) {
		checkState(!started,"Cannot be changed while running");
		log.info("Set rmi host as: " + hostName);
		System.setProperty("rmi.host", hostName);
	}
	
	@Override
	public void setBuildConfig(BuildConfig config) {
		checkState(!started,"Cannot be changed while running");
		log.info("Set resource build config as "+config);
		if (!ResourceBuilder.canBuild(config))
			throw new WrongModelConfigException("Imposible build configuration: " + config);
		this.config = config;
	}
	
	@Override
	public void setReceiver() {
		checkState(!started,"Cannot be changed while running");
		log.info("This control will work as receiver");
		isSender = false;
		
	}
	@Override
	public void setSender() {
		checkState(!started,"Cannot be changed while running");
		log.info("This control will work as sender");
		isSender = true;
	}
	
	@Override
	public void setMessageForSent(IMessage message) {
		checkNotNull(message, "Message cannot be null");
		log.info("Set message as "+message);
		messageForSend = message;
	}

	@Override
	public void setIterationTime(long iterration) {
		checkArgument(iterration >= 0 && iterration <= 10_000, "Iterration time must be between 0 and 10 000");
		log.info("Set iteration time as " + iterration);
		iterationTime = iterration;
	}
	
	@Override
	public void setMaxIterationCount(int iterationCount) {
		checkArgument(iterationCount >= 0 && iterationCount <= 1_000_000, "Max iteration must be between 0 and 1 000 000");
		log.info("Set max iteration count as "+iterationCount);
		maxIteration = iterationCount;
	}
	
	@Override
	public void start() {
		checkState(!started,"Already started");
		checkNotNull(config,"Resource build config wasn't set yet");
		log.info("Start control shedule");
		resourceCreated = false;
		stopFlag = false;
		startMonitorring();
		started = true;
		Runnable worker;
		if (isSender) {
			checkNotNull(messageForSend, "Message cannot be null");
			worker = new SenderWorker();
		} else {
			worker = new ReceiverWorker();
		}
		worker = new CallbacWrapper(worker);
		workingThread = new Thread(worker);
		workingThread.start();
		Thread.yield();
		log.info("Wait while resource created");
		boolean created = Wait.until(new Condition() {
			public boolean check() { return resourceCreated; }
		}).forPeriod(START_TIMEOUT).go();
		if (created)
			log.info("Resource was created successfully");
		else
			log.info("Resource wasn't created yet. Continue anyway.");
	}
	
	private void startMonitorring() {
		if (traficMonitor != null)
			try {
				traficMonitor.start();
			} catch (Exception e) {
				log.error("Error in trafic monitor", e);
			}
	}
	
	private void stopMonitorring() {
		log.info("Stop trafic monitoring");
		if (traficMonitor != null)
			try {
				List<PacketInfo> stat = traficMonitor.stop();
				this.traficStat.addAll(stat);
			} catch (Exception e) {
				log.error("Error in trafic monitor", e);
			}
		else
			log.info("Trafic monitoring wasn't set");
			
	}
	
	@Override
	public void stop() {
		log.info("Stop control shedule");
		if (!started) {
			log.info("Already stopped. No action needed.");
		} else {
			log.info("Stop worker.");
			stopFlag = true;
			log.info("Wait while worker thread stopped");
			boolean stopped = Wait.until(new Condition() {
				public boolean check() {
					return !started || !workingThread.isAlive();
				}
			}).forPeriod(STOP_TIMEOUT).go();
			if (stopped)
				log.info("Worker stopped successfully");
			else
				log.info("Worker didn't stop yet. Continue anyway.");
		}
		stopFlag = false;
		started = false;
		stopMonitorring();
	}

	@Override
	public String getErrors() {
		log.info("Read errors from log file");
		File errorsLog = new File("errors.log");
		if (errorsLog.exists()) {
			String errors = null;
			try {
				errors = FileUtils.readFileToString(errorsLog);
			} catch (IOException e) {
				log.error("Problem with reading errors", e);
			}
			return errors;
		} else {
			return null;
		}
	}
	
	@Override
	public void resetTrafic() {
		log.info("Clear trafic stat");
		traficStat.clear();
	}

	@Override
	public void setTrafficMonitor(boolean set) {
		if (set) {
			log.info("Set trafic monitor");
			if (traficMonitor == null) {
				traficMonitor = new NetPcapMonitor(getNetworkDeviceName());
			} else {
				log.info("Trafic monitor already set");
			}
		} else {
			log.info("Unset trafic monitor");
			traficMonitor = null;
		}
	}

	@Override
	public void setTrafficFilter(PacketFilter filter) {
		checkNotNull(traficMonitor, "Impossible to set trafic filter, when monitor isn't set.");
		traficMonitor.setFilter(filter);
	}
	
	private String getNetworkDeviceName() {
		if (System.getProperty("jnetpcap.device") != null)
			return System.getProperty("jnetpcap.device");
		else
			return "eth0";
	}
	
	@Override
	public List<PacketInfo> getTraficStat() {
		return new ArrayList<>(traficStat);
	}
	
	private class ReceiverWorker implements Runnable {
		@Override
		public void run() {
			log.info("Start receiver worker in control.");
			Receiver receiver = ResourceBuilder.fromConfig(config).buildReceiver();
			resourceCreated = true;
			long stepStart;
			long currentTime;
			long stepDelay = iterationTime;
			long stepFine = 0;
			try (AutoCloseable res = (receiver instanceof AutoCloseable) ? (AutoCloseable) receiver : null) {
				for (int i = 0; !stopFlag && i < maxIteration; i++) {
					Thread.sleep(Math.max(0, stepDelay));
					stepStart = System.currentTimeMillis();
					IMessage message = receiver.getReceived();
					log.info("Received: "+message);
					if (message != null)
						receivedCount++;
					currentTime = System.currentTimeMillis();
					stepDelay = iterationTime - currentTime + stepStart - stepFine;
					stepFine = Math.abs(Math.min(0, stepDelay));
					log.debug("Step start="+stepStart);
					log.debug("Current time="+currentTime);
					log.debug("Step delay="+stepDelay);
					log.debug("Step fine="+stepFine);
				}
			} catch (Exception e) {
				throw new RuntimeException(e);
			}
		}
	}
	
	private class SenderWorker implements Runnable {
		@Override
		public void run() {
			log.info("Start sender worker in control.");
			Sender sender = ResourceBuilder.fromConfig(config).buildSender();
			resourceCreated = true;
			long stepStart;
			long currentTime;
			long stepDelay;
			long stepFine = 0;
			try (AutoCloseable res = (sender instanceof AutoCloseable) ? (AutoCloseable) sender : null) {
				for (int i = 0; !stopFlag && i < maxIteration; i++) {
					stepStart = System.currentTimeMillis();
					sender.send(messageForSend);
					log.info("Sent: "+messageForSend);
					currentTime = System.currentTimeMillis();
					stepDelay = iterationTime - currentTime + stepStart - stepFine;
					Thread.sleep(Math.max(0, stepDelay));
					stepFine = Math.abs(Math.min(0, stepDelay));
					sentCount++;
					log.debug("Step start="+stepStart);
					log.debug("Current time="+currentTime);
					log.debug("Step delay="+stepDelay);
					log.debug("Step fine="+stepFine);
				}
			} catch (Exception e) {
				throw new RuntimeException(e);
			}
		}		
	}
	
	private class CallbacWrapper implements Runnable {
		Runnable wrapped;
		public CallbacWrapper(Runnable wrapped) {
			this.wrapped = wrapped;
		}
		@Override
		public void run() {
			try {
				wrapped.run();
			} finally {
				started = false;
			}
		}
	}

	@Override
	public int getReceivedCount() {
		return receivedCount;
	}

	@Override
	public int getSentCount() {
		// TODO Auto-generated method stub
		return sentCount;
	}
}
