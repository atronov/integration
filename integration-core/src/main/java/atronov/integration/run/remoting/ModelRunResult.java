package atronov.integration.run.remoting;

import java.util.Date;
import java.util.List;
import java.util.Map;

import atronov.integration.message.IMessage;
import atronov.integration.monitor.PacketInfo;
import atronov.integration.run.build.BuildConfig;

public class ModelRunResult {
	
	private Map<String, List<PacketInfo>> traffic;
	
	private long bytes;
	
	private Map<String, String> errors;
	
	private long startTime;
	
	private long endTime;
	
	private IMessage message;
	
	private Map<String, Integer> messagesSent;
	
	private Map<String, Integer> messagesReceived;
	
	private int packets;
	
	private BuildConfig config;

	public Map<String, List<PacketInfo>> getTraffic() {
		return traffic;
	}

	public void setTraffic(Map<String, List<PacketInfo>> traffic) {
		this.traffic = traffic;
	}

	public Map<String, String> getErrors() {
		return errors;
	}

	public void setErrors(Map<String, String> errors) {
		this.errors = errors;
	}

	public long getStartTime() {
		return startTime;
	}

	public void setStartTime(long startTime) {
		this.startTime = startTime;
	}

	public long getEndTime() {
		return endTime;
	}

	public void setEndTime(long endTime) {
		this.endTime = endTime;
	}

	public IMessage getMessage() {
		return message;
	}

	public void setMessage(IMessage message) {
		this.message = message;
	}

	public Map<String, Integer> getMessagesSent() {
		return messagesSent;
	}

	public void setMessagesSent(Map<String, Integer> messagesSent) {
		this.messagesSent = messagesSent;
	}

	public Map<String, Integer> getMessageReceived() {
		return messagesReceived;
	}

	public void setMessageReceived(Map<String, Integer> messageReceived) {
		this.messagesReceived = messageReceived;
	}

	public BuildConfig getConfig() {
		return config;
	}

	public void setConfig(BuildConfig config) {
		this.config = config;
	}

	public long getBytes() {
		return bytes;
	}

	public void setBytes(long bytes) {
		this.bytes = bytes;
	}

	public int getPackets() {
		return packets;
	}

	public void setPackets(int packets) {
		this.packets = packets;
	}

}
