package atronov.integration.run.remoting;

import org.springframework.beans.factory.support.BeanDefinitionBuilder;
import org.springframework.context.support.AbstractApplicationContext;
import org.springframework.context.support.GenericApplicationContext;
import org.springframework.remoting.rmi.RmiProxyFactoryBean;

public class ClientAppContextBuilder {

private static final int DEFAULT_PORT = 8899;
private static final String DEFAULT_HOST = "localhost";
	
	private ClientAppContextBuilder() {}
	
	public static AbstractApplicationContext build(String host, int port) {
		BeanDefinitionBuilder clientServiceBuilder = BeanDefinitionBuilder
				.rootBeanDefinition(RmiProxyFactoryBean.class)
				.addPropertyValue("serviceUrl", String.format("rmi://%s:%s/Control", host, port))
				.addPropertyValue("serviceInterface", Control.class);
		BeanDefinitionBuilder clientServiceBuilderDebug = BeanDefinitionBuilder
				.rootBeanDefinition(RmiProxyFactoryBean.class)
				.addPropertyValue("serviceUrl", String.format("rmi://%s:%s/ControlDebug", host, port))
				.addPropertyValue("serviceInterface", ControlDebug.class);
		GenericApplicationContext context = new GenericApplicationContext();
		context.registerBeanDefinition("remoteControl", clientServiceBuilder.getBeanDefinition());
		//context.registerBeanDefinition("remoteControlDebug", clientServiceBuilderDebug.getBeanDefinition());
		context.refresh();
		return context;
	}
	
	public static AbstractApplicationContext build(String host) {
		return build(host, DEFAULT_PORT);
	}
	
	public static AbstractApplicationContext build() {
		return build(DEFAULT_HOST, DEFAULT_PORT);
	}
}
