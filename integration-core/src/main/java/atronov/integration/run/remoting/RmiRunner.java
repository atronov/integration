package atronov.integration.run.remoting;

import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.io.StringWriter;
import java.util.Properties;

import org.apache.commons.io.IOUtils;
import org.apache.log4j.Logger;

import com.jcraft.jsch.ChannelShell;
import com.jcraft.jsch.JSch;
import com.jcraft.jsch.JSchException;
import com.jcraft.jsch.Session;

public class RmiRunner {

	private final Logger log = Logger.getLogger(this.getClass());
	
	private final NodeInfo node;
	private String out;
	private ChannelShell channel;
	
	private boolean started = false;
	
	public RmiRunner(NodeInfo node) {
		this.node = node;
	}
	
	public void run() {
		log.info("Run rmi on " + node);
		//if (true) return;
		try {
			started = true;
			Properties config = new java.util.Properties();
			config.put("StrictHostKeyChecking", "no");
			String command = String.format("sudo java -Xms256m -Xmx256m -XX:MaxPermSize=128m -Djava.security.policy=policy.all -jar ./app/integration.jar %s", node.getHost());
			JSch jsch = new JSch();
			Session session = jsch.getSession(node.getSshUser(), node.getHost(), 22);
			session.setConfig(config);
			session.setPassword(node.getSshPassword());
			session.connect();
			channel = (ChannelShell) session.openChannel("shell");
			channel.setPty(true);
			channel.setInputStream(null);
			InputStream in = channel.getInputStream();
			channel.connect();
			OutputStream out = channel.getOutputStream();
			try {Thread.sleep(1000);} catch (InterruptedException e) {}
			out.write((command + "\n").getBytes());
			out.flush();
			try {Thread.sleep(2000);} catch (InterruptedException e) {}
			out.write((node.getSshPassword() + "\n").getBytes());
			out.flush();
		} catch (JSchException | IOException e) {
			throw new RuntimeException(e);
		}
	}
	
	public void stop() {
		try {
			log.info("Stop rmi on " + node);
			//if (true) return;
			OutputStream out = channel.getOutputStream();
			out.write(("q" + "\n").getBytes());
			out.flush();
			try {Thread.sleep(1000);} catch (InterruptedException e) {}
			InputStream in = channel.getInputStream();
			Session session = channel.getSession();
			channel.disconnect();
			session.disconnect();
			StringWriter writer = new StringWriter();
	    IOUtils.copy(in, writer, "utf-8");
	    started = false;
	    this.out = writer.toString();
		} catch (JSchException | IOException e) {
			throw new RuntimeException(e);
		}
	}

	public String getOut() {
		return out;
	}
}
