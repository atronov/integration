package atronov.integration.run.remoting;

import java.io.BufferedReader;
import java.io.File;
import java.io.IOException;
import java.io.InputStreamReader;
import java.rmi.RMISecurityManager;

import org.apache.commons.io.FileUtils;
import org.springframework.context.support.AbstractApplicationContext;

public class ColtrolRunner {

	public static void main(String[] args) throws IOException {
		if (System.getSecurityManager() == null) {
			System.setSecurityManager(new RMISecurityManager());
		}
		String host = null;
		if (args.length > 0)
			host = args[0];
		System.out.println("Rmi context start.");
		try (AbstractApplicationContext serverContext = ServerAppContextBuilder.build(host)) {
			BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
			String readLine;
			{
				readLine = br.readLine();
			} while(!"q".equalsIgnoreCase(readLine.trim()));
		}
		System.out.println("Rmi context end");
	}
}
