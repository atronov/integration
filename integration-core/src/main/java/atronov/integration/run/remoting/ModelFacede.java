package atronov.integration.run.remoting;

import java.rmi.RMISecurityManager;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.HashSet;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;

import org.apache.log4j.Logger;
import org.springframework.context.support.AbstractApplicationContext;

import atronov.integration.config.JmsConfing;
import atronov.integration.config.NodeConfig;
import atronov.integration.config.WrongModelConfigException;
import atronov.integration.message.IMessage;
import atronov.integration.monitor.FilterBuilder;
import atronov.integration.monitor.PacketFilter;
import atronov.integration.monitor.PacketInfo;
import atronov.integration.run.build.BuildConfig;
import atronov.integration.run.build.ResourceBuilder;
import atronov.integration.run.build.Technology;
import atronov.integration.run.build.Topology;

import com.google.common.base.Predicate;
import com.google.common.base.Strings;
import com.google.common.collect.ImmutableList;
import com.google.common.collect.Iterables;

public class ModelFacede implements Facade {
	
	private final Logger log = Logger.getLogger(this.getClass());
	
	private List<NodeInfo> nodes;
	private List<NodeInfo> receivers;
	private List<NodeInfo> senders;
	private NodeInfo jmsNode;
	private BuildConfig resourceConfig;
	private int subscribers = 1;
	
	private Map<String, RmiRunner> rmiMapping = new HashMap<>();
	private Map<String, Control> controlMapping = new HashMap<>();
	private Map<String, List<PacketInfo>> trafficStatMapping = new HashMap<>();
	private Map<String, String> errMapping = new HashMap<>();
	private Map<String, Integer> messagesSent = new HashMap<String, Integer>();
	private Map<String, Integer> messagesReceived = new HashMap<String, Integer>();
	
	private int maxIterations = 10_000;
	private long iterationTime = 500;
	// TODO implement parallel time controller
	private long maxSessionTime = 1000 * 60 * 5; // 5 min
	private long startTime = -1;
	private long stopTime = -1;
	
	private IMessage messageToSend = null;
	
	public ModelFacede() {
		JmsConfing jms = JmsConfing.get();
		jmsNode = new NodeInfo(jms.jmsHost(), jms.jmsHostUser(), jms.jmsHostPassword());
		NodeConfig nodeConfig = NodeConfig.get();
		List<NodeInfo> nodesInfo = new ArrayList<>();
		for (String nodeHost : nodeConfig.nodes()) {
			NodeInfo node = new NodeInfo(nodeHost);
			nodesInfo.add(node);
		}
		this.setNodes(nodesInfo);
	}
	
	@Override
	public void setNodes(Collection<NodeInfo> nodesInfo) {
		nodes = ImmutableList.copyOf(nodesInfo);
	}

	@Override
	public void setJmsHost(NodeInfo jmsNode) {
		this.jmsNode = jmsNode;
	}

	@Override
	public void setResourceConfig(BuildConfig config) {
		this.resourceConfig = config;
	}

	@Override
	public void setSubscribers(int count) {
		log.info("Set subscribers cont to "+count);
		this.subscribers = count;
	}
	
	private void selectNodes() {
		log.info("Select nodes for experiment");
		if (resourceConfig.getTopology() == Topology.TOPIC) {
			senders = ImmutableList.of(nodes.get(0));
			receivers = nodes.subList(1, subscribers+1);
		} else {
			senders = ImmutableList.of(nodes.get(0));
			receivers = ImmutableList.of(nodes.get(1));
		}
		log.info(String.format("Selected. Senders: %s; Receivers: %s.", senders, receivers));
	}

	@Override
	public void start() {
		log.info("Start modeling");
		selectNodes();
		runRmi();
		try { Thread.sleep(5000); } catch (InterruptedException e) {}
		runControls();
	}

	@Override
	public void stop() {
		log.info("Stop controls");
		//waitAllMessagesReceived();
		stopControls();
		collectTraficStat();
		stopRmi();
	}
	
	// TODO no need in this method yet
//	private void waitAllMessagesReceived() {
//		Control senderControl = controlMapping.get(senders.get(0));
//		int sentMessages = senderControl.getMessageSent();
//		for (NodeInfo receiverNode : receivers) {
//			String host = receiverNode.getHost();
//			Control receiverControl = controlMapping.get(host);
//			receiverControl.get
//		}
//	}
	
	private void collectTraficStat() {
		for (Entry<String, Control> entry : controlMapping.entrySet()) {
			String ctlHost = entry.getKey();
			Control ctl = entry.getValue();
			List<PacketInfo> packets = trafficStatMapping.get(ctlHost);
			if (packets != null) {
				packets.addAll(ctl.getTraficStat());
			} else {
				packets = new LinkedList<>(ctl.getTraficStat());
				trafficStatMapping.put(ctlHost, packets);
			}
		}
	}
	
	private void runRmi() {
		log.info("Run rmi on selected nodes");
		for (NodeInfo node : activeNodes()) {
			RmiRunner rmi = new RmiRunner(node);
			rmi.run();
			rmiMapping.put(node.getHost(), rmi);
		}
	}
	
	private void stopRmi() {
		for (RmiRunner rmi : rmiMapping.values()) {
			rmi.stop();
		}
	}
	
	private void runControls() {
		log.info("Run controls");
		startTime = System.currentTimeMillis();
		prepareControls();
		if (resourceConfig.getTopology() == Topology.TOPIC) {
			startSenders();
			startReceivers();
		}
		else {
			startReceivers();
			startSenders();
		}
	}
	
	private void startReceivers() {
		log.info("Run receiver controls");
		for (NodeInfo sender : receivers) {
			log.info("Run control on " + sender.getHost());
			controlMapping.get(sender.getHost()).start();
		}
	}
	
	private void startSenders() {
		log.info("Run sender controls");
		for (NodeInfo sender : senders) {
			log.info("Run control on " + sender.getHost());
			controlMapping.get(sender.getHost()).start();
		}
	}
	
	private String getRmiHost() {
		if (resourceConfig.getTopology() == Topology.TOPIC) {
			return senders.get(0).getHost();
		}
		else {
			return receivers.get(0).getHost();
		}
	}
	
	private String getServerHost() {
		if (resourceConfig.getTopology() == Topology.TOPIC)
			return senders.get(0).getHost();
		else
			return receivers.get(0).getHost();
	}
	
	private void stopControls() {
		for (Entry<String, Control> controlEntry : controlMapping.entrySet()) {
			controlEntry.getValue().stop();
			String host = controlEntry.getKey();
			Control ctl = controlEntry.getValue();
			String errs = ctl.getErrors();
			appendErrs(host, errs);
			collectMessageStat(host, ctl);
			log.info(String.format("%s sent %d and received %d messages",host,ctl.getSentCount(),ctl.getReceivedCount()));
		}
		stopTime = System.currentTimeMillis();
	}
	
	private void collectMessageStat(String host, Control ctl) {
		int currentReceived = (messagesReceived.containsKey(host))? messagesReceived.get(host): 0;
		int currentSent = (messagesSent.containsKey(host))? messagesSent.get(host): 0;
		currentReceived += ctl.getReceivedCount();
		currentSent += ctl.getSentCount();
		messagesReceived.put(host, currentReceived);
		messagesSent.put(host, currentSent);
	}
	
	private void prepareControls() {
		log.info("Prepare controls");
		if (System.getSecurityManager() == null) {
			System.setSecurityManager(new RMISecurityManager());
		}
		for (NodeInfo node : activeNodes()) {
			log.info("Configure controll on node "+node.getHost());
			try (AbstractApplicationContext clientContext = ClientAppContextBuilder.build(node.getHost())) {
				Control control = (Control)clientContext.getBean("remoteControl");
				configControl(control);
				controlMapping.put(node.getHost(), control);
			}
		}
		for (NodeInfo node : senders) {
			Control control = controlMapping.get(node.getHost());
			control.setSender();
			control.setTrafficFilter(createSenderTrafficFilter(node));
		}
		for (NodeInfo node : receivers) {
			Control control = controlMapping.get(node.getHost());
			control.setReceiver();
			control.setTrafficFilter(createReceiverTrafficFilter(node));
		}
	}
	
	private PacketFilter createSenderTrafficFilter(NodeInfo node) {
		FilterBuilder builder = FilterBuilder.allow();
		if (resourceConfig.getTechnology() != Technology.JMS) {
			for (NodeInfo receiver : receivers)
				builder.from(receiver.getHost());
		} else {
			builder.from(jmsNode.getHost());
		}
		builder.to(node.getHost());
		return builder.create();
	}

	private PacketFilter createReceiverTrafficFilter(NodeInfo node) {
		FilterBuilder builder = FilterBuilder.allow();
		if (resourceConfig.getTechnology() != Technology.JMS) {
			for (NodeInfo sender : senders)
				builder.from(sender.getHost());
		} else {
			builder.from(jmsNode.getHost());
		}
		builder.to(node.getHost());
		return builder.create();
	}
	
	private void configControl(Control control) {
		String rmiHost = getRmiHost();
		String jmsHost = jmsNode.getHost();
		String serverHost = getServerHost();
		log.info("Set jms host as "+jmsHost);
		log.info("Set rmi host as "+rmiHost);
		log.info("set http-serv host as "+serverHost);
		control.setBuildConfig(resourceConfig);
		control.setMessageForSent(messageToSend);
		control.setIterationTime(iterationTime);
		control.setMaxIterationCount(maxIterations);
		control.setJmsHost(jmsHost);
		control.setRmiHost(rmiHost);
		control.setServerHost(serverHost);
		control.setTrafficMonitor(true);
	}
	
	@SuppressWarnings("serial")
	private List<NodeInfo> activeNodes() {
		return new ArrayList<NodeInfo>() {
			{
				this.addAll(receivers);
				this.addAll(senders);
			}
		};
	}
	
	private void appendErrs(NodeInfo nodeInfo, String outTail) {
		appendErrs(nodeInfo.getHost(), outTail);
	}
	
	private void appendErrs(String nodeHost, String outTail) {
		String currentOut = Strings.nullToEmpty(errMapping.get(nodeHost));
		currentOut += ((currentOut.equals(""))? "":"\r\n") + outTail;
		errMapping.put(nodeHost, currentOut);
	}

	@Override
	public void setIterationTime(long time) {
		this.iterationTime = time;
	}

	@Override
	public void setMaxIterationCount(int iterationCount) {
		this.maxIterations = iterationCount;
	}

	@Override
	public void setMaxSessionTime(long time) {
		this.maxSessionTime = time;
	}

	@Override
	public long getTotalBytesTransferred() {
		long bytesAmount = 0;
		for (List<PacketInfo> stat : trafficStatMapping.values())
			for (PacketInfo packet : stat)
				bytesAmount += packet.length;
		return bytesAmount;
	}

	@Override
	public Map<String, Long> getBytesTransferred() {
		Map<String, Long> trafficAmountMaping = new HashMap<>();
		for (Entry<String, List<PacketInfo>> entry : trafficStatMapping.entrySet()) {
			long bytesAmount = 0;
			String ctlHost = entry.getKey();
			for (PacketInfo packet : entry.getValue())
				bytesAmount += packet.length;
			trafficAmountMaping.put(ctlHost, bytesAmount);
		}
		return trafficAmountMaping;
	}

	@Override
	public void resetTrafic() {
		for (Control ctl : controlMapping.values())
			ctl.resetTrafic();
	}

	@Override
	public void setMessageToSend(IMessage message) {
		this.messageToSend = message;
	}

	@Override
	public Map<String, String> getErrors() {
		return new HashMap<>(errMapping);
	}

	@Override
	public ModelRunResult getResult() {
		ModelRunResult result = new ModelRunResult();
		result.setTraffic(trafficStatMapping);
		result.setErrors(errMapping);
		result.setStartTime(startTime);
		result.setEndTime(stopTime);
		result.setMessage(messageToSend);
		result.setMessagesSent(messagesSent);
		result.setMessageReceived(messagesReceived);
		result.setConfig(resourceConfig);
		long bytes = 0;
		for (long bytesInHost : getBytesTransferred().values())
			bytes += bytesInHost;
		result.setBytes(bytes);
		int packets = 0;
		for (List<PacketInfo> packetsFromHost : trafficStatMapping.values())
			packets += packetsFromHost.size();
		result.setPackets(packets);
		return result;
	}
}
