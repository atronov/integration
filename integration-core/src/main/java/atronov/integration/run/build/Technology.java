package atronov.integration.run.build;

public enum Technology {
	JMS,
	RMI,
	HESSIAN,
	REST;
	
	public static Technology fromName(String name) {
		for (Technology item : values()) {
			if (item.name().equalsIgnoreCase(name))
				return item;
		}
		throw new IllegalArgumentException("No enum item with name="+name);
	}
}
