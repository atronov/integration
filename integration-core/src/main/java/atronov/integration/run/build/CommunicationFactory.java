package atronov.integration.run.build;

import atronov.integration.transfer.Receiver;
import atronov.integration.transfer.Sender;

public interface CommunicationFactory {
	Receiver createReceiver();
	Sender createSender();
}
