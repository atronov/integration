package atronov.integration.run.build;

import java.io.Serializable;

import com.google.common.base.Objects;

public class BuildConfig implements Serializable {
	private static final long serialVersionUID = 121754710194073626L;
	
	private final Technology technology;
	private final Protocol protocol;
	private final DataType dataType;
	private final Topology topology;
	private final boolean ssl;
	
	public BuildConfig(
			Technology technology,
			Topology topology,
			Protocol protocol,
			DataType dataType,
			boolean ssl
		) {
		this.technology = technology;
		this.protocol = protocol;
		this.dataType = dataType;
		this.ssl = ssl;
		this.topology = topology;
	}
	
	@Override
	public boolean equals(Object obj) {
		if (obj == null)
			return false;
		if (obj instanceof BuildConfig) {
			BuildConfig item = (BuildConfig) obj;
			return this.technology == item.technology
					&& this.protocol == item.protocol
					&& this.topology == item.topology
					&& this.dataType == item.dataType
					&& this.ssl == item.ssl;
		} else {
			return false;
		}
	}
	
	@Override
	public String toString() {
		return Objects.toStringHelper("BuildConfig")
		.add("technology", getTechnology())
		.add("protocol", getProtocol())
		.add("topology", getTopology())
		.add("dataType", getDataType())
		.add("ssl", isSsl())
		.toString();
	}
	

	public Technology getTechnology() {
		return technology;
	}
	
	public Topology getTopology() {
		return topology;
	}

	public Protocol getProtocol() {
		return protocol;
	}

	public DataType getDataType() {
		return dataType;
	}

	public boolean isSsl() {
		return ssl;
	}
	
}
