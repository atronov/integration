package atronov.integration.run.build;

import java.util.List;

import org.apache.log4j.Logger;

import atronov.integration.transfer.Receiver;
import atronov.integration.transfer.Sender;

import com.google.common.base.Predicate;
import com.google.common.collect.FluentIterable;

public class ResourceBuilder {
	
	private final Logger log = Logger.getLogger(this.getClass());
	
	private final static List<BuildMappingItem> mapping = MappingBuilder.create();
	
	private final Technology technology;
	private Protocol protocol = null;
	private DataType dataType = null;
	private Topology topology = null;
	private Boolean ssl = false;
	
	private ResourceBuilder(Technology technology) {
		this.technology = technology;
	}
	
	public ResourceBuilder use(Protocol protocol) {
		this.protocol = protocol;
		return this;
	}
	
	public ResourceBuilder use(DataType dataType) {
		this.dataType = dataType;
		return this;
	}
	
	public ResourceBuilder use(Topology topology) {
		this.topology = topology;
		return this;
	}
	
	public ResourceBuilder useSsl() {
		ssl = true;
		return this; 
	}
	
	public ResourceBuilder dontUseSsl() {
		ssl = false;
		return this; 
	}
	
	public static ResourceBuilder forTechology(Technology technology) {
		return new ResourceBuilder(technology);
	}
	
	public static boolean canBuild(final BuildConfig config) {
		Predicate<BuildMappingItem> searchPredicate = new Predicate<BuildMappingItem>() {
			public boolean apply(BuildMappingItem arg0) {
				return config.equals(arg0);
			}
		};
		return FluentIterable
			.from(mapping)
			.anyMatch(searchPredicate);
	}
	
	public static ResourceBuilder fromConfig(BuildConfig config) {
		ResourceBuilder builder = forTechology(config.getTechnology())
				.use(config.getTopology())
				.use(config.getDataType())
				.use(config.getProtocol());
		builder.ssl = config.isSsl();
		return builder;
	}
	
	public Sender buildSender() {
		log.info("Create sender");
		return findMapping().getFactory().createSender();
	}
	
	public Receiver buildReceiver() {
		log.info("Create receiver");
		return findMapping().getFactory().createReceiver();
	}
	
	private BuildMappingItem findMapping() {
		final BuildConfig mockMappingItem = new BuildConfig(technology, topology, protocol, dataType, ssl);
		Predicate<BuildMappingItem> searchPredicate = new Predicate<BuildMappingItem>() {
			public boolean apply(BuildMappingItem arg0) {
				return mockMappingItem.equals(arg0);
			}
		};
		List<BuildMappingItem> foundMappings = FluentIterable
			.from(mapping)
			.filter(searchPredicate)
			.toList();
		if (foundMappings.size() > 1)
			throw new IllegalStateException("Exists more then one mapping matched to required conditionds.\r\nResult: "+foundMappings);
		if (foundMappings.size() == 0)
			throw new IllegalStateException("Exists no mapping matched to required conditionds.\r\nPrototipe: "+mockMappingItem);
		return foundMappings.get(0);
	}
}
