package atronov.integration.run.build;

import com.google.common.base.Objects;
import com.google.gson.annotations.Expose;

public class BuildMappingItem extends BuildConfig {

	private final CommunicationFactory factory;
	
	public BuildMappingItem(
			Technology technology,
			Topology topology,
			Protocol protocol,
			DataType dataType,
			boolean ssl,
			CommunicationFactory factory
		) {
		super(technology,topology,protocol,dataType, ssl);
		this.factory = factory;
	}
	
	@Override
	public String toString() {
		return Objects.toStringHelper("Mapping")
		.add("technology", getTechnology())
		.add("protocol", getProtocol())
		.add("topology", getTopology())
		.add("dataType", getDataType())
		.add("ssl", isSsl())
		.add("factory", getFactory().getClass())
		.toString();
	}

	public CommunicationFactory getFactory() {
		return factory;
	}
	
}
