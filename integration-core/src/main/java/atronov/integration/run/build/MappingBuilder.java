package atronov.integration.run.build;

import static atronov.integration.run.build.DataType.BINARY;
import static atronov.integration.run.build.DataType.TEXT;
import static atronov.integration.run.build.Protocol.HTTP;
import static atronov.integration.run.build.Protocol.TCP;
import static atronov.integration.run.build.Technology.HESSIAN;
import static atronov.integration.run.build.Technology.JMS;
import static atronov.integration.run.build.Technology.REST;
import static atronov.integration.run.build.Technology.RMI;
import static atronov.integration.run.build.Topology.QUEUE;
import static atronov.integration.run.build.Topology.TOPIC;

import java.util.List;

import atronov.integration.config.JmsConfing;
import atronov.integration.message.MessageHandlingFactory;
import atronov.integration.message.bson.BsonMessageHandlingFactory;
import atronov.integration.message.json.JsonMessageHandlingFactory;
import atronov.integration.transfer.Receiver;
import atronov.integration.transfer.Sender;
import atronov.integration.transfer.hessian.HessianPublisher;
import atronov.integration.transfer.hessian.HessianReceiver;
import atronov.integration.transfer.hessian.HessianSender;
import atronov.integration.transfer.hessian.HessianSubcriber;
import atronov.integration.transfer.jms.JmsBinaryReceiver;
import atronov.integration.transfer.jms.JmsBinarySender;
import atronov.integration.transfer.jms.JmsBinarySubscriber;
import atronov.integration.transfer.jms.JmsTextReceiver;
import atronov.integration.transfer.jms.JmsTextSender;
import atronov.integration.transfer.jms.JmsTextSubscriber;
import atronov.integration.transfer.jms.resources.HttpJmsResourcesFactory;
import atronov.integration.transfer.jms.resources.JmsResourcesFactory;
import atronov.integration.transfer.jms.resources.TcpJmsResourcesFactory;
import atronov.integration.transfer.rest.RestBinaryPublisher;
import atronov.integration.transfer.rest.RestBinaryReceiver;
import atronov.integration.transfer.rest.RestBinarySender;
import atronov.integration.transfer.rest.RestBinarySubscriber;
import atronov.integration.transfer.rest.RestTextPublisher;
import atronov.integration.transfer.rest.RestTextReceiver;
import atronov.integration.transfer.rest.RestTextSender;
import atronov.integration.transfer.rest.RestTextSubscriber;
import atronov.integration.transfer.rmi.spring.SpringHttpPublisher;
import atronov.integration.transfer.rmi.spring.SpringHttpReceiver;
import atronov.integration.transfer.rmi.spring.SpringSender;
import atronov.integration.transfer.rmi.spring.SpringSubscriber;
import atronov.integration.transfer.rmi.spring.SpringTcpPublisher;
import atronov.integration.transfer.rmi.spring.SpringTcpReceiver;
import atronov.integration.transfer.rmi.spring.context.ContextFactory;
import atronov.integration.transfer.rmi.spring.context.HttpContextFactory;
import atronov.integration.transfer.rmi.spring.context.HttpsContextFactory;
import atronov.integration.transfer.rmi.spring.context.TcpContextFactory;
import atronov.integration.transfer.rmi.spring.context.TcpSslContextFactory;
import atronov.integration.transfer.server.CommunicationConfigFactory;
import atronov.integration.transfer.server.SecureCommunicationFactory;
import atronov.integration.transfer.server.SimpleCommunicationFactory;

import com.google.common.collect.ImmutableList;

public class MappingBuilder {

	private MappingBuilder() {}
	
	public static List<BuildConfig> createPossible() {
		List<BuildConfig> mapping = ImmutableList.of(
				// jms
				new BuildConfig(JMS, QUEUE, TCP, TEXT, false),
				new BuildConfig(JMS, QUEUE, TCP, TEXT, true),
				new BuildConfig(JMS, QUEUE, TCP, BINARY, false),
				new BuildConfig(JMS, QUEUE, TCP, BINARY, true),
				new BuildConfig(JMS, QUEUE, HTTP, TEXT, false),
				new BuildConfig(JMS, QUEUE, HTTP, BINARY, false),
				new BuildConfig(JMS, TOPIC, TCP, TEXT, false),
				new BuildConfig(JMS, TOPIC, TCP, TEXT, true),
				new BuildConfig(JMS, TOPIC, TCP, BINARY, false),
				new BuildConfig(JMS, TOPIC, TCP, BINARY, true),
				new BuildConfig(JMS, TOPIC, HTTP, TEXT, false),
				new BuildConfig(JMS, TOPIC, HTTP, BINARY, false),
				// rest
				new BuildConfig(REST, QUEUE, HTTP, TEXT, false),
				new BuildConfig(REST, QUEUE, HTTP, TEXT, true),
				new BuildConfig(REST, QUEUE, HTTP, BINARY, false),
				new BuildConfig(REST, QUEUE, HTTP, BINARY, true),
				new BuildConfig(REST, TOPIC, HTTP, TEXT, false),
				new BuildConfig(REST, TOPIC, HTTP, TEXT, true),
				new BuildConfig(REST, TOPIC, HTTP, BINARY, false),
				new BuildConfig(REST, TOPIC, HTTP, BINARY, true),
				// rmi
				new BuildConfig(RMI, QUEUE, TCP, BINARY, false),
				new BuildConfig(RMI, QUEUE, TCP, BINARY, true),
				new BuildConfig(RMI, QUEUE, HTTP, BINARY, false),
				new BuildConfig(RMI, QUEUE, HTTP, BINARY, true),
				new BuildConfig(RMI, TOPIC, TCP, BINARY, false),
				new BuildConfig(RMI, TOPIC, TCP, BINARY, true),
				new BuildConfig(RMI, TOPIC, HTTP, BINARY, false),
				new BuildConfig(RMI, TOPIC, HTTP, BINARY, true),
				// hessian
				new BuildConfig(HESSIAN, QUEUE, HTTP, BINARY, false),
				new BuildConfig(HESSIAN, QUEUE, HTTP, BINARY, true),
				new BuildConfig(HESSIAN, TOPIC, HTTP, BINARY, false),
				new BuildConfig(HESSIAN, TOPIC, HTTP, BINARY, true)
		);
		return mapping;
	}
	
	public static List<BuildMappingItem> create() {
		// handling
		final MessageHandlingFactory<String> messageTextHandling = new JsonMessageHandlingFactory();
		final MessageHandlingFactory<byte[]> messageBinaryHandling = new BsonMessageHandlingFactory();
		// jms resources
		final JmsResourcesFactory tcpQueueJmsResources = new TcpJmsResourcesFactory(JmsConfing.get().jmsTcpFactory(), JmsConfing.get().jmsQueue());
		final JmsResourcesFactory tcpTopicJmsResources = new TcpJmsResourcesFactory(JmsConfing.get().jmsTcpFactory(), JmsConfing.get().jmsTopic());
		final JmsResourcesFactory sslTcpQueueJmsResources = new TcpJmsResourcesFactory(JmsConfing.get().jmsTcpSslFactory(), JmsConfing.get().jmsQueue());
		final JmsResourcesFactory sslTcpTopicJmsResources = new TcpJmsResourcesFactory(JmsConfing.get().jmsTcpSslFactory(), JmsConfing.get().jmsTopic());
		final JmsResourcesFactory httpQueueJmsResources = new HttpJmsResourcesFactory(JmsConfing.get().jmsHttpFactory(), JmsConfing.get().jmsQueue());
		final JmsResourcesFactory httpTopicJmsResources = new HttpJmsResourcesFactory(JmsConfing.get().jmsHttpFactory(), JmsConfing.get().jmsTopic());
		// http communication
		final CommunicationConfigFactory httpCommunication = new SimpleCommunicationFactory();
		final CommunicationConfigFactory httpsCommunication = new SecureCommunicationFactory();
		// spring
		final ContextFactory tcpContextFactory = new TcpContextFactory();
		final ContextFactory tcpSslContextFactory = new TcpSslContextFactory();
		final ContextFactory httpContextFactory = new HttpContextFactory();
		final ContextFactory httpsContextFactory = new HttpsContextFactory();
		// jms text
		CommunicationFactory jmsTcpTextQueueFactory = new CommunicationFactory() {
			public Sender createSender() {
				return new JmsTextSender(tcpQueueJmsResources, messageTextHandling);
			}
			public Receiver createReceiver() {
				return new JmsTextReceiver(tcpQueueJmsResources, messageTextHandling);
			}
		};
		CommunicationFactory jmsSslTcpTextQueueFactory = new CommunicationFactory() {
			public Sender createSender() {
				return new JmsTextSender(sslTcpQueueJmsResources, messageTextHandling);
			}
			public Receiver createReceiver() {
				return new JmsTextReceiver(sslTcpQueueJmsResources, messageTextHandling);
			}
		};
		CommunicationFactory jmsTcpTextTopicFactory = new CommunicationFactory() {
			public Sender createSender() {
				return new JmsTextSender(tcpTopicJmsResources, messageTextHandling);
			}
			public Receiver createReceiver() {
				return new JmsTextSubscriber(tcpTopicJmsResources, messageTextHandling);
			}
		};
		CommunicationFactory jmsSslTcpTextTopicFactory = new CommunicationFactory() {
			public Sender createSender() {
				return new JmsTextSender(sslTcpTopicJmsResources, messageTextHandling);
			}
			public Receiver createReceiver() {
				return new JmsTextSubscriber(sslTcpTopicJmsResources, messageTextHandling);
			}
		};
		CommunicationFactory jmsHttpTextQueueFactory = new CommunicationFactory() {
			public Sender createSender() {
				return new JmsTextSender(httpQueueJmsResources, messageTextHandling);
			}
			public Receiver createReceiver() {
				return new JmsTextReceiver(httpQueueJmsResources, messageTextHandling);
			}
		};
		CommunicationFactory jmsHttpTextTopicFactory = new CommunicationFactory() {
			public Sender createSender() {
				return new JmsTextSender(httpTopicJmsResources, messageTextHandling);
			}
			public Receiver createReceiver() {
				return new JmsTextSubscriber(httpTopicJmsResources, messageTextHandling);
			}
		};
		// jms binary
		CommunicationFactory jmsTcpBinaryQueueFactory = new CommunicationFactory() {
			public Sender createSender() {
				return new JmsBinarySender(tcpQueueJmsResources, messageBinaryHandling);
			}
			public Receiver createReceiver() {
				return new JmsBinaryReceiver(tcpQueueJmsResources, messageBinaryHandling);
			}
		};
		CommunicationFactory jmsSslTcpBinaryQueueFactory = new CommunicationFactory() {
			public Sender createSender() {
				return new JmsBinarySender(sslTcpQueueJmsResources, messageBinaryHandling);
			}
			public Receiver createReceiver() {
				return new JmsBinaryReceiver(sslTcpQueueJmsResources, messageBinaryHandling);
			}
		};
		CommunicationFactory jmsTcpBinaryTopicFactory = new CommunicationFactory() {
			public Sender createSender() {
				return new JmsBinarySender(tcpTopicJmsResources, messageBinaryHandling);
			}
			public Receiver createReceiver() {
				return new JmsBinarySubscriber(tcpTopicJmsResources, messageBinaryHandling);
			}
		};
		CommunicationFactory jmsSslTcpBinaryTopicFactory = new CommunicationFactory() {
			public Sender createSender() {
				return new JmsBinarySender(sslTcpTopicJmsResources, messageBinaryHandling);
			}
			public Receiver createReceiver() {
				return new JmsBinarySubscriber(sslTcpTopicJmsResources, messageBinaryHandling);
			}
		};
		CommunicationFactory jmsHttpBinaryQueueFactory = new CommunicationFactory() {
			public Sender createSender() {
				return new JmsBinarySender(httpQueueJmsResources, messageBinaryHandling);
			}
			public Receiver createReceiver() {
				return new JmsBinaryReceiver(httpQueueJmsResources, messageBinaryHandling);
			}
		};
		CommunicationFactory jmsHttpBinaryTopicFactory = new CommunicationFactory() {
			public Sender createSender() {
				return new JmsBinarySender(httpTopicJmsResources, messageBinaryHandling);
			}
			public Receiver createReceiver() {
				return new JmsBinarySubscriber(httpTopicJmsResources, messageBinaryHandling);
			}
		};
		// rest queue
		CommunicationFactory restQueueTextFactory = new CommunicationFactory() {
			public Receiver createReceiver() {
				return new RestTextReceiver(httpCommunication, messageTextHandling);
			}
			public Sender createSender() {
				return new RestTextSender(httpCommunication, messageTextHandling);
			}
		};
		CommunicationFactory restQueueSecureTextFactory = new CommunicationFactory() {
			public Receiver createReceiver() {
				return new RestTextReceiver(httpsCommunication, messageTextHandling);
			}
			public Sender createSender() {
				return new RestTextSender(httpsCommunication, messageTextHandling);
			}
		};
		CommunicationFactory restQueueBinaryFactory = new CommunicationFactory() {
			public Receiver createReceiver() {
				return new RestBinaryReceiver(httpCommunication, messageBinaryHandling);
			}
			public Sender createSender() {
				return new RestBinarySender(httpCommunication, messageBinaryHandling);
			}
		};
		CommunicationFactory restQueueSecureBinaryFactory = new CommunicationFactory() {
			public Receiver createReceiver() {
				return new RestBinaryReceiver(httpsCommunication, messageBinaryHandling);
			}
			public Sender createSender() {
				return new RestBinarySender(httpsCommunication, messageBinaryHandling);
			}
		};
		// rest topic
		CommunicationFactory restTopicTextFactory = new CommunicationFactory() {
			public Receiver createReceiver() {
				return new RestTextSubscriber(httpCommunication, messageTextHandling);
			}
			public Sender createSender() {
				return new RestTextPublisher(httpCommunication, messageTextHandling);
			}
		};
		CommunicationFactory restTopicSecureTextFactory = new CommunicationFactory() {
			public Receiver createReceiver() {
				return new RestTextSubscriber(httpsCommunication, messageTextHandling);
			}
			public Sender createSender() {
				return new RestTextPublisher(httpsCommunication, messageTextHandling);
			}
		};
		CommunicationFactory restTopicBinaryFactory = new CommunicationFactory() {
			public Receiver createReceiver() {
				return new RestBinarySubscriber(httpCommunication, messageBinaryHandling);
			}
			public Sender createSender() {
				return new RestBinaryPublisher(httpCommunication, messageBinaryHandling);
			}
		};
		CommunicationFactory restTopicSecureBinaryFactory = new CommunicationFactory() {
			public Receiver createReceiver() {
				return new RestBinarySubscriber(httpsCommunication, messageBinaryHandling);
			}
			public Sender createSender() {
				return new RestBinaryPublisher(httpsCommunication, messageBinaryHandling);
			}
		};
		// rmi queue
		CommunicationFactory rmiTcpQueueFactory = new CommunicationFactory() {
			public Sender createSender() { return new SpringSender(tcpContextFactory);	}
			public Receiver createReceiver() { return new SpringTcpReceiver(tcpContextFactory); }
		};
		CommunicationFactory rmiSslTcpQueueFactory = new CommunicationFactory() {
			public Sender createSender() { return new SpringSender(tcpSslContextFactory);	}
			public Receiver createReceiver() { return new SpringTcpReceiver(tcpSslContextFactory); }
		};
		CommunicationFactory rmiHttpQueueFactory = new CommunicationFactory() {
			public Sender createSender() { return new SpringSender(httpContextFactory);	}
			public Receiver createReceiver() { return new SpringHttpReceiver(httpContextFactory, httpCommunication); }
		};
		CommunicationFactory rmiHttpsQueueFactory = new CommunicationFactory() {
			public Sender createSender() { return new SpringSender(httpsContextFactory);	}
			public Receiver createReceiver() { return new SpringHttpReceiver(httpsContextFactory, httpsCommunication); }
		};
		// rmi topic
		CommunicationFactory rmiTcpTopicFactory = new CommunicationFactory() {
			public Sender createSender() { return new SpringTcpPublisher(tcpContextFactory);	}
			public Receiver createReceiver() { return new SpringSubscriber(tcpContextFactory); }
		};
		CommunicationFactory rmiSslTcpTopicFactory = new CommunicationFactory() {
			public Sender createSender() { return new SpringTcpPublisher(tcpSslContextFactory);	}
			public Receiver createReceiver() { return new SpringSubscriber(tcpSslContextFactory); }
		};
		CommunicationFactory rmiHttpTopicFactory = new CommunicationFactory() {
			public Sender createSender() { return new SpringHttpPublisher(httpContextFactory, httpCommunication);	}
			public Receiver createReceiver() { return new SpringSubscriber(httpContextFactory); }
		};
		CommunicationFactory rmiHttpsTopicFactory = new CommunicationFactory() {
			public Sender createSender() { return new SpringHttpPublisher(httpsContextFactory, httpsCommunication);	}
			public Receiver createReceiver() { return new SpringSubscriber(httpsContextFactory); }
		};
		// hessian queue
		CommunicationFactory hessianQueueFactory = new CommunicationFactory() {
			public Sender createSender() { return new HessianSender(httpCommunication); }
			public Receiver createReceiver() { return new HessianReceiver(httpCommunication); }
		};
		CommunicationFactory hessianSecureQueueFactory = new CommunicationFactory() {
			public Sender createSender() { return new HessianSender(httpsCommunication); }
			public Receiver createReceiver() { return new HessianReceiver(httpsCommunication); }
		};
		// hessian topic
		CommunicationFactory hessianTopicFactory = new CommunicationFactory() {
			public Sender createSender() { return new HessianPublisher(httpCommunication); }
			public Receiver createReceiver() { return new HessianSubcriber(httpCommunication); }
		};
		CommunicationFactory hessianTopicQueueFactory = new CommunicationFactory() {
			public Sender createSender() { return new HessianPublisher(httpsCommunication); }
			public Receiver createReceiver() { return new HessianSubcriber(httpsCommunication); }
		};
		List<BuildMappingItem> mapping = ImmutableList.of(
				// jms
				new BuildMappingItem(JMS, QUEUE, TCP, TEXT, false, jmsTcpTextQueueFactory),
				new BuildMappingItem(JMS, QUEUE, TCP, TEXT, true, jmsSslTcpTextQueueFactory),
				new BuildMappingItem(JMS, QUEUE, TCP, BINARY, false, jmsTcpBinaryQueueFactory),
				new BuildMappingItem(JMS, QUEUE, TCP, BINARY, true, jmsSslTcpBinaryQueueFactory),
				new BuildMappingItem(JMS, QUEUE, HTTP, TEXT, false, jmsHttpTextQueueFactory),
				new BuildMappingItem(JMS, QUEUE, HTTP, BINARY, false, jmsHttpBinaryQueueFactory),
				new BuildMappingItem(JMS, TOPIC, TCP, TEXT, false, jmsTcpTextTopicFactory),
				new BuildMappingItem(JMS, TOPIC, TCP, TEXT, true, jmsSslTcpTextTopicFactory),
				new BuildMappingItem(JMS, TOPIC, TCP, BINARY, false, jmsTcpBinaryTopicFactory),
				new BuildMappingItem(JMS, TOPIC, TCP, BINARY, true, jmsSslTcpBinaryTopicFactory),
				new BuildMappingItem(JMS, TOPIC, HTTP, TEXT, false, jmsHttpTextTopicFactory),
				new BuildMappingItem(JMS, TOPIC, HTTP, BINARY, false, jmsHttpBinaryTopicFactory),
				// rest
				new BuildMappingItem(REST, QUEUE, HTTP, TEXT, false, restQueueTextFactory),
				new BuildMappingItem(REST, QUEUE, HTTP, TEXT, true, restQueueSecureTextFactory),
				new BuildMappingItem(REST, QUEUE, HTTP, BINARY, false, restQueueBinaryFactory),
				new BuildMappingItem(REST, QUEUE, HTTP, BINARY, true, restQueueSecureBinaryFactory),
				new BuildMappingItem(REST, TOPIC, HTTP, TEXT, false, restTopicTextFactory),
				new BuildMappingItem(REST, TOPIC, HTTP, TEXT, true, restTopicSecureTextFactory),
				new BuildMappingItem(REST, TOPIC, HTTP, BINARY, false, restTopicBinaryFactory),
				new BuildMappingItem(REST, TOPIC, HTTP, BINARY, true, restTopicSecureBinaryFactory),
				// rmi
				new BuildMappingItem(RMI, QUEUE, TCP, BINARY, false, rmiTcpQueueFactory),
				new BuildMappingItem(RMI, QUEUE, TCP, BINARY, true, rmiSslTcpQueueFactory),
				new BuildMappingItem(RMI, QUEUE, HTTP, BINARY, false, rmiHttpQueueFactory),
				new BuildMappingItem(RMI, QUEUE, HTTP, BINARY, true, rmiHttpsQueueFactory),
				new BuildMappingItem(RMI, TOPIC, TCP, BINARY, false, rmiTcpTopicFactory),
				new BuildMappingItem(RMI, TOPIC, TCP, BINARY, true, rmiSslTcpTopicFactory),
				new BuildMappingItem(RMI, TOPIC, HTTP, BINARY, false, rmiHttpTopicFactory),
				new BuildMappingItem(RMI, TOPIC, HTTP, BINARY, true, rmiHttpsTopicFactory),
				// hessian
				new BuildMappingItem(HESSIAN, QUEUE, HTTP, BINARY, false, hessianQueueFactory),
				new BuildMappingItem(HESSIAN, QUEUE, HTTP, BINARY, true, hessianSecureQueueFactory),
				new BuildMappingItem(HESSIAN, TOPIC, HTTP, BINARY, false, hessianTopicFactory),
				new BuildMappingItem(HESSIAN, TOPIC, HTTP, BINARY, true, hessianTopicQueueFactory)
		);
		return mapping;
	}
}
