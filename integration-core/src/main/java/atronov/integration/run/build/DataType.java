package atronov.integration.run.build;

public enum DataType {
	TEXT,
	BINARY
}
