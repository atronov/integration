package atronov.integration.run.console;

import org.kohsuke.args4j.Option;

public class RunOptions {
	
	@Option(name="-rec", usage="run as receiver")
  private boolean receiver = false;
	
	@Option(name="-send", usage="run as sender")
  private boolean sender = false;
	
	@Option(name="-tech", usage="run as sender")
  private String technology = "jms";
	
	@Option(name="-tcp", usage="use tcp protocol" )
  private boolean tcp = false;
	
	@Option(name="-http", usage="use http protocol")
  private boolean http = false;

	@Option(name="-queue", usage="queue topology")
  private boolean queue = false;
	
	@Option(name="-topic", usage="topic topology")
  private boolean topic = false;
	
	@Option(name="-text", usage="text data")
  private boolean text = false;
	
	@Option(name="-bin", usage="binary data")
  private boolean binary = false;
	
  @Option(name="-ssl",usage="use ssl")
  private boolean ssl = false;
  
  @Option(name="-iter", usage="numer of iterrations")
  private int iterrationCount = 1;
  
  // sender shedule
  @Option(name="-msg", depends= {"-send" }, usage="numer of messages in one iterration")
  private int messagesInIterration = 5;

  // receiver shedule
  @Option(name="-period", depends= {"-receiver" }, usage="period of check storage")
  private int checkPeriod = 1;
  
  
  
	public boolean isReceiver() {
		return receiver;
	}

	public boolean isSender() {
		return sender;
	}

	public boolean isTcp() {
		return tcp;
	}

	public boolean isHttp() {
		return http;
	}

	public boolean isQueue() {
		return queue;
	}

	public boolean isTopic() {
		return topic;
	}

	public boolean isText() {
		return text;
	}

	public boolean isBinary() {
		return binary;
	}

	public boolean isSsl() {
		return ssl;
	}

	public int getIterrationCount() {
		return iterrationCount;
	}

	public int getMessagesInIterration() {
		return messagesInIterration;
	}

	public int getCheckPeriod() {
		return checkPeriod;
	}

	public String getTechnology() {
		return technology;
	}
  
}
