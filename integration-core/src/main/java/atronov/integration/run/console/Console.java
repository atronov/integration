package atronov.integration.run.console;

import static com.google.common.base.Preconditions.*;
import static atronov.integration.run.build.Technology.*;
import static atronov.integration.run.build.Topology.*;
import static atronov.integration.run.build.DataType.*;
import static atronov.integration.run.build.Protocol.*;

import org.jnetpcap.protocol.network.Arp.OpCode;
import org.kohsuke.args4j.CmdLineException;
import org.kohsuke.args4j.CmdLineParser;
import org.springframework.expression.spel.ast.OpInc;

import atronov.integration.message.IMessage;
import atronov.integration.message.IMixedMessage;
import atronov.integration.message.simple.PropertyMessageFactory;
import atronov.integration.run.build.DataType;
import atronov.integration.run.build.Protocol;
import atronov.integration.run.build.ResourceBuilder;
import atronov.integration.run.build.Technology;
import atronov.integration.run.build.Topology;
import atronov.integration.transfer.Receiver;
import atronov.integration.transfer.Sender;

import com.google.common.base.Preconditions;
import com.sun.grizzly.util.ConcurrentReferenceHashMap.Option;

public class Console {
	public static void main(String[] args) throws Exception {
		RunOptions options = new RunOptions();
		CmdLineParser parser = new CmdLineParser(options);
		parser.parseArgument(args);
		checkOptions(options);
		Technology technology = Technology.fromName(options.getTechnology());
		Topology topology = (options.isQueue())? QUEUE: TOPIC;
		DataType dataType = (options.isText())? TEXT: BINARY;
		Protocol protocol = (options.isTcp())? TCP: HTTP;
		int iterrations = options.getIterrationCount();
		ResourceBuilder builder = ResourceBuilder
		.forTechology(technology)
		.use(protocol)
		.use(topology)
		.use(dataType);
		if (options.isSsl())
			builder.useSsl();
		else
			builder.dontUseSsl();
		System.out.println(String.format("%s %s %s %s ssl=%s iter=%s",
				technology,
				topology,
				dataType,
				protocol,
				options.isSsl(),
				iterrations
				));
		if (options.isSender()) {
			System.out.println("Create sender");
			Sender sender = builder.buildSender();
			try (AutoCloseable res = (sender instanceof AutoCloseable)? (AutoCloseable)sender: null) {
				IMixedMessage message = new PropertyMessageFactory().createMixedMessage();
				for (int i = 0; i < iterrations; i++) {
					System.out.println("Send message: "+message);
					sender.send(message);
					Thread.sleep(1000);
				}
			}
		} else {
			System.out.println("Create receiver");
			Receiver receiver = builder.buildReceiver();
			System.out.println("Wait while server start");
			Thread.sleep(3000);
			try (AutoCloseable res = (receiver instanceof AutoCloseable)? (AutoCloseable)receiver: null) {
				for (int i = 0; i < iterrations; i++) {
					System.out.println("Try receive message");
					IMessage received = receiver.getReceived();
					System.out.println("Received: "+received);
					Thread.sleep(1000);
				}
			}
		}
		
	}
	
	private static void checkOptions(RunOptions options) {
		checkArgument(options.isReceiver() ^ options.isSender(),"Cannot be run as sender and receiver simultaneously");
		checkArgument(options.isTcp() ^ options.isHttp(),"Cannot work via HTTP and TCP simultaneously");
		checkArgument(options.isBinary() ^ options.isText(),"Cannot use text and binary simultaneously");
		checkArgument(options.isQueue() ^ options.isTopic(),"Cannot be run as queue and topic simultaneously");
		
	}
}
