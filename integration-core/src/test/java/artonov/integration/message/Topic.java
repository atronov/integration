package artonov.integration.message;

import static org.junit.Assert.assertArrayEquals;
import static org.junit.Assert.assertEquals;

import org.junit.Test;

import atronov.integration.config.JmsConfing;
import atronov.integration.message.IMixedMessage;
import atronov.integration.message.MessageHandlingFactory;
import atronov.integration.message.bson.BsonMessageHandlingFactory;
import atronov.integration.message.json.JsonMessageHandlingFactory;
import atronov.integration.message.simple.PropertyMessageFactory;
import atronov.integration.transfer.ClosableReceiver;
import atronov.integration.transfer.ClosableSender;
import atronov.integration.transfer.Receiver;
import atronov.integration.transfer.hessian.HessianPublisher;
import atronov.integration.transfer.hessian.HessianSubcriber;
import atronov.integration.transfer.jms.JmsTextSender;
import atronov.integration.transfer.jms.JmsTextSubscriber;
import atronov.integration.transfer.jms.resources.JmsResourcesFactory;
import atronov.integration.transfer.jms.resources.TcpJmsResourcesFactory;
import atronov.integration.transfer.rest.RestBinaryPublisher;
import atronov.integration.transfer.rest.RestBinarySubscriber;
import atronov.integration.transfer.rest.RestTextPublisher;
import atronov.integration.transfer.rest.RestTextSubscriber;
import atronov.integration.transfer.rmi.spring.SpringHttpPublisher;
import atronov.integration.transfer.rmi.spring.SpringSubscriber;
import atronov.integration.transfer.rmi.spring.SpringTcpPublisher;
import atronov.integration.transfer.rmi.spring.context.ContextFactory;
import atronov.integration.transfer.rmi.spring.context.HttpContextFactory;
import atronov.integration.transfer.rmi.spring.context.HttpsContextFactory;
import atronov.integration.transfer.rmi.spring.context.TcpContextFactory;
import atronov.integration.transfer.rmi.spring.context.TcpSslContextFactory;
import atronov.integration.transfer.server.CommunicationConfigFactory;
import atronov.integration.transfer.server.SecureCommunicationFactory;
import atronov.integration.transfer.server.SimpleCommunicationFactory;

public class Topic {

	@Test
	public void jms() throws Exception {
		JmsResourcesFactory tcpTopicJmsResources = new TcpJmsResourcesFactory(JmsConfing.get().jmsTcpFactory(), JmsConfing.get().jmsTopic());
		MessageHandlingFactory<String> messageTextHandling = new JsonMessageHandlingFactory();
		IMixedMessage sentMessage = new PropertyMessageFactory().createMixedMessage();
		try (ClosableSender sender = new JmsTextSender(tcpTopicJmsResources, messageTextHandling);
				ClosableReceiver receiver1 = new JmsTextSubscriber(tcpTopicJmsResources, messageTextHandling);
				ClosableReceiver receiver2 = new JmsTextSubscriber(tcpTopicJmsResources, messageTextHandling)) {
			sender.send(sentMessage);
	    try { Thread.sleep(3000);} catch (InterruptedException e) {}
			IMixedMessage receivedMessage1 = (IMixedMessage) receiver1.getReceived();
			IMixedMessage receivedMessage2 = (IMixedMessage) receiver2.getReceived();
			assertMessages(receivedMessage1, receivedMessage2);
		}
	}
	
	@Test
	public void restText() throws Exception {
		CommunicationConfigFactory simpleCommunication = new SimpleCommunicationFactory();
		MessageHandlingFactory<String> messageTextHandling = new JsonMessageHandlingFactory();
		try (ClosableSender sender = new RestTextPublisher(simpleCommunication, messageTextHandling)) {
			Receiver receiver1 = new RestTextSubscriber(simpleCommunication, messageTextHandling);
			Receiver receiver2 = new RestTextSubscriber(simpleCommunication, messageTextHandling);
			IMixedMessage sentMessage = new PropertyMessageFactory().createMixedMessage();
			sender.send(sentMessage);
	    try { Thread.sleep(1000);} catch (InterruptedException e) {}
			IMixedMessage receivedMessage1 = (IMixedMessage) receiver1.getReceived();
			IMixedMessage receivedMessage2 = (IMixedMessage) receiver2.getReceived();
			assertMessages(receivedMessage1, receivedMessage2);
		}
	}
	
	@Test
	public void restBinary() throws Exception {
		CommunicationConfigFactory simpleCommunication = new SimpleCommunicationFactory();
		MessageHandlingFactory<byte[]> messageTextHandling = new BsonMessageHandlingFactory();
		try (ClosableSender sender = new RestBinaryPublisher(simpleCommunication, messageTextHandling)) {
			Receiver receiver1 = new RestBinarySubscriber(simpleCommunication, messageTextHandling);
			Receiver receiver2 = new RestBinarySubscriber(simpleCommunication, messageTextHandling);
			IMixedMessage sentMessage = new PropertyMessageFactory().createMixedMessage();
			sender.send(sentMessage);
	    try { Thread.sleep(1000);} catch (InterruptedException e) {}
			IMixedMessage receivedMessage1 = (IMixedMessage) receiver1.getReceived();
			IMixedMessage receivedMessage2 = (IMixedMessage) receiver2.getReceived();
			assertMessages(receivedMessage1, receivedMessage2);
		}
	}
	
	@Test
	public void hessian() throws Exception {
		CommunicationConfigFactory simpleCommunication = new SimpleCommunicationFactory();
		try (ClosableSender sender = new HessianPublisher(simpleCommunication)) {
			Receiver receiver1 = new HessianSubcriber(simpleCommunication);
			Receiver receiver2 = new HessianSubcriber(simpleCommunication);
			IMixedMessage sentMessage = new PropertyMessageFactory().createMixedMessage();
			sender.send(sentMessage);
	    try { Thread.sleep(1000);} catch (InterruptedException e) {}
			IMixedMessage receivedMessage1 = (IMixedMessage) receiver1.getReceived();
			IMixedMessage receivedMessage2 = (IMixedMessage) receiver2.getReceived();
			assertMessages(receivedMessage1, receivedMessage2);
		}
	}
	
	@Test
	public void rmi() throws Exception {
		ContextFactory contextFactory = new TcpContextFactory();
		try (ClosableSender sender = new SpringTcpPublisher(contextFactory)) {
			Receiver receiver1 = new SpringSubscriber(contextFactory);
			Receiver receiver2 = new SpringSubscriber(contextFactory);
			IMixedMessage sentMessage = new PropertyMessageFactory().createMixedMessage();
			sender.send(sentMessage);
			try {Thread.sleep(1000);} catch (InterruptedException e) {}
			IMixedMessage receivedMessage1 = (IMixedMessage) receiver1.getReceived();
			IMixedMessage receivedMessage2 = (IMixedMessage) receiver2.getReceived();
			assertMessages(receivedMessage1, receivedMessage2);
		}
	}
	
	@Test
	public void rmiSsl() throws Exception {
		ContextFactory contextFactory = new TcpSslContextFactory();
    try { Thread.sleep(3000);} catch (InterruptedException e) {}
		System.setProperty("javax.net.ssl.trustStore", "messaging.truststore");
		System.setProperty("javax.net.ssl.trustStorePassword", "secureexample");
		try (ClosableSender sender = new SpringTcpPublisher(contextFactory)) {
			Receiver receiver1 = new SpringSubscriber(contextFactory);
			Receiver receiver2 = new SpringSubscriber(contextFactory);
			IMixedMessage sentMessage = new PropertyMessageFactory().createMixedMessage();
			sender.send(sentMessage);
	    try { Thread.sleep(1000);} catch (InterruptedException e) {}
			IMixedMessage receivedMessage1 = (IMixedMessage) receiver1.getReceived();
			IMixedMessage receivedMessage2 = (IMixedMessage) receiver2.getReceived();
			assertMessages(receivedMessage1, receivedMessage2);
		}
	}
	
	@Test
	public void httpsInvoker() throws Exception {
		CommunicationConfigFactory secureSpringCommunication = new SecureCommunicationFactory();
		ContextFactory contextFactory = new HttpsContextFactory();
		try (ClosableSender sender = new SpringHttpPublisher(contextFactory, secureSpringCommunication)) {
			Receiver receiver1 = new SpringSubscriber(contextFactory);
			Receiver receiver2 = new SpringSubscriber(contextFactory);
			IMixedMessage sentMessage = new PropertyMessageFactory().createMixedMessage();
			sender.send(sentMessage);
			IMixedMessage receivedMessage1 = (IMixedMessage) receiver1.getReceived();
			IMixedMessage receivedMessage2 = (IMixedMessage) receiver2.getReceived();
			assertMessages(receivedMessage1, receivedMessage2);
		}
	}
	
	@Test
	public void httpInvoker() throws Exception {
		CommunicationConfigFactory springCommunication = new SimpleCommunicationFactory();
		ContextFactory contextFactory = new HttpContextFactory();
		try (ClosableSender sender = new SpringHttpPublisher(contextFactory, springCommunication)) {
			Receiver receiver1 = new SpringSubscriber(contextFactory);
			Receiver receiver2 = new SpringSubscriber(contextFactory);
			IMixedMessage sentMessage = new PropertyMessageFactory().createMixedMessage();
			sender.send(sentMessage);
	    try { Thread.sleep(1000);} catch (InterruptedException e) {}
			IMixedMessage receivedMessage1 = (IMixedMessage) receiver1.getReceived();
			IMixedMessage receivedMessage2 = (IMixedMessage) receiver2.getReceived();
			assertMessages(receivedMessage1, receivedMessage2);
		}
	}
	
	private void assertMessages(IMixedMessage message1, IMixedMessage message2) {
		assertEquals(message1.isActive(), message2.isActive());
		assertEquals(message1.getCount(), message2.getCount());
		assertEquals(message1.getAmount(), message2.getAmount(), 0.0001);
		assertEquals(message1.getText(), message2.getText());
		assertArrayEquals(message1.getTags(), message2.getTags());
		assertEquals(message1.getProperties(), message2.getProperties());
	}
}