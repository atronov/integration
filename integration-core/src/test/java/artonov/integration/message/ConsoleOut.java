package artonov.integration.message;

import java.io.ByteArrayOutputStream;
import java.io.PrintStream;

import org.apache.commons.io.output.TeeOutputStream;
import org.junit.Test;

public class ConsoleOut {
	
	@Test
	public void test() {
		ByteArrayOutputStream baos = new ByteArrayOutputStream();
		TeeOutputStream biDirection = new TeeOutputStream(System.out, baos);
		PrintStream ps = new PrintStream(biDirection);
		System.setOut(ps);
		System.out.println("Hello from test");
		System.out.print("Hello from test again");
		String result = baos.toString();
		System.out.println(result);
	}

}
