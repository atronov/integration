package artonov.integration.message;

import org.junit.Test;

import atronov.integration.run.remoting.NodeInfo;
import atronov.integration.run.remoting.RmiRunner;

public class RmiRunnerTest {
	@Test
	public void test() throws InterruptedException {
		RmiRunner rmi = new RmiRunner(new NodeInfo("188.226.160.181"));
		rmi.run();
		Thread.sleep(10_000);
		rmi.stop();
		String out = rmi.getOut();
	}
}
