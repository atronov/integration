package artonov.integration.message;

import static atronov.integration.run.build.DataType.BINARY;
import static atronov.integration.run.build.DataType.TEXT;
import static atronov.integration.run.build.Protocol.HTTP;
import static atronov.integration.run.build.Protocol.TCP;
import static atronov.integration.run.build.Technology.HESSIAN;
import static atronov.integration.run.build.Technology.JMS;
import static atronov.integration.run.build.Technology.REST;
import static atronov.integration.run.build.Technology.RMI;
import static atronov.integration.run.build.Topology.QUEUE;
import static atronov.integration.run.build.Topology.TOPIC;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;

import static org.junit.Assert.*;
import static org.hamcrest.Matchers.*;

import org.apache.commons.io.FileUtils;
import org.apache.log4j.Logger;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;

import com.google.gson.Gson;

import atronov.integration.config.JmsConfing;
import atronov.integration.message.ICustomMessage;
import atronov.integration.message.IMixedMessage;
import atronov.integration.message.pojo.custom.CustomMessageBuilder;
import atronov.integration.message.simple.PropertyMessageFactory;
import atronov.integration.run.build.BuildConfig;
import atronov.integration.run.build.Topology;
import atronov.integration.run.remoting.Facade;
import atronov.integration.run.remoting.ModelDebugFacade;
import atronov.integration.run.remoting.ModelFacede;
import atronov.integration.run.remoting.ModelRunResult;
import atronov.integration.run.remoting.NodeInfo;
import atronov.integration.transfer.jms.resources.JmsResourcesFactory;
import atronov.integration.transfer.jms.resources.TcpJmsResourcesFactory;

public class ModelTest {
	
	private static String simple_text = " Sed ut perspiciatis unde omnis iste natus error sit voluptatem accusantium doloremque laudantium, totam rem aperiam, eaque ipsa quae ab illo inventore veritatis et quasi architecto beatae vitae dicta sunt explicabo. Nemo enim ipsam voluptatem quia voluptas sit aspernatur aut odit aut fugit, sed quia consequuntur magni dolores eos qui ratione voluptatem sequi nesciunt. Neque porro quisquam est, qui dolorem ipsum quia dolor sit amet, consectetur, adipisci velit, sed quia non numquam eius modi tempora incidunt ut labore et dolore magnam aliquam quaerat voluptatem. Ut enim ad minima veniam, quis nostrum exercitationem ullam corporis suscipit laboriosam, nisi ut aliquid ex ea commodi consequatur? Quis autem vel eum iure reprehenderit qui in ea voluptate velit esse quam nihil molestiae consequatur, vel illum qui dolorem eum fugiat quo voluptas nulla pariatur?";
	
	private final Logger log = Logger.getLogger(this.getClass());
	
	@Test
	public void cleanJms() {
		JmsResourcesFactory tcpJmsResources = new TcpJmsResourcesFactory(JmsConfing.get().jmsTcpFactory(), JmsConfing.get().jmsTopic());
		new JmsHelper(tcpJmsResources).emptyDestination();
		tcpJmsResources = new TcpJmsResourcesFactory(JmsConfing.get().jmsTcpFactory(), JmsConfing.get().jmsQueue());
		new JmsHelper(tcpJmsResources).emptyDestination();
	}
	
	@Test
	public void test() throws InterruptedException {
		BuildConfig config = new BuildConfig(RMI, TOPIC, HTTP, TEXT, false);
		IMixedMessage message = new PropertyMessageFactory().createMixedMessage();
		List<NodeInfo> nodes = new ArrayList<>();
		nodes.add(new NodeInfo("188.226.171.174"));
		nodes.add(new NodeInfo("188.226.197.65"));
		nodes.add(new NodeInfo("188.226.187.236"));
		nodes.add(new NodeInfo("188.226.162.179"));
		Facade model = new ModelFacede();
		model.setNodes(nodes);
		model.setResourceConfig(config);
		model.setJmsHost(new NodeInfo("188.226.162.179"));
		model.setIterationTime(500);
		model.setMessageToSend(message);
		model.setMaxIterationCount(10_000);
		model.setSubscribers(3);
		model.start();
		Thread.sleep(30_000);
		model.stop();
		Map<String, String> errMapping = model.getErrors();
		StringBuilder totalLog = new StringBuilder();
		for (Entry<String,String> errs: errMapping.entrySet()) {
			String currentLog = "\r\n-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-\r\n"
					+ "Errors from " + errs.getKey()+"\r\n\r\n"
					+ errs.getValue() + "\r\n"
					+ "-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-\r\n";
			totalLog.append(currentLog);
			log.info(currentLog);
			}
		log.info("Total trafic transfered " + model.getTotalBytesTransferred());
		log.info("Trafic transfered by nodes " + model.getBytesTransferred());
		saveResult(model);
		assertThat(errMapping.values(), everyItem(isEmptyOrNullString()));
		
	}
	
	@Test
	public void testNew() throws InterruptedException {
		BuildConfig config = new BuildConfig(JMS, QUEUE, HTTP, BINARY, false);
		ICustomMessage message = CustomMessageBuilder.CreateForList()
				.add("hi")
				.build();
		List<NodeInfo> nodes = new ArrayList<>();
		nodes.add(new NodeInfo("188.226.162.179"));
		nodes.add(new NodeInfo("188.226.171.174"));
		ModelDebugFacade model = new ModelDebugFacade();
		// model.setNodes(nodes);
		model.setResourceConfig(config);
		model.setJmsHost(new NodeInfo("188.226.197.65"));
		model.setIterationTime(500);
		model.setMessageToSend(message);
		model.setMaxIterationCount(10_000);
		model.setRequiredMessages(3);
		model.start();
		int sent = 0;
		int received = 0;
		do {
			sent = model.sentMessages();
			received = model.receivedMessages();
			log.info("Sent: "+sent);
			log.info("Received: "+received);
		} while(sent < 3 || received < 3);
		Thread.sleep(5000);
		model.stop();
		Map<String, String> errMapping = model.getErrors();
		StringBuilder totalLog = new StringBuilder();
		for (Entry<String,String> errs: errMapping.entrySet()) {
			String currentLog = "\r\n-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-\r\n"
					+ "Errors from " + errs.getKey()+"\r\n\r\n"
					+ errs.getValue() + "\r\n"
					+ "-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-\r\n";
			totalLog.append(currentLog);
			log.info(currentLog);
			}
		log.info("Total trafic transfered " + model.getTotalBytesTransferred());
		log.info("Trafic transfered by nodes " + model.getBytesTransferred());
		saveResult(model);
		assertThat(errMapping.values(), everyItem(isEmptyOrNullString()));
		
	}
	
	private boolean saveResult(Facade model) {
		final String fileNamePattern = "result/result%s.json";
		String fileName = String.format(fileNamePattern, System.currentTimeMillis());
		log.info("Save run result as file " + fileName);
		ModelRunResult result = model.getResult();
		String resultJson = new Gson().toJson(result);
		try {
			FileUtils.writeStringToFile(new File(fileName), resultJson);
			return true;
		} catch (IOException e) {
			log.error("Error while writing result to json file", e);
			return false;
		}
	}
}
