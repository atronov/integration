package artonov.integration.message;

import org.junit.Assert;
import org.junit.Test;
import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;

import atronov.integration.transfer.resource.QueueService;

public class Spring {

	@Test
	public void check() {
		new ClassPathXmlApplicationContext("integration-server.xml");
		ApplicationContext factory = new ClassPathXmlApplicationContext("integration-client.xml");
		QueueService queue = (QueueService) factory.getBean("remoteQueue");
		Assert.assertNotNull(queue);
	}
	
}
