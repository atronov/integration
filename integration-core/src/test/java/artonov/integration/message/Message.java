package artonov.integration.message;

import static org.hamcrest.Matchers.arrayContaining;
import static org.junit.Assert.assertArrayEquals;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertThat;
import static org.junit.Assert.assertTrue;

import org.junit.Test;

import atronov.integration.message.IMixedMessage;
import atronov.integration.message.Serializer;
import atronov.integration.message.bson.BsonSerializer;
import atronov.integration.message.json.JsonSerializer;
import atronov.integration.message.pojo.MixedMessage;
import atronov.integration.message.simple.PropertyMessageFactory;

import com.google.common.collect.ImmutableMap;

public class Message {
	
	@Test
	public void loadFromProperty() {
		IMixedMessage message = new PropertyMessageFactory().createMixedMessage();
		String expectedMessage = "This is the simpliest text message to test application integration";
		assertEquals("Wrong message text.", expectedMessage, message.getText());
		assertEquals("Wrong count in message", 13547, message.getCount());
		assertEquals("Wrong amount in message", 5672323.342346, message.getAmount(), 0.00001);
		assertTrue("Message isn't active", message.isActive());
		assertThat("Tags don't contains needed values"
				, message.getTags()
				, arrayContaining("jms","message","javaee","rsreu","master"));
		assertEquals("Wrong message properties"
				, message.getProperties()
				, ImmutableMap.of(
						"author","alexey tronov",
						"group","840M",
						"email","atronov@gmail.com",
						"skype","alexey_tronov"));
	}
	
	@Test
	public void serializationToJson() {
		IMixedMessage message = new PropertyMessageFactory().createMixedMessage();
		Serializer<String> serializer = new JsonSerializer();
		String json = serializer.serealize(message);
		IMixedMessage handledMessage = serializer.deserealize(json, MixedMessage.class);
		assertEquals(message.isActive(), handledMessage.isActive());
		assertEquals(message.getCount(), handledMessage.getCount());
		assertEquals(message.getAmount(), handledMessage.getAmount(), 0.0001);
		assertEquals(message.getText(), handledMessage.getText());
		assertArrayEquals(message.getTags(), handledMessage.getTags());
		assertEquals(message.getProperties(), handledMessage.getProperties());
	}
	
	@Test
	public void serializationToBson() {
		IMixedMessage message = new PropertyMessageFactory().createMixedMessage();
		Serializer<byte[]> serializer = new BsonSerializer();
		byte[] bson = serializer.serealize(message);
		IMixedMessage handledMessage = serializer.deserealize(bson, MixedMessage.class);
		assertEquals(message.isActive(), handledMessage.isActive());
		assertEquals(message.getCount(), handledMessage.getCount());
		assertEquals(message.getAmount(), handledMessage.getAmount(), 0.0001);
		assertEquals(message.getText(), handledMessage.getText());
		assertArrayEquals(message.getTags(), handledMessage.getTags());
		assertEquals(message.getProperties(), handledMessage.getProperties());
	}
}
