package artonov.integration.message;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.text.ParseException;
import java.text.SimpleDateFormat;

import org.apache.commons.io.FileUtils;
import org.junit.Test;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;

import de.undercouch.bson4jackson.BsonFactory;

public class BsonTest {
	@Test
	public void check() throws ParseException, IOException {
		BookDetails book = new BookDetails();
		book.bookId = 334455;
		book.title = "Head First Design Patterns";
		book.description = "At any given moment, somewhere in the world someone struggles with the same software design problems you have.";
		book.reslised = new SimpleDateFormat("MMyyyy").parse("102004");
		book.pages = 678;
		book.authors = new String[] {"Eric Freeman", "Elisabeth Robson", "Bert Bates", "Kathy Sierra"};
		book.publisher = "O'Reilly Media";
		book.available = true;
		
    ObjectMapper mapper = new ObjectMapper(new BsonFactory());
		byte[] bytes = mapper.writeValueAsBytes(book);
		String str = new String(bytes, "UTF-8");
		int c = 0;
		FileUtils.writeStringToFile(new File("bson.txt"), str);
	}
}
