package artonov.integration.message;

import static atronov.integration.run.build.DataType.BINARY;
import static atronov.integration.run.build.DataType.TEXT;
import static atronov.integration.run.build.Protocol.HTTP;
import static atronov.integration.run.build.Protocol.TCP;
import static atronov.integration.run.build.Technology.HESSIAN;
import static atronov.integration.run.build.Technology.JMS;
import static atronov.integration.run.build.Technology.REST;
import static atronov.integration.run.build.Technology.RMI;
import static atronov.integration.run.build.Topology.QUEUE;
import static atronov.integration.run.build.DataType.BINARY;
import static atronov.integration.run.build.DataType.TEXT;
import static atronov.integration.run.build.Protocol.HTTP;
import static atronov.integration.run.build.Technology.JMS;
import static atronov.integration.run.build.Technology.REST;
import static atronov.integration.run.build.Topology.QUEUE;
import static org.hamcrest.Matchers.containsString;
import static org.hamcrest.Matchers.*;
import static org.hamcrest.Matchers.allOf;
import static org.junit.Assert.assertThat;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;

import org.apache.log4j.Logger;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.Parameterized;
import org.junit.runners.Parameterized.Parameters;

import atronov.integration.config.JmsConfing;
import atronov.integration.message.IMixedMessage;
import atronov.integration.message.simple.PropertyMessageFactory;
import atronov.integration.run.build.BuildConfig;
import atronov.integration.run.build.Technology;
import atronov.integration.run.remoting.Facade;
import atronov.integration.run.remoting.ModelFacede;
import atronov.integration.run.remoting.NodeInfo;
import atronov.integration.transfer.jms.resources.JmsResourcesFactory;
import atronov.integration.transfer.jms.resources.TcpJmsResourcesFactory;

import com.google.common.collect.ImmutableList;

@RunWith(Parameterized.class)
public class ModelTestTotal {

	private final Logger log = Logger.getLogger(this.getClass());
	
	@Parameters
	public static Collection<Object[]> getResources() {
		 return ImmutableList.of(
				 new Object[] { new BuildConfig(RMI, QUEUE, TCP, BINARY, false) },
				 new Object[] { new BuildConfig(RMI, QUEUE, HTTP, BINARY, false) },
				 new Object[] { new BuildConfig(RMI, QUEUE, TCP, BINARY, true) },
				 new Object[] { new BuildConfig(RMI, QUEUE, HTTP, BINARY, true) },
				 new Object[] { new BuildConfig(HESSIAN, QUEUE, HTTP, BINARY, false) },
				 new Object[] { new BuildConfig(HESSIAN, QUEUE, HTTP, BINARY, true) },
				 new Object[] { new BuildConfig(JMS, QUEUE, TCP, BINARY, false) },
				 new Object[] { new BuildConfig(JMS, QUEUE, TCP, TEXT, false) },
				 new Object[] { new BuildConfig(JMS, QUEUE, TCP, BINARY, true) },
				 new Object[] { new BuildConfig(JMS, QUEUE, TCP, TEXT, true) },
				 new Object[] { new BuildConfig(JMS, QUEUE, HTTP, TEXT, false) },
				 new Object[] { new BuildConfig(JMS, QUEUE, HTTP, BINARY, false) },
				 new Object[] { new BuildConfig(REST, QUEUE, HTTP, BINARY, false) },
				 new Object[] { new BuildConfig(REST, QUEUE, HTTP, TEXT, false) },
				 new Object[] { new BuildConfig(REST, QUEUE, HTTP, TEXT, true) },
				 new Object[] { new BuildConfig(REST, QUEUE, HTTP, BINARY, true) }
				 );
	}
	
	private final BuildConfig config;
	
	public ModelTestTotal(BuildConfig config) {
		this.config = config;
	}
	
	@Before
	public void cleanJms() {
		if (config.getTechnology() == JMS) {
			JmsResourcesFactory tcpJmsResources = new TcpJmsResourcesFactory(JmsConfing.get().jmsTcpFactory(), JmsConfing.get().jmsTopic());
			new JmsHelper(tcpJmsResources).emptyDestination();
			tcpJmsResources = new TcpJmsResourcesFactory(JmsConfing.get().jmsTcpFactory(), JmsConfing.get().jmsQueue());
			new JmsHelper(tcpJmsResources).emptyDestination();
		}
	}
	
	@Test
	public void test() throws InterruptedException {
		IMixedMessage message = new PropertyMessageFactory().createMixedMessage();
		List<NodeInfo> nodes = new ArrayList<>();
		nodes.add(new NodeInfo("188.226.160.181"));
		nodes.add(new NodeInfo("188.226.169.13"));
		Facade model = new ModelFacede();
		model.setNodes(nodes);
		model.setResourceConfig(config);
		model.setJmsHost(new NodeInfo("188.226.162.179"));
		model.setIterationTime(500);
		model.setMessageToSend(message);
		model.start();
		Thread.sleep(5_000);
		model.stop();
		Map<String, String> errMapping = model.getErrors();
		StringBuilder totalLog = new StringBuilder();
		for (Entry<String,String> errs: errMapping.entrySet()) {
			String currentLog = "\r\n-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-\r\n"
					+ "Errors from " + errs.getKey()+"\r\n\r\n"
					+ errs.getValue() + "\r\n"
					+ "-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-\r\n";
			totalLog.append(currentLog);
			log.info(currentLog);
			}
		assertThat(errMapping.values(), everyItem(isEmptyOrNullString()));
				
	}
	
	@AfterClass
	public static void delay() throws InterruptedException {
		Thread.sleep(5000);
	}
}
