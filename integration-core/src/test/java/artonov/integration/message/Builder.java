package artonov.integration.message;


import static atronov.integration.run.build.DataType.BINARY;
import static atronov.integration.run.build.DataType.TEXT;
import static atronov.integration.run.build.Protocol.HTTP;
import static atronov.integration.run.build.Protocol.TCP;
import static atronov.integration.run.build.Technology.HESSIAN;
import static atronov.integration.run.build.Technology.JMS;
import static atronov.integration.run.build.Technology.REST;
import static atronov.integration.run.build.Technology.RMI;
import static atronov.integration.run.build.Topology.QUEUE;
import static atronov.integration.run.build.Topology.TOPIC;
import static org.hamcrest.Matchers.instanceOf;
import static org.junit.Assert.assertThat;

import org.junit.Test;

import atronov.integration.run.build.ResourceBuilder;
import atronov.integration.transfer.Receiver;
import atronov.integration.transfer.Sender;
import atronov.integration.transfer.hessian.HessianPublisher;
import atronov.integration.transfer.hessian.HessianSubcriber;
import atronov.integration.transfer.jms.JmsBinaryReceiver;
import atronov.integration.transfer.jms.JmsBinarySender;
import atronov.integration.transfer.rest.RestTextPublisher;
import atronov.integration.transfer.rest.RestTextSubscriber;
import atronov.integration.transfer.rmi.spring.SpringSender;
import atronov.integration.transfer.rmi.spring.SpringTcpReceiver;

public class Builder {

	@Test
	public void tcpJmsBinaryQueue() {
		ResourceBuilder builder = ResourceBuilder
		.forTechology(JMS)
		.use(TCP)
		.use(QUEUE)
		.use(BINARY)
		.dontUseSsl();
		Sender sender = builder.buildSender();
		Receiver receiver = builder.buildReceiver();
		assertThat(sender, instanceOf(JmsBinarySender.class));
		assertThat(receiver, instanceOf(JmsBinaryReceiver.class));
	}
	
	@Test
	public void hessainTopic() throws Exception {
		ResourceBuilder builder = ResourceBuilder
		.forTechology(HESSIAN)
		.use(HTTP)
		.use(TOPIC)
		.use(BINARY)
		.dontUseSsl();
		Sender sender = builder.buildSender();
		Receiver receiver = builder.buildReceiver();
		if (sender instanceof AutoCloseable)
			((AutoCloseable)sender).close();
		assertThat(sender, instanceOf(HessianPublisher.class));
		assertThat(receiver, instanceOf(HessianSubcriber.class));
	}
	
	@Test
	public void rmiSslTcpQueue() throws Exception {
		ResourceBuilder builder = ResourceBuilder
		.forTechology(RMI)
		.use(TCP)
		.use(QUEUE)
		.use(BINARY)
		.useSsl();
		Sender sender = builder.buildSender();
		Receiver receiver = builder.buildReceiver();
		if (receiver instanceof AutoCloseable)
			((AutoCloseable)receiver).close();
		assertThat(sender, instanceOf(SpringSender.class));
		assertThat(receiver, instanceOf(SpringTcpReceiver.class));
	}
	
	@Test
	public void restHttpsTextTopic() throws Exception {
		ResourceBuilder builder = ResourceBuilder
		.forTechology(REST)
		.use(HTTP)
		.use(TOPIC)
		.use(TEXT)
		.useSsl();
		Sender sender = builder.buildSender();
		Receiver receiver = builder.buildReceiver();
		if (sender instanceof AutoCloseable)
			((AutoCloseable)sender).close();
		assertThat(sender, instanceOf(RestTextPublisher.class));
		assertThat(receiver, instanceOf(RestTextSubscriber.class));
	}
	
	@Test(expected=IllegalStateException.class)
	public void httpsJmsBinaryTopic() {
		ResourceBuilder builder = ResourceBuilder
		.forTechology(JMS)
		.use(HTTP)
		.use(TOPIC)
		.use(BINARY)
		.useSsl();
		Sender sender = builder.buildSender();
	}
	
	@Test(expected=IllegalStateException.class)
	public void hessianTcpBinaryTopic() {
		ResourceBuilder builder = ResourceBuilder
		.forTechology(HESSIAN)
		.use(TCP)
		.use(TOPIC)
		.use(BINARY)
		.useSsl();
		Sender sender = builder.buildSender();
	}
	
	@Test(expected=IllegalStateException.class)
	public void rmiHttpTextTopic() {
		ResourceBuilder builder = ResourceBuilder
		.forTechology(RMI)
		.use(HTTP)
		.use(TOPIC)
		.use(TEXT)
		.dontUseSsl();
		Sender sender = builder.buildSender();
	}
}
