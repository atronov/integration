package artonov.integration.message;

import static org.junit.Assert.assertArrayEquals;
import static org.junit.Assert.assertEquals;

import java.net.URI;
import java.util.Collection;

import javax.ws.rs.core.UriBuilder;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.Parameterized;
import org.junit.runners.Parameterized.Parameters;

import atronov.integration.config.JmsConfing;
import atronov.integration.message.IMixedMessage;
import atronov.integration.message.MessageHandlingFactory;
import atronov.integration.message.bson.BsonMessageHandlingFactory;
import atronov.integration.message.json.JsonMessageHandlingFactory;
import atronov.integration.message.simple.PropertyMessageFactory;
import atronov.integration.run.build.DataType;
import atronov.integration.run.build.Protocol;
import atronov.integration.run.build.ResourceBuilder;
import atronov.integration.run.build.Technology;
import atronov.integration.run.build.Topology;
import atronov.integration.transfer.Receiver;
import atronov.integration.transfer.Sender;
import atronov.integration.transfer.jms.resources.HttpJmsResourcesFactory;
import atronov.integration.transfer.jms.resources.JmsResourcesFactory;
import atronov.integration.transfer.jms.resources.TcpJmsResourcesFactory;
import atronov.integration.transfer.server.CommunicationConfigFactory;
import atronov.integration.transfer.server.SecureCommunicationFactory;
import atronov.integration.transfer.server.SimpleCommunicationFactory;

import com.google.common.collect.ImmutableList;

@RunWith(Parameterized.class)
public class Transfer {

	@Parameters
	public static Collection<Object[]> getResources() {
		MessageHandlingFactory<String> messageTextHandling = new JsonMessageHandlingFactory();
		MessageHandlingFactory<byte[]> messageBinaryHandling = new BsonMessageHandlingFactory();
		JmsResourcesFactory httpJmsResources = new HttpJmsResourcesFactory(JmsConfing.get().jmsHttpFactory(), JmsConfing.get().jmsQueue());
		JmsResourcesFactory tcpJmsResources = new TcpJmsResourcesFactory(JmsConfing.get().jmsTcpFactory(), JmsConfing.get().jmsQueue());
		JmsResourcesFactory tcpTopicJmsResources = new TcpJmsResourcesFactory(JmsConfing.get().jmsTcpFactory(), JmsConfing.get().jmsTopic());
		JmsResourcesFactory sslTcpJmsResources = new TcpJmsResourcesFactory(JmsConfing.get().jmsTcpSslFactory(), JmsConfing.get().jmsQueue());
		CommunicationConfigFactory simpleCommunication = new SimpleCommunicationFactory();
		CommunicationConfigFactory secureCommunication = new SecureCommunicationFactory();
		CommunicationConfigFactory simpleCommunication2 = new SimpleCommunicationFactory() {
			@Override
			public URI getHostUri() {
				return UriBuilder.fromUri("http://localhost/").port(6778).build();
			}
		};
		CommunicationConfigFactory secureCommunication2 = new SecureCommunicationFactory() {
			@Override
			public URI getHostUri() {
				return UriBuilder.fromUri("https://localhost/").port(6779).build();
			}
		};
		CommunicationConfigFactory simpleCommunication3 = new SimpleCommunicationFactory() {
			@Override
			public URI getHostUri() {
				return UriBuilder.fromUri("http://localhost/").port(3339).build();
			}
		};
		CommunicationConfigFactory secureCommunication3 = new SecureCommunicationFactory() {
			@Override
			public URI getHostUri() {
				return UriBuilder.fromUri("https://localhost/").port(4449).build();
			}
		};
		CommunicationConfigFactory springCommunication = new SimpleCommunicationFactory() {
			@Override
			public URI getHostUri() {
				return UriBuilder.fromUri("http://localhost/").port(1235).build();
			}
		};
		CommunicationConfigFactory secureSpringCommunication = new SecureCommunicationFactory() {
			@Override
			public URI getHostUri() {
				return UriBuilder.fromUri("https://localhost/").port(1236).build();
			}
		};
		ResourceBuilder builder = ResourceBuilder.forTechology(Technology.HESSIAN).use(Protocol.HTTP).use(Topology.QUEUE).use(DataType.BINARY)
				.dontUseSsl();
		// new JmsHelper(tcpJmsResources).emptyDestination();
		return ImmutableList.of(
		// new Object[] { new SpringSender("integration-client.xml"), new
		// SpringTcpReceiver("integration-server.xml") } ,
		// new Object[] { new SpringSender("integration-client-http.xml"), new
		// SpringHttpReceiver("integration-server-http.xml", springCommunication) },

				new Object[] { builder.buildSender(), builder.buildReceiver() }
				// new Object[] { new SpringSender("integration-client-https.xml"), new
				// SpringHttpReceiver("integration-server-http.xml",
				// secureSpringCommunication) }
				);
		// return Lists
		// .newArrayList(
		// // sender, receiver
		// new Object[] { new JmsTextSender(httpJmsResources, messageTextHandling),
		// new JmsTextReceiver(httpJmsResources, messageTextHandling) },
		// new Object[] { new JmsTextSender(tcpJmsResources, messageTextHandling),
		// new JmsTextReceiver(tcpJmsResources, messageTextHandling) },
		// new Object[] { new JmsTextSender(sslTcpJmsResources,
		// messageTextHandling), new JmsTextReceiver(sslTcpJmsResources,
		// messageTextHandling) } ,
		// new Object[] { new JmsBinarySender(httpJmsResources,
		// messageBinaryHandling), new JmsBinaryReceiver(httpJmsResources,
		// messageBinaryHandling) },
		// new Object[] { new JmsBinarySender(tcpJmsResources,
		// messageBinaryHandling), new JmsBinaryReceiver(tcpJmsResources,
		// messageBinaryHandling) },
		// new Object[] { new JmsBinarySender(sslTcpJmsResources,
		// messageBinaryHandling), new JmsBinaryReceiver(sslTcpJmsResources,
		// messageBinaryHandling) },
		// new Object[] { new RestTextSender(simpleCommunication,
		// messageTextHandling), new RestTextReceiver(simpleCommunication,
		// messageTextHandling)},
		// new Object[] { new RestTextSender(secureCommunication,
		// messageTextHandling), new RestTextReceiver(secureCommunication,
		// messageTextHandling)},
		// new Object[] { new RestBinarySender(simpleCommunication2,
		// messageBinaryHandling), new RestBinaryReceiver(simpleCommunication2,
		// messageBinaryHandling)},
		// new Object[] { new RestBinarySender(secureCommunication2,
		// messageBinaryHandling), new RestBinaryReceiver(secureCommunication2,
		// messageBinaryHandling)},
		// new Object[] { new HessianSender(simpleCommunication3), new
		// HessianReceiver(simpleCommunication3)},
		// new Object[] { new HessianSender(secureCommunication3), new
		// HessianReceiver(secureCommunication3)},
		// new Object[] { new SpringSender("integration-client.xml"), new
		// TcpSpringReceiver("integration-server.xml") },
		// new Object[] { new SpringSender("integration-client-ssl.xml"), new
		// TcpSpringReceiver("integration-server-ssl.xml") },
		// new Object[] { new SpringSender("integration-client-http.xml"), new
		// HttpSpringReceiver("integration-server-http.xml", springCommunication) },
		// new Object[] { new SpringSender("integration-client-https.xml"), new
		// HttpSpringReceiver("integration-server-https.xml",
		// secureSpringCommunication) }
		// );
	}

	private Sender sender;
	private Receiver receiver;

	public Transfer(Sender sender, Receiver receiver) {
		this.sender = sender;
		this.receiver = receiver;
	}

	@Test
	public void sendAndReceiveMixed() {
		IMixedMessage sentMessage = new PropertyMessageFactory().createMixedMessage();
		sender.send(sentMessage);
		try {
			Thread.sleep(3000);
		} catch (InterruptedException e) {
		}
		IMixedMessage receivedMessage = (IMixedMessage) receiver.getReceived();
		assertEquals(sentMessage.isActive(), receivedMessage.isActive());
		assertEquals(sentMessage.getCount(), receivedMessage.getCount());
		assertEquals(sentMessage.getAmount(), receivedMessage.getAmount(), 0.0001);
		assertEquals(sentMessage.getText(), receivedMessage.getText());
		assertArrayEquals(sentMessage.getTags(), receivedMessage.getTags());
		assertEquals(sentMessage.getProperties(), receivedMessage.getProperties());
	}

	// @Test
	// public void sendAndReceiveText() {
	// ITextMessage sentMessage = new
	// PropertyMessageFactory().createTextMessage();
	// sender.publish(sentMessage);
	// ITextMessage receivedMessage = (ITextMessage) receiver.receive();
	// assertEquals(sentMessage.getText(), receivedMessage.getText());
	// }
}
