package artonov.integration.message;

import junit.framework.Assert;

import org.junit.Test;

import atronov.integration.message.ICustomMessage;
import atronov.integration.message.IMessage;
import atronov.integration.message.MessageHandlingFactory;
import atronov.integration.message.Serializer;
import atronov.integration.message.TypeRecognition;
import atronov.integration.message.bson.BsonMessageHandlingFactory;
import atronov.integration.message.json.JsonMessageHandlingFactory;
import atronov.integration.message.pojo.custom.CustomListElement;
import atronov.integration.message.pojo.custom.CustomMapElement;
import atronov.integration.message.pojo.custom.CustomMessageBuilder;

public class CustomMessage {
	
	private IMessage createMessage() {
		CustomMessageBuilder messageBuilder = CustomMessageBuilder
				.CreateForMap()
				.put("flag1",true)
				.put("flag2",false)
				.put("flag3",true)
				.put("text1", "gsfgsgsdgsdfsdgsdg")
				.put("text2", "ergerhweh3hrergweghw3rg3aqger")
				.put("text3", "ergergergqwgq34gq34rgq35gq4gq")
				.put("number1", 5)
				.put("number2", 45)
				.put("number3", 532.532)
				.put("number4", -0.2423);
				
			messageBuilder.putListElement("Child1")
					.add(false)
					.add(true)
					.add(false)
					.add("weeeeeeeee")
					.add("ttttttttttttttt")
					.add("ggggggggggggggggggggggggggggg")
					.add(77)
					.add(88888)
					.add(444.555)
					.add(-0.00000001);
		
		messageBuilder.putMapElement("Child2")
				.put("flag11",false)
				.put("flag12",true)
				.put("flag13",false)
				.put("text11","weeeeeeeee")
				.put("text12","ttttttttttttttt")
				.put("text13","ggggggggggggggggggggggggggggg")
				.put("number11",77)
				.put("number12",88888)
				.put("number13",444.555)
				.put("number14",-0.00000001);
		
		ICustomMessage message = messageBuilder.build();
		return message;
	}
	
	private void assertEquals(IMessage actualMessage, IMessage expectedMessage) {
		Object targer1 = ((CustomMapElement)actualMessage).getContent().get("number1");
		Object targer2 = ((CustomMapElement)actualMessage).getContent().get("text2");
		Object targer3 = ((CustomMapElement)actualMessage).getContent().get("flag2");
		//Object targer4 = ((CustomMapElement)((CustomMapElement)actualMessage).getContent().get("Child2")).getContent().get("text13");
		//Object targer5 = ((CustomListElement)((CustomMapElement)actualMessage).getContent().get("Child1")).getContent().get(4);
		
		Object expected1 = ((CustomMapElement)expectedMessage).getContent().get("number1");
		Object expected2 = ((CustomMapElement)expectedMessage).getContent().get("text2");
		Object expected3 = ((CustomMapElement)expectedMessage).getContent().get("flag2");
		//Object expected4 = ((CustomMapElement)((CustomMapElement)actualMessage).getContent().get("Child2")).getContent().get("text13");
		//Object expected5 = ((CustomListElement)((CustomMapElement)expectedMessage).getContent().get("Child1")).getContent().get(4);
		if (expected1 instanceof Integer)
			expected1 = (double)(int)expected1;
		if (targer1 instanceof Integer)
			targer1 = (double)(int)targer1; 
		
		Assert.assertEquals(expected1, targer1);
		Assert.assertEquals(expected2, targer2);
		Assert.assertEquals(expected3, targer3);
		//Assert.assertEquals(expected4, targer4);
		//Assert.assertEquals(expected5, targer5);
	}
	
	@Test
	public void buildJson() {
		IMessage message = createMessage();
		MessageHandlingFactory<String> handling = new JsonMessageHandlingFactory();
		Serializer<String> serial = handling.createSerializer();
		TypeRecognition<String> recog = handling.createRecognition();
		String jsonMessage = serial.serealize(message);
		Class<? extends IMessage> type = recog.recognize(jsonMessage);
		IMessage resultMessage = serial.deserealize(jsonMessage, type);
		assertEquals(resultMessage, message);
	}
	
	@Test
	public void buildBson() {
		IMessage message = createMessage();
		MessageHandlingFactory<byte[]> handling = new BsonMessageHandlingFactory();
		Serializer<byte[]> serial = handling.createSerializer();
		TypeRecognition<byte[]> recog = handling.createRecognition();
		byte[] jsonMessage = serial.serealize(message);
		Class<? extends IMessage> type = recog.recognize(jsonMessage);
		IMessage resultMessage = serial.deserealize(jsonMessage, type);
		assertEquals(resultMessage, message);
	}

}
