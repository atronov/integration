package artonov.integration.message;

import static atronov.integration.run.build.DataType.TEXT;
import static atronov.integration.run.build.Protocol.HTTP;
import static atronov.integration.run.build.Technology.REST;
import static atronov.integration.run.build.Topology.QUEUE;
import static org.junit.Assert.assertArrayEquals;
import static org.junit.Assert.assertEquals;

import org.junit.Test;

import atronov.integration.config.JmsConfing;
import atronov.integration.message.ICustomMessage;
import atronov.integration.message.IMixedMessage;
import atronov.integration.message.MessageHandlingFactory;
import atronov.integration.message.bson.BsonMessageHandlingFactory;
import atronov.integration.message.json.JsonMessageHandlingFactory;
import atronov.integration.message.pojo.custom.CustomMessageBuilder;
import atronov.integration.message.simple.PropertyMessageFactory;
import atronov.integration.run.build.BuildConfig;
import atronov.integration.run.build.ResourceBuilder;
import atronov.integration.transfer.ClosableReceiver;
import atronov.integration.transfer.ClosableSender;
import atronov.integration.transfer.Sender;
import atronov.integration.transfer.hessian.HessianReceiver;
import atronov.integration.transfer.hessian.HessianSender;
import atronov.integration.transfer.jms.JmsTextReceiver;
import atronov.integration.transfer.jms.JmsTextSender;
import atronov.integration.transfer.jms.resources.HttpJmsResourcesFactory;
import atronov.integration.transfer.jms.resources.JmsResourcesFactory;
import atronov.integration.transfer.jms.resources.TcpJmsResourcesFactory;
import atronov.integration.transfer.rest.RestBinaryReceiver;
import atronov.integration.transfer.rest.RestBinarySender;
import atronov.integration.transfer.rest.RestTextReceiver;
import atronov.integration.transfer.rest.RestTextSender;
import atronov.integration.transfer.rmi.spring.SpringHttpReceiver;
import atronov.integration.transfer.rmi.spring.SpringSender;
import atronov.integration.transfer.rmi.spring.SpringTcpReceiver;
import atronov.integration.transfer.rmi.spring.context.ContextFactory;
import atronov.integration.transfer.rmi.spring.context.HttpContextFactory;
import atronov.integration.transfer.rmi.spring.context.HttpsContextFactory;
import atronov.integration.transfer.rmi.spring.context.TcpContextFactory;
import atronov.integration.transfer.rmi.spring.context.TcpSslContextFactory;
import atronov.integration.transfer.server.CommunicationConfigFactory;
import atronov.integration.transfer.server.SecureCommunicationFactory;
import atronov.integration.transfer.server.SimpleCommunicationFactory;

public class Queue {
	
	@Test
	public void jms() throws Exception {
		JmsResourcesFactory tcpQueueJmsResources = new TcpJmsResourcesFactory(JmsConfing.get().jmsTcpFactory(), JmsConfing.get().jmsQueue());
		MessageHandlingFactory<String> messageTextHandling = new JsonMessageHandlingFactory();
		try (ClosableReceiver receiver = new JmsTextReceiver(tcpQueueJmsResources, messageTextHandling);
				ClosableSender sender = new JmsTextSender(tcpQueueJmsResources, messageTextHandling)) {
			IMixedMessage sentMessage = new PropertyMessageFactory().createMixedMessage();
			sender.send(sentMessage);
			try { Thread.sleep(3000); } catch (InterruptedException e) {}
			IMixedMessage receivedMessage = (IMixedMessage) receiver.getReceived();
			assertMessages(sentMessage, receivedMessage);
		}
	}
	
	@Test
	public void jmsHttp() throws Exception {
		JmsResourcesFactory httpQueueJmsResources = new HttpJmsResourcesFactory(JmsConfing.get().jmsHttpFactory(), JmsConfing.get().jmsQueue());
		CustomMessageBuilder builder = CustomMessageBuilder.CreateForMap()
				.put("bookId", 334455)
				.put("title","Head First Design Patterns")
				.put("description","At any given moment, somewhere in the world someone struggles with the same software design problems you have.")
				.put("reslised","October 2004");
			builder.putListElement("authors")
					.add("Eric Freeman")
					.add("Elisabeth Robson")
					.add("Bert Bates")
					.add("Kathy Sierra");
		ICustomMessage message = builder.build();
		MessageHandlingFactory<String> messageTextHandling = new JsonMessageHandlingFactory();
		try (ClosableReceiver receiver = new JmsTextReceiver(httpQueueJmsResources, messageTextHandling);
				ClosableSender sender = new JmsTextSender(httpQueueJmsResources, messageTextHandling)) {
			sender.send(message);
			try { Thread.sleep(3000); } catch (InterruptedException e) {}
			IMixedMessage receivedMessage = (IMixedMessage) receiver.getReceived();
		}
	}
	
	@Test
	public void jmsHttpSend() throws Exception {
		JmsResourcesFactory httpQueueJmsResources = new HttpJmsResourcesFactory(JmsConfing.get().jmsHttpFactory(), JmsConfing.get().jmsQueue());
		CustomMessageBuilder builder = CustomMessageBuilder.CreateForMap()
				.put("bookId", 334455)
				.put("title","Head First Design Patterns")
				.put("description","At any given moment, somewhere in the world someone struggles with the same software design problems you have.")
				.put("reslised","October 2004");
			builder.putListElement("authors")
					.add("Eric Freeman")
					.add("Elisabeth Robson")
					.add("Bert Bates")
					.add("Kathy Sierra");
		ICustomMessage message = builder.build();
		MessageHandlingFactory<String> messageTextHandling = new JsonMessageHandlingFactory();
		try (ClosableSender sender = new JmsTextSender(httpQueueJmsResources, messageTextHandling)) {
			sender.send(message);
		}
	}
	
	@Test
	public void jmsHttpReceive() throws Exception {
		JmsResourcesFactory httpQueueJmsResources = new HttpJmsResourcesFactory(JmsConfing.get().jmsHttpFactory(), JmsConfing.get().jmsQueue());
		MessageHandlingFactory<String> messageTextHandling = new JsonMessageHandlingFactory();
		try (ClosableReceiver receiver = new JmsTextReceiver(httpQueueJmsResources, messageTextHandling)) {
			receiver.getReceived();
		}
	}
	
	@Test
	public void restText() throws Exception {
		CommunicationConfigFactory simpleCommunication = new SimpleCommunicationFactory();
		MessageHandlingFactory<String> messageTextHandling = new JsonMessageHandlingFactory();
		try (ClosableReceiver receiver = new RestTextReceiver(simpleCommunication, messageTextHandling)) {
			Sender sender = new RestTextSender(simpleCommunication, messageTextHandling);
			IMixedMessage sentMessage = new PropertyMessageFactory().createMixedMessage();
			sender.send(sentMessage);
	    try { Thread.sleep(1000);} catch (InterruptedException e) {}
			IMixedMessage receivedMessage = (IMixedMessage) receiver.getReceived();
			assertMessages(sentMessage, receivedMessage);
		}
	}
	
	@Test
	public void restTextWithBuilder() throws Exception {
		BuildConfig config = new BuildConfig(REST, QUEUE, HTTP, TEXT, false);
		ResourceBuilder resources = ResourceBuilder.fromConfig(config);
		try (ClosableReceiver receiver = (ClosableReceiver)resources.buildReceiver()) {
			Sender sender = resources.buildSender();
			IMixedMessage sentMessage = new PropertyMessageFactory().createMixedMessage();
			sender.send(sentMessage);
	    try { Thread.sleep(1000);} catch (InterruptedException e) {}
			IMixedMessage receivedMessage = (IMixedMessage) receiver.getReceived();
			assertMessages(sentMessage, receivedMessage);
		}
	}
	
	@Test
	public void restBinary() throws Exception {
		CommunicationConfigFactory simpleCommunication = new SimpleCommunicationFactory();
		MessageHandlingFactory<byte[]> messageBinHandling = new BsonMessageHandlingFactory();
		try (ClosableReceiver receiver = new RestBinaryReceiver(simpleCommunication, messageBinHandling)) {
			Sender sender = new RestBinarySender(simpleCommunication, messageBinHandling);
			IMixedMessage sentMessage = new PropertyMessageFactory().createMixedMessage();
			sender.send(sentMessage);
	    try { Thread.sleep(1000);} catch (InterruptedException e) {}
			IMixedMessage receivedMessage = (IMixedMessage) receiver.getReceived();
			assertMessages(sentMessage, receivedMessage);
		}
	}
	
	@Test
	public void hessian() throws Exception {
		CommunicationConfigFactory simpleCommunication = new SimpleCommunicationFactory();
		try (ClosableReceiver receiver = new HessianReceiver(simpleCommunication)) {
			Sender sender = new HessianSender(simpleCommunication);
			IMixedMessage sentMessage = new PropertyMessageFactory().createMixedMessage();
			sender.send(sentMessage);
	    try { Thread.sleep(1000);} catch (InterruptedException e) {}
			IMixedMessage receivedMessage = (IMixedMessage) receiver.getReceived();
			assertMessages(sentMessage, receivedMessage);
		}
		
	}
	
	@Test
	public void hessian1() throws Exception {
		CustomMessageBuilder builder = CustomMessageBuilder.CreateForMap()
			.put("bookId", 334455)
			.put("title","Head First Design Patterns")
			.put("description","At any given moment, somewhere in the world someone struggles with the same software design problems you have.")
			.put("reslised","October 2004");
		builder.putListElement("authors")
				.add("Eric Freeman")
				.add("Elisabeth Robson")
				.add("Bert Bates")
				.add("Kathy Sierra");
		ICustomMessage message = builder.build();
		CommunicationConfigFactory simpleCommunication = new SimpleCommunicationFactory();
		try (ClosableReceiver receiver = new HessianReceiver(simpleCommunication)) {
			Sender sender = new HessianSender(simpleCommunication);
			sender.send(message);
		}
	}
	
	@Test
	public void rmi() throws Exception {
		ContextFactory contextFactory = new TcpContextFactory();
		try (ClosableReceiver receiver = new SpringTcpReceiver(contextFactory)) {
			Sender sender = new SpringSender(contextFactory);
			IMixedMessage sentMessage = new PropertyMessageFactory().createMixedMessage();
			sender.send(sentMessage);
			try {Thread.sleep(1000);} catch (InterruptedException e) {}
			IMixedMessage receivedMessage = (IMixedMessage) receiver.getReceived();
			assertMessages(sentMessage, receivedMessage);
		}
	}
	
	@Test
	public void rmiSsl() throws Exception {
    try { Thread.sleep(3000);} catch (InterruptedException e) {}
		System.setProperty("javax.net.ssl.trustStore", "messaging.truststore");
		System.setProperty("javax.net.ssl.trustStorePassword", "secureexample");
		ContextFactory contextFactory = new TcpSslContextFactory();
		try (ClosableReceiver receiver = new SpringTcpReceiver(contextFactory)) {
			Sender sender = new SpringSender(contextFactory);
			IMixedMessage sentMessage = new PropertyMessageFactory().createMixedMessage();
			sender.send(sentMessage);
			try {Thread.sleep(1000);} catch (InterruptedException e) {}
			IMixedMessage receivedMessage = (IMixedMessage) receiver.getReceived();
			assertMessages(sentMessage, receivedMessage);
		}
	}
	
	@Test
	public void rmiHttp1() throws Exception {
		
		CustomMessageBuilder builder = CustomMessageBuilder.CreateForMap()
				.put("bookId", 334455)
				.put("title","Head First Design Patterns")
				.put("description","At any given moment, somewhere in the world someone struggles with the same software design problems you have.")
				.put("reslised","October 2004");
			builder.putListElement("authors")
					.add("Eric Freeman")
					.add("Elisabeth Robson")
					.add("Bert Bates")
					.add("Kathy Sierra");
			ICustomMessage message = builder.build();
			ContextFactory contextFactory = new HttpContextFactory();
			CommunicationConfigFactory simpleCommunication = new SimpleCommunicationFactory();
			try (ClosableReceiver receiver = new SpringHttpReceiver(contextFactory, simpleCommunication)) {
				Sender sender = new SpringSender(contextFactory);
				sender.send(message);
			}
	}
	
	@Test
	public void httpsInvoker() throws Exception {
		CommunicationConfigFactory secureSpringCommunication = new SecureCommunicationFactory();
		ContextFactory contextFactory = new HttpsContextFactory();
		try (ClosableReceiver receiver = new SpringHttpReceiver(contextFactory, secureSpringCommunication)) {
			Sender sender = new SpringSender(contextFactory);
			IMixedMessage sentMessage = new PropertyMessageFactory().createMixedMessage();
			sender.send(sentMessage);
			try {Thread.sleep(1000);} catch (InterruptedException e) {}
			IMixedMessage receivedMessage = (IMixedMessage) receiver.getReceived();
			assertMessages(sentMessage, receivedMessage);
		}
	}
	
	@Test
	public void httpInvoker() throws Exception {
		CommunicationConfigFactory springCommunication = new SimpleCommunicationFactory();
		ContextFactory contextFactory = new HttpContextFactory();
		try (ClosableReceiver receiver = new SpringHttpReceiver(contextFactory, springCommunication)) {
			Sender sender = new SpringSender(contextFactory);
			IMixedMessage sentMessage = new PropertyMessageFactory().createMixedMessage();
			sender.send(sentMessage);
			try {Thread.sleep(1000);} catch (InterruptedException e) {}
			IMixedMessage receivedMessage = (IMixedMessage) receiver.getReceived();
			assertMessages(sentMessage, receivedMessage);
		}
	}
	
	private void assertMessages(IMixedMessage message1, IMixedMessage message2) {
		assertEquals(message1.isActive(), message2.isActive());
		assertEquals(message1.getCount(), message2.getCount());
		assertEquals(message1.getAmount(), message2.getAmount(), 0.0001);
		assertEquals(message1.getText(), message2.getText());
		assertArrayEquals(message1.getTags(), message2.getTags());
		assertEquals(message1.getProperties(), message2.getProperties());
	}
}
