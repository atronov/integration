package artonov.integration.message;

import static atronov.integration.run.build.DataType.BINARY;
import static atronov.integration.run.build.DataType.TEXT;
import static atronov.integration.run.build.Protocol.HTTP;
import static atronov.integration.run.build.Protocol.TCP;
import static atronov.integration.run.build.Technology.HESSIAN;
import static atronov.integration.run.build.Technology.JMS;
import static atronov.integration.run.build.Technology.REST;
import static atronov.integration.run.build.Technology.RMI;
import static atronov.integration.run.build.Topology.QUEUE;
import static atronov.integration.run.build.Topology.TOPIC;
import static org.hamcrest.Matchers.allOf;
import static org.hamcrest.Matchers.containsString;
import static org.hamcrest.Matchers.not;
import static org.junit.Assert.assertThat;

import org.junit.Test;
import org.springframework.beans.factory.support.BeanDefinitionBuilder;
import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.Bean;
import org.springframework.context.support.AbstractApplicationContext;
import org.springframework.context.support.GenericApplicationContext;
import org.springframework.remoting.rmi.RmiProxyFactoryBean;
import org.springframework.remoting.rmi.RmiServiceExporter;

import atronov.integration.message.IMixedMessage;
import atronov.integration.message.simple.PropertyMessageFactory;
import atronov.integration.monitor.PacketInfo;
import atronov.integration.run.build.BuildConfig;
import atronov.integration.run.build.ResourceBuilder;
import atronov.integration.run.remoting.ClientAppContextBuilder;
import atronov.integration.run.remoting.ResourceControl;
import atronov.integration.run.remoting.Control;
import atronov.integration.run.remoting.ServerAppContextBuilder;
import atronov.integration.transfer.Receiver;

public class ControlTest {

	@Test
	public void local() throws InterruptedException {
		System.setProperty("jnetpcap.device", "\\Device\\NPF_{76498161-6AC7-41B5-9A41-F4A4180A6CB9}");
		
		BuildConfig config = new BuildConfig(RMI, TOPIC, TCP, BINARY, false);
		
		Receiver r = ResourceBuilder.fromConfig(config).buildReceiver();
		
		IMixedMessage message = new PropertyMessageFactory().createMixedMessage();
		
		Control receiverCtl = new ResourceControl();
		receiverCtl.setTrafficMonitor(true);
		receiverCtl.setBuildConfig(config);
		receiverCtl.setReceiver();
		receiverCtl.setMaxIterationCount(180); // 3m
		receiverCtl.setIterationTime(500);
		receiverCtl.start();
		Thread.sleep(1500);
		
		Control senderCtl = new ResourceControl();
		senderCtl.setBuildConfig(config);
		senderCtl.setTrafficMonitor(true);
		senderCtl.setSender();
		senderCtl.setMaxIterationCount(180); // 3m
		senderCtl.setIterationTime(500);
		senderCtl.setMessageForSent(message);
		senderCtl.start();
		
		Thread.sleep(5_000);
		
		senderCtl.stop();
		receiverCtl.stop();
		
		String receiverOut = receiverCtl.getErrors();
		String senderOut = receiverCtl.getErrors();
		
		System.out.println("Captured receiver packets "+receiverCtl.getTraficStat().size());
		for (PacketInfo info : receiverCtl.getTraficStat())
			System.out.println(info);
		
		System.out.println();
		System.out.println("Captured sender packets "+ senderCtl.getTraficStat().size());
		for (PacketInfo info : senderCtl.getTraficStat())
			System.out.println(info);
		
		assertThat(receiverOut,allOf(not(containsString("Exception")),not(containsString("Exception"))));
		assertThat(senderOut,allOf(not(containsString("Exception")),not(containsString("Exception"))));
	}
	
	@Test
	public void localJms() throws InterruptedException {
		
		BuildConfig config = new BuildConfig(JMS, QUEUE, TCP, TEXT, false);
		
		Control receiverCtl = new ResourceControl();
		receiverCtl.setTrafficMonitor(false);
		receiverCtl.setBuildConfig(config);
		receiverCtl.setReceiver();
		receiverCtl.setJmsHost("188.226.197.65");
		receiverCtl.setMaxIterationCount(180); // 3m
		receiverCtl.setIterationTime(500);
		receiverCtl.start();

		Thread.sleep(20_000);

		receiverCtl.stop();
		
		String receiverOut = receiverCtl.getErrors();
		int count = receiverCtl.getReceivedCount();
		int c = 0;
	}
	
	@Test
	public void remote() throws InterruptedException {
		try (AbstractApplicationContext serverContext = ServerAppContextBuilder.build();
				AbstractApplicationContext clientContext = ClientAppContextBuilder.build()) {
		
		BuildConfig config = new BuildConfig(REST, QUEUE, HTTP, TEXT, false);
		IMixedMessage message = new PropertyMessageFactory().createMixedMessage();

		Control receiverCtl = (Control)clientContext.getBean("remoteControl");
		receiverCtl.setBuildConfig(config);
		receiverCtl.setReceiver();
		receiverCtl.setMaxIterationCount(180); // 3m
		receiverCtl.setIterationTime(500);
		receiverCtl.start();
		Thread.sleep(1500);
		
		Control senderCtl = new ResourceControl();
		senderCtl.setBuildConfig(config);
		senderCtl.setSender();
		senderCtl.setMaxIterationCount(180); // 3m
		senderCtl.setIterationTime(500);
		senderCtl.setMessageForSent(message);
		senderCtl.start();
		
		Thread.sleep(5_000);
		
		senderCtl.stop();
		receiverCtl.stop();
		
		String receiverOut = receiverCtl.getErrors();
		String senderOut = senderCtl.getErrors();
		}
	}
	
	@Test
	public void cloud() throws InterruptedException {
		try (AbstractApplicationContext clientContext = ClientAppContextBuilder.build("188.226.169.13")) {
			BuildConfig config = new BuildConfig(HESSIAN, QUEUE, HTTP, BINARY, false);
			Control receiverCtl = (Control)clientContext.getBean("remoteControl");
			receiverCtl.setBuildConfig(config);
			receiverCtl.setReceiver();
			receiverCtl.setMaxIterationCount(180); // 3m
			receiverCtl.setIterationTime(500);
			receiverCtl.start();
			Thread.sleep(3_000);
			receiverCtl.stop();
		}
	}
}
