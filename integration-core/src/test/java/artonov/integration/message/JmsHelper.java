package artonov.integration.message;

import javax.jms.Connection;
import javax.jms.Destination;
import javax.jms.JMSException;
import javax.jms.MessageConsumer;
import javax.jms.Session;
import javax.jms.TextMessage;

import org.apache.log4j.Logger;

import atronov.integration.message.IMessage;
import atronov.integration.transfer.jms.resources.JmsResourcesFactory;

public class JmsHelper {

	private final Logger log = Logger.getLogger(this.getClass());
	
	JmsResourcesFactory resources;
	
	public JmsHelper(JmsResourcesFactory resources) {
		this.resources=resources;
	}
	
	public void emptyDestination() {
		Connection connection = null;
		try {
			try {
				connection = resources.getConnection();
				Session session = connection.createSession(false, Session.AUTO_ACKNOWLEDGE);
				Destination destination = resources.getDestination();
				MessageConsumer consumer = session.createConsumer(destination);
				connection.start();
				int counter = 0;
				javax.jms.Message m;
				while ((m = consumer.receive(5000L)) != null) {
					counter++;
				}
				log.info(String.format("%s messages were deleted from %s",counter, destination));
			} finally {
				if (connection != null)
					connection.close();
			}
		} catch (JMSException e) {
			throw new RuntimeException(e);
		}
	}
}
