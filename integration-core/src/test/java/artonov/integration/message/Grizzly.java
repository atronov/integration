package artonov.integration.message;

import org.junit.Test;

import atronov.integration.run.build.DataType;
import atronov.integration.run.build.Protocol;
import atronov.integration.run.build.ResourceBuilder;
import atronov.integration.run.build.Technology;
import atronov.integration.run.build.Topology;
import atronov.integration.run.console.Console;
import atronov.integration.transfer.ClosableReceiver;

public class Grizzly {

	@Test
	public void fromBuilder() throws Exception {
		ResourceBuilder builder = ResourceBuilder
				.forTechology(Technology.HESSIAN)
				.use(Protocol.HTTP)
				.use(Topology.QUEUE)
				.use(DataType.BINARY)
				.dontUseSsl();
		try (ClosableReceiver rec = (ClosableReceiver)builder.buildReceiver()) {}
	}
	
	@Test
	public void fromConsole() throws Exception {
		String[] args = new String[] {"-rec", "-tech=hessian", "-http", "-bin", "-queue", "-iter=3"};
		Console.main(args);
	}
}
