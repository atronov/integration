package artonov.integration.message;

import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.io.StringWriter;
import java.util.HashMap;

import org.apache.commons.io.IOUtils;
import org.junit.Test;

import com.jcraft.jsch.Channel;
import com.jcraft.jsch.ChannelExec;
import com.jcraft.jsch.ChannelShell;
import com.jcraft.jsch.JSch;
import com.jcraft.jsch.JSchException;
import com.jcraft.jsch.Session;

public class Ssh {
	
	private final String commandPattern = "sudo -S -p '' tcpdump -i eth0 -w bedbedbed '%s'";
	private final String filterExpression = "";
	
	@Test
	public void test() throws IOException, JSchException {
		String host="192.168.0.59";
    String user="alexey";
    String password="balando";
    String command = String.format(commandPattern, filterExpression);
    int port = 22;
    java.util.Properties config = new java.util.Properties(); 
    config.put("StrictHostKeyChecking", "no");
    JSch jsch = new JSch();
    Session session = jsch.getSession(user, host, port);
    session.setConfig(config);
    session.setPassword(password);
    session.connect();
    Channel channel = session.openChannel("exec");
    ((ChannelExec)channel).setErrStream(System.err);
    ((ChannelExec)channel).setCommand(command);
    channel.setInputStream(null);
    InputStream is = channel.getInputStream();
    channel.connect();
    OutputStream out=channel.getOutputStream();
    out.write((password+"\n").getBytes());
    out.flush();
    out.write(("\u0003"+"\n").getBytes());
    out.flush();
    channel.disconnect();
    
    StringWriter writer = new StringWriter();
    IOUtils.copy(is, writer, "utf-8");
    String result = writer.toString();
    
    session.disconnect();
	}
	
}
