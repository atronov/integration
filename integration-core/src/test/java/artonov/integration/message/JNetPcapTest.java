package artonov.integration.message;

import java.util.ArrayList;
import java.util.List;

import org.jnetpcap.Pcap;
import org.jnetpcap.PcapIf;
import org.junit.Test;

public class JNetPcapTest {
	@Test
	public void test() {
		List<PcapIf> alldevs = new ArrayList<>();
		StringBuilder errbuf = new StringBuilder();
		int r = Pcap.findAllDevs(alldevs, errbuf);  
    if (r == Pcap.NOT_OK || alldevs.isEmpty()) {  
        System.err.printf("Can't read list of devices, error is %s", errbuf  
            .toString());  
        return;  
    }
    
    int i = 0;  
    for (PcapIf device : alldevs) {  
        String description =  
            (device.getDescription() != null) ? device.getDescription()  
                : "No description available";  
        System.out.printf("#%d: %s [%s]\n", i++, device.getName(), description);  
    }
	}
}
