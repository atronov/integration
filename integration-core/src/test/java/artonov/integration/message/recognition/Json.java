package artonov.integration.message.recognition;

import java.util.Collection;

import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.Parameterized;
import org.junit.runners.Parameterized.Parameters;

import atronov.integration.message.IMessage;
import atronov.integration.message.MessageFactory;
import atronov.integration.message.Serializer;
import atronov.integration.message.TypeRecognition;
import atronov.integration.message.json.JsonSerializer;
import atronov.integration.message.json.recognition.JsonRecognition;
import atronov.integration.message.pojo.ArrayMessage;
import atronov.integration.message.pojo.BooleanMessage;
import atronov.integration.message.pojo.DoubleMessage;
import atronov.integration.message.pojo.IntegerMessage;
import atronov.integration.message.pojo.MapMessage;
import atronov.integration.message.pojo.MixedMessage;
import atronov.integration.message.pojo.TextMessage;
import atronov.integration.message.simple.PropertyMessageFactory;

import com.google.common.collect.Lists;

@RunWith(Parameterized.class)
public class Json {

	@Parameters
	public static Collection<Object[]> getResources() {
		MessageFactory factory = new PropertyMessageFactory();
		return Lists.newArrayList(
				new Object[] { factory.createMixedMessage(), MixedMessage.class }
				, new Object[] { factory.createIntegerMessage(), IntegerMessage.class }
				, new Object[] { factory.createBooleanMessage(), BooleanMessage.class }
				, new Object[] { factory.createDoubleMessage(), DoubleMessage.class }
				, new Object[] { factory.createTextMessage(), TextMessage.class }
				, new Object[] { factory.createArrayMessage(), ArrayMessage.class }
				, new Object[] { factory.createMapMessage(), MapMessage.class }
				);
	}
	
	private static String cutOffRedundant(IMessage message, Class<? extends IMessage> targetClass) {
		Serializer<String> serializer = new JsonSerializer();
		String text = serializer.serealize(message);
		IMessage messageHandled = serializer.deserealize(text, targetClass);
		return serializer.serealize(messageHandled);
	}
	
	protected IMessage message;
	protected Class<? extends IMessage> expectedType;
	
	public Json(IMessage message, Class<? extends IMessage> expectedType) {
		this.message = message;
		this.expectedType = expectedType;
	}
	
	@Test
	public void recognition() {
		String messageData = cutOffRedundant(message, expectedType);
		TypeRecognition<String> recognition = new JsonRecognition();
		Class<? extends IMessage> messageType = recognition.recognize(messageData);
		Assert.assertEquals("Unexpected recognized class", expectedType, messageType);
	}
}
