sudo apt-get update
sudo apt-get -y install openjdk-7-jre
sudo apt-get -y install openjdk-7-jdk
export JAVA_HOME=/usr/lib/jvm/java-7-openjdk-i386 # path to jdk
export PATH=$PATH:$JAVA_HOME/bin
sudo apt-get -y install maven
sudo apt-get -y install git

# more information there sourceforge.net/projects/jnetpcap/files/jnetpcap/1.3/
wget http://heanet.dl.sourceforge.net/project/jnetpcap/jnetpcap/1.3/jnetpcap-1.3.0-1.ubuntu.i386.tgz
unzip jnetpcap-1.3.0-1.ubuntu.i386.tgz
sudo cp ./jnetpcap-1.3.0/libjnetpcap.so /usr/lib
sudo cp ./jnetpcap-1.3.0/jnetpcap.jar /usr/share/java

# config git
git config --global user.name "Alexey Tronov"
git config --global user.email "atronov@gmail.com"
git clone https://atronov@bitbucket.org/atronov/integration.git

#clean repository
cd integration
git reset --hard
git clean -f -d
git pull

mvn clean compile assembly:single
cd ..
rm -f ./app/integration.jar
mv ./integration/target/*.jar ./app/integration.jar

# for maven & console run
mvn -Dmaven.test.skip=true install
mvn exec:java -Dexec.mainClass="atronov.integration.run.console.Console" -Dexec.args="-rec -tech=hessian -http -bin -queue -iter=60"
mvn exec:java -Dexec.mainClass="atronov.integration.run.console.Console" -Dexec.args="-send -tech=hessian -http -bin -queue -iter=10"

#to build complete jar
mvn clean compile assembly:single
# to run jar
java -jar integration.jar -rec -tech=hessian -http -bin -queue -iter=10

mkdir app
mv ./integration/target/*.jar ./app/integration.jar